<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

//Route::get('/', function () {
//    return view('welcome');
//});

//Route::get('/', 'FrontController@businessRegister')->name('front');
Route::post('/subscription', 'FrontController@subscribe')->name('subscribe');
Route::post('/contact', 'FrontController@sendMail')->name('contact');

Route::get('privacy-policy', function (){
    $title = "Privacy Policy";
    return view('frontend.privacypolicy',compact('title'));
})->name('privacy');

Auth::routes();
Route::get('register/verify/{token}', 'Auth\RegisterController@verify');

//Business register
Route::get('business-register', 'FrontController@businessRegister')->name('business.register');
Route::post('business-add', 'BusinessController@partnersAdd')->name('business.add');

Route::get('/home', function () {
    return redirect()->route('home');
});

//Route::get('/', function () {
//    return redirect()->route('login');
//})->name('root.home');

Route::get('/','HomeController@index')->name('landing');

// Route::get('/','HomeController@index')->name('root.home');

Route::get('/explode', function () {
    $date = "2017/08/26 - 2017/08/27";
    $date_final = explode('-', $date);
    $date1 = $date_final[0];
    print_r($date_final[1]);
    $date_final2 = \Carbon\Carbon::parse($date1)->format('Y-m-d');
    echo $date_final2;
});
//Admin routes
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function () {

    Route::get('home', 'AdminController@index')->name('home');

    Route::get('settings', 'AdminController@settings')->name('settings');
    Route::post('settingsadd', 'AdminController@settingsAdd')->name('settingsAdd');

    Route::get('subscribe', 'AdminController@subscribe')->name('subscribe');

    //Trivia
    Route::get('questions','AdminController@questions')->name('questions');
    Route::get('questions-table','AdminController@questionsTable')->name('questions.table');
    Route::post('questions-add','AdminController@questionsAdd')->name('questions.add');
    Route::get('questions-edit/{id}','AdminController@questionsEdit')->name('questions.edit');
    Route::get('questions-date/{id}','AdminController@questionsDate')->name('questions.date');
    Route::get('questions-delete/{id}','AdminController@questionsDelete')->name('questions.delete');
    Route::get('questions-add-view','AdminController@questionsAddView')->name('questions.add.view');


    //Business types
    Route::get('business-types', 'AdminController@businessTypes')->name('business.types');
    Route::post('business-types-add', 'AdminController@businessTypesAdd')->name('business.types.add');
    Route::get('business-types-table', 'AdminController@businessTypesTable')->name('business.types.table');
    Route::get('business-types-details/{id}', 'AdminController@businessTypesDetails')->name('business.types.details');
    Route::get('business-types-delete/{id}', 'AdminController@businessTypesDelete')->name('business.types.delete');

    //Partners
    Route::get('partners', 'AdminController@partners')->name('partners');
    Route::post('partners-add', 'AdminController@partnersAdd')->name('partners.add');
    Route::get('partners-table', 'AdminController@partnersTable')->name('partners.table');
    Route::get('partners-details/{id}', 'AdminController@partnersDetails')->name('partners.details');
    Route::get('partners-delete/{id}', 'AdminController@partnersDelete')->name('partners.delete');
    Route::get('partners-activate/{id}', 'AdminController@partnersActivate')->name('partners.activate');
    Route::get('partners-deactivate/{id}', 'AdminController@partnersDeactivate')->name('partners.deactivate');

    //Activity
    Route::get('activity', 'AdminController@activity')->name('admin.activity');
    Route::get('activity-table', 'AdminController@activityTable')->name('admin.activity.table');
    Route::get('activity-delete/{id}', 'PartnersController@activityDelete')->name('admin.activity.delete');
    Route::get('activity-activate/{id}', 'PartnersController@activityActivate')->name('admin.activity.activate');
    Route::get('activity-deactivate/{id}', 'PartnersController@activityDeactivate')->name('admin.activity.deactivate');
    Route::get('activity-details/{id}', 'PartnersController@activityDetails')->name('admin.activity.details');

    Route::get('difficultyLevel', 'AdminController@difficultyLevel')->name('difficultyLevel.types');
    Route::post('difficultyLevel-add', 'AdminController@difficultyLevelAdd')->name('difficultyLevel.add');
    Route::get('difficultyLevel-table', 'AdminController@difficultyLevelTable')->name('difficultyLevel.table');
    Route::get('difficultyLevel-details/{id}', 'AdminController@difficultyLevelDetails')->name('difficultyLevel.details');
    Route::get('difficultyLevel-delete/{id}', 'AdminController@difficultyLevelDelete')->name('difficultyLevel.delete');

    Route::get('subscribers', 'AdminController@subscribers')->name('subscribers');
    Route::get('subscriberstable', 'AdminController@subscribersTable')->name('subscribers.table');

    Route::get('comments/{id}', 'AdminController@comments')->name('comments');
    Route::get('comments-table/{id}', 'AdminController@commentsTable')->name('comments.table');

    Route::get('bookings', 'AdminController@bookings')->name('bookings');
    Route::get('bookingsTable', 'AdminController@bookingsTable')->name('bookings.table');

    Route::get('users', 'AdminController@users')->name('users');
    Route::get('usersTable', 'AdminController@usersTable')->name('users.table');

    Route::get('slider', 'AdminController@slider')->name('slider');
    Route::get('slider-table', 'AdminController@sliderTable')->name('slider.table');
    Route::post('slider-add', 'AdminController@sliderAddEdit')->name('slider.add');
    Route::get('slider-details/{id}', 'AdminController@sliderDetails')->name('slider.details');
    Route::get('slider-delete/{id}', 'AdminController@sliderDelete')->name('slider.delete');

    //Activity Images
    Route::get('productImages/{id}', 'AdminController@productImages')->name('productImages');
    Route::get('productImagesTable/{id}', 'AdminController@productImagesTable')->name('productImagesTable');
    Route::post('productImagesAdd', 'AdminController@productImagesAdd')->name('productImagesAdd');
    Route::get('productImagesDelete/{id}', 'AdminController@productImagesDelete')->name('productImagesDelete');

    /**
     * Product routes ofr selling
     */
    Route::get('products', 'AdminController@addProductsSale')->name('product.sell.view');
    Route::get('products-new', 'AdminController@addProductsNew')->name('product.sell.new');
    Route::get('products-table', 'AdminController@addProductsSaleTable')->name('product.sell.table');
    Route::get('products-detail/{id}', 'AdminController@addProductsDetail')->name('product.sell.detail');
    Route::get('products-delete/{id}', 'AdminController@addProductsDelete')->name('product.sell.delete');
    Route::post('products', 'AdminController@addProductsSaleAddEdit')->name('product.sell.addedit');
    Route::get('products-edit/{id}', 'AdminController@addProductsEdit')->name('product.sell.edit');
    Route::get('products-images/{id}', 'AdminController@addProductsImages')->name('product.sell.images');
    Route::get('products-activate/{id}', 'AdminController@addProductsActivate')->name('product.sell.activate');
    Route::get('products-deactivate/{id}', 'AdminController@addProductsDeactivate')->name('product.sell.deactivate');


    /**
     * Products images
     */
    Route::get('productSellImagesTable/{id}', 'AdminController@productSellImagesTable')->name('product.sell.ImagesTable');
    Route::post('productSellImagesAdd', 'AdminController@productSellImagesAdd')->name('product.sell.ImagesAdd');
    Route::get('productSellImagesDelete/{id}', 'AdminController@productSellImagesDelete')->name('product.sell.ImagesDelete');

    /**
     *
     * Help Center
     */

    Route::get('help-center','AdminController@helpCentre')->name("help.centre");
    Route::post('help-center','AdminController@helpcentreAdd')->name("help.center.add");

    Route::get('cart','AdminController@cart')->name('cart');
});


Route::group(['prefix' => 'partners', 'middleware' => ['auth', 'partners']], function () {
    //Partners homes
    Route::get('home', 'PartnersController@partnersindex')->name('home.partners');

    //Activities
    Route::get('activity', 'PartnersController@activity')->name('activity');
    Route::post('activity-add', 'PartnersController@activityAdd')->name('post.activity');
    Route::get('activity-table', 'PartnersController@activityTable')->name('activity.table');
    Route::get('activity-new', 'PartnersController@activityNew')->name('activity.new');
    Route::get('activity-edit/{id}', 'PartnersController@activityEdit')->name('activity.edit');
    Route::get('activity-delete/{id}', 'PartnersController@activityDelete')->name('activity.delete');
    Route::get('activity-activate/{id}', 'PartnersController@activityActivate')->name('activity.activate');
    Route::get('activity-deactivate/{id}', 'PartnersController@activityDeactivate')->name('activity.deactivate');
    Route::get('activity-details/{id}', 'PartnersController@activityDetails')->name('activity.details');

    Route::get('comments/{id}', 'AdminController@comments')->name('comments.partner');
    Route::get('comments-table/{id}', 'AdminController@commentsTable')->name('comments.table.partner');

    Route::get('bookings', 'PartnersController@bookings')->name('bookings.partner');
    Route::get('bookingsTable', 'PartnersController@bookingsTable')->name('bookings.table.partner');
    Route::post('bookingsSearch', 'PartnersController@bookingSearch')->name('bookings.partner.search');

    Route::get('productImages/{id}', 'PartnersController@productImages')->name('productImages.partner');
    Route::get('productImagesTable/{id}', 'PartnersController@productImagesTable')->name('productImagesTable.partner');
    Route::post('productImagesAdd', 'PartnersController@productImagesAdd')->name('productImagesAdd.partner');
    Route::get('productImagesDelete/{id}', 'PartnersController@productImagesDelete')->name('productImagesDelete.partner');

    Route::get('other/{id}','PartnersController@other')->name('other');
    Route::get('other-delete/{id}','PartnersController@otherDelete')->name('other.delete');
    Route::get('otherTable/{id}','PartnersController@otherTable')->name('other.Table');
    Route::post('otherAdd','PartnersController@otherAdd')->name('other.add');

    Route::get('amenities','PartnersController@amenities')->name('amenities');
    Route::get('amenitiesTable','PartnersController@amenitiesTable')->name('amenities.Table');
    Route::post('amenitiesAdd','PartnersController@amenitiesAdd')->name('amenities.add');

    /***
     *
     *
     * Previously Business Partners
     */


});


Route::group(['prefix' => 'business', 'middleware' => ['auth']], function () {
    Route::get('home', 'BusinessController@home')->name('home.business');

    Route::get('stylist', 'BusinessController@stylist')->name('stylist');
    Route::post('stylist', 'BusinessController@stylistAddEdit')->name('stylist.add');
    Route::get('stylist/{id}', 'BusinessController@stylistDetail')->name('stylist.detail');
    Route::get('stylist-table', 'BusinessController@stylistTable')->name('stylist.table');
    Route::get('stylist-delete/{id}', 'BusinessController@stylistDelete')->name('stylist.delete');

    Route::get('customers', 'BusinessController@customers')->name('customers');
    Route::post('customers', 'BusinessController@customersAddEdit')->name('customers.add');
    Route::get('customers/{id}', 'BusinessController@customersDetail')->name('customers.detail');
    Route::get('customers-table', 'BusinessController@customersTable')->name('customers.table');

    Route::get('customers-activity', 'BusinessController@customersActivity')->name('customers.activity');
    Route::get('customers-activity/{id}', 'BusinessController@customersActivityDetail')->name('customers.activity.detail');
    Route::get('customers-activity-table', 'BusinessController@customersActivityTable')->name('customers.activity.table');
    Route::post('customers-activity-table-search', 'BusinessController@customersActivityTableSearch')->name('customers.activity.table.search');

    Route::get('manager', 'BusinessController@manager')->name('manager');
    Route::post('manager', 'BusinessController@managerAdd')->name('manager.add');
    Route::get('manager-table', 'BusinessController@managerTable')->name('manager.table');
    Route::get('manager-delete/{id}', 'BusinessController@managerDelete')->name('manager.delete');
    Route::get('manager-detail/{id}', 'BusinessController@managerDetail')->name('manager.detail');

    Route::get('expenses', 'BusinessController@expenses')->name('expenses');
    Route::post('expenses', 'BusinessController@expensesAdd')->name('expenses.add');
    Route::get('expenses-table', 'BusinessController@expensesTable')->name('expenses.table');
    Route::get('expenses-delete/{id}', 'BusinessController@expensesDelete')->name('expenses.delete');
    Route::get('expenses-detail/{id}', 'BusinessController@expensesDetail')->name('expenses.detail');

    Route::get('target', 'BusinessController@target')->name('expenses.target');
    Route::post('target', 'BusinessController@targetAdd')->name('expenses.target.add');
    Route::get('target-table', 'BusinessController@targetTable')->name('expenses.target.table');
    Route::get('target-delete/{id}', 'BusinessController@targetDelete')->name('expenses.target.delete');
    Route::get('target-detail/{id}', 'BusinessController@targetDetail')->name('expenses.target.detail');

    Route::get('income-target', 'BusinessController@incomeTarget')->name('income.target');
    Route::post('income-target', 'BusinessController@incomeTargetAdd')->name('income.target.add');
    Route::get('income-target-table', 'BusinessController@incomeTargetTable')->name('income.target.table');
    Route::get('income-target-delete/{id}', 'BusinessController@incomeTargetDelete')->name('income.target.delete');
    Route::get('income-target-detail/{id}', 'BusinessController@incomeTargetDetail')->name('income.target.detail');

    /**
     * Profit and loss
     */
    Route::get('profit-loss', 'BusinessController@profitLoss')->name('profit-loss');
    Route::post('profit-loss-filter', 'BusinessController@profitLossFilter')->name('profit-loss-filter');


    Route::post('remarket', 'BusinessController@remarket')->name('remarket');
    Route::get('insights', 'BusinessController@insights')->name('insights');

    Route::post('dashboard-filter', 'BusinessController@dashboardFilter')->name('dashboard-filter');

    /**
     * Income graphs
     */
    Route::get('income-graph', 'BusinessController@graphIncome')->name('income-graph');
    Route::post('income-graph-filter', 'BusinessController@graphIncomeFilter')->name('income-filter');

    /**
     * Expense graphs
     */
    Route::get('expense-graph', 'BusinessController@graphExpense')->name('expense-graph');
    Route::post('expense-graph-filter', 'BusinessController@graphExpenseFilter')->name('expense-filter');

    /**
     * Customers graphs
     */
    Route::get('customers-graph', 'BusinessController@graphCustomers')->name('customers-graph');
    Route::post('customers-graph-filter', 'BusinessController@graphCustomersFilter')->name('customers-filter');

    /**
     * Stylist certificates
     */
    Route::post('certificates', 'BusinessController@certificates')->name('stylist_certificates');
    Route::get('certificates-load/{id}', 'BusinessController@certificatesLoad')->name('certificates');
    Route::get('certificates-delete/{id}', 'BusinessController@certificatesDelete')->name('certificates.delete');
    Route::get('certificates-table/{id}', 'BusinessController@certificatesTable')->name('certificates.table');

    /**
     * Customer insights filter
     */
    Route::post('insights-filter', 'BusinessController@insightsFilter')->name('insights.filter');
});
