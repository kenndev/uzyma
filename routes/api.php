<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//sign in
Route::post('/uz/sign-in','MobileController@signIn');
//sign up
Route::post('/uz/sign-up','MobileController@signUp');
//Update user info database
Route::post('/uz/updateUser','MobileController@updateUser');
//Upload profile pic
Route::post('/uz/uploadProfilePic','MobileController@updateProfilePic');
//Get services
Route::get('/uz/get-services','MobileController@getActivities');
//Get Activities
Route::get('/uz/get-activities','MobileController@getActivities');
//Check out
Route::post('/uz/check-out','MobileController@checkOut2');
//Mpesa checkout
Route::post('/uz/check-out-mpesa','MobileController@checkOut3');
//Get Activities users has checked out on
Route::post('/uz/getActivityUser','MobileController@getActivityUser');
Route::post('/uz/getScedule','MobileController@getActivityUser');
//Activies filter
Route::post('/uz/filter','MobileController@filter');
//Activities filter time
Route::post('/uz/filter-time','MobileController@filterTime');
//Activity schedule by date and firebase
Route::post('/uz/filter-sh','MobileController@filterSchedule');
//Route::get('/uz/slider','MobileController@slider');
Route::post('/uz/slider','MobileController@getProductImages');

Route::get('/uz/products','MobileController@getProducts');

Route::post('/uz/help','MobileController@helpCentre');

Route::get('/uz/help-get-support','MobileController@helpCentre2');

Route::post('/uz/transactions','MobileController@transactions');

Route::post('/uz/save-payment','MobileController@savePayment');

Route::post('/uz/stkpush', 'PaymentController@stkpush');

Route::post('/uz/callback', 'PaymentController@callback');

Route::post('/uz/national-id','MobileController@saveNationalId');

Route::post('/uz/get-notifications','MobileController@getNotifications');

Route::post('uz/get-shades','MobileController@getShades');

Route::post('uz/get-rate','MobileController@getRate');

Route::post('uz/add-rate','MobileController@Rate');

Route::post('uz/get-stylist','MobileController@getStylistClient');





/********************************************************* Client Activity **************************************************/
//post client activity
Route::post('/uz/clientactivity', 'MobileController@customersAddEdit');
//signin Stylist
Route::post('/uz/sign-in-stylist','MobileController@signInStylist');
//Update Stylist
Route::post('/uz/updateStylist','MobileController@updateUserStylist');
//Upload profile pic Stylist
Route::post('/uz/uploadProfilePicStylist','MobileController@updateProfilePicStylist');
//get stylist data
Route::post('/uz/manager-activity','MobileController@stylistActivity');

//Get Stylist
Route::post('/uz/stylist','MobileController@getStylist');

Route::post('/uz/get-money','MobileController@getMoney');

Route::post('/uz/get-money-today','MobileController@getMoneyToday');

Route::get('/uz/customer-types','MobileController@customerTypes');

Route::post('/uz/search-customer','MobileController@searchCustomer');


/********************************************************* Client Activity **************************************************/
//PAYMENT
Route::get('/uz/mpesaAuth', 'ApiController@mpesaOAUTH');

Route::get('/uz/savePayment', 'ApiController@savePayment');

Route::get('/uz/saveTxt', 'ApiController@saveTxt');

Route::post('/uz/stkpush', 'PaymentController@stkpush');

Route::post('/uz/callback', 'PaymentController@callback');

Route::post('/uz/confirm', 'PaymentController@c2b_confirm_url');

Route::post('/uz/validate', 'PaymentController@c2b_validate_url');

Route::get('/uz/c2b', 'PaymentController@c2b');

Route::get('/uz/c2b_register_url', 'PaymentController@c2b_register_url');

Route::get('/uz/testuser', 'PaymentController@testAppUser');