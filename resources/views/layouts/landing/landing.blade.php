<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{getcong('site_name')}}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="{{asset('packages/landing/images/apple-touch-icon.png')}}">
    <link rel="shortcut icon" type="image/ico" href="{{asset('packages/landing/images/favicon.ico')}}" />

    <!-- Plugin-CSS -->
    <link rel="stylesheet" href="{{asset('packages/landing/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('packages/landing/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('packages/landing/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('packages/landing/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('packages/landing/css/magnific-popup.css')}}">

    <!-- Main-Stylesheets -->
    <link rel="stylesheet" href="{{asset('packages/landing/css/space.css')}}">
    <link rel="stylesheet" href="{{asset('packages/landing/css/theme.css')}}">
    <link rel="stylesheet" href="{{asset('packages/landing/css/overright.css')}}">
    <link rel="stylesheet" href="{{asset('packages/landing/css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('packages/landing/style.css')}}">
    <link rel="stylesheet" href="{{asset('packages/landing/css/responsive.css')}}">
    <script src="{{asset('packages/landing/js/vendor/modernizr-2.8.3.min.js')}}"></script>
</head>

<body data-spy="scroll" data-target="#mainmenu" data-offset="50">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="preloade">
    <span><i class="ti-mobile"></i></span>
</div>

<!--Header-Area-->
<header class="blue-bg relative fix" id="home">
    <div class="section-bg overlay-bg dewo ripple"></div>
    <div class="particles-js" id="particles-js"></div>
    <!--Mainmenu-->
    <nav class="navbar navbar-default mainmenu-area navbar-fixed-top" data-spy="affix" data-offset-top="60">
        <div class="container">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" class="navbar-toggle" data-target="#mainmenu">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand">
                    <!--<img src="images/logo.png" alt="">-->
                    <h2 class="text-white logo-text">appro</h2>
                </a>
            </div>
            <div class="collapse navbar-collapse navbar-right" id="mainmenu">
                <ul class="nav navbar-nav">
                    <li><a href="#home">Home</a></li>
                    <li><a href="#work">Work</a></li>
                    <li><a href="#feature">Features</a></li>
                    <li><a href="#team">Team</a></li>
                    <li><a href="#client">Client</a></li>
                    <li><a href="#price">Pricing</a></li>
                    <li><a href="#blog">Blog</a></li>
                    <li><a href="#contact">Contact</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!--Mainmenu/-->
    <div class="space-100"></div>
    <div class="space-20 hidden-xs"></div>

</header>
<!--Header-Area/-->

<!--Map/-->
<!--Footer-area-->
<footer class="black-bg">
    <div class="container">
        <div class="row">
            <div class="offset-top">
                <div class="col-xs-12 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                    <div class="well well-lg">
                        <h3>Get in Touch</h3>
                        <div class="space-20"></div>
                        <form action="process.php" id="contact-form" method="post">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="form-name" class="sr-only">Name</label>
                                        <input type="text" class="form-control" id="form-name" name="form-name" placeholder="Name" required>
                                    </div>
                                    <div class="space-10"></div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="form-email" class="sr-only">Email</label>
                                        <input type="email" class="form-control" id="form-email" name="form-email" placeholder="Email" required>
                                    </div>
                                    <div class="space-10"></div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="form-subject" class="sr-only">Email</label>
                                        <input type="text" class="form-control" id="form-subject" name="form-subject" placeholder="Subject" required>
                                    </div>
                                    <div class="space-10"></div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="form-message" class="sr-only">comment</label>
                                        <textarea class="form-control" rows="6" id="form-message" name="form-message" placeholder="Message" required></textarea>
                                    </div>
                                    <div class="space-10"></div>
                                    <button class="btn btn-link no-round text-uppercase" type="submit">Send message</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                    <div class="well well-lg">
                        <h3>Address</h3>
                        <div class="space-20"></div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis ab quia officia, minus obcaecati corporis! Tenetur tempore, inventore cum sapiente minima accusantium illo animi, doloribus rerum deleniti cumque, consequatur eaque in unde facilis consectetur, eius eligendi nostrum. Facilis, recusandae, eos!</p>
                        <div class="space-25"></div>
                        <table class="table">
                            <tbody>
                            <tr>
                                <td>
                                    <div class="border-icon sm"><span class="ti-headphone"></span></div>
                                </td>
                                <td><a href="callto:+0044545989626">+0044 545 989 626</a></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="border-icon sm"><span class="ti-email"></span></div>
                                </td>
                                <td>
                                    <a href="mailto:marveltheme@gmail.com">marveltheme@gmail.com</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="border-icon sm"><span class="ti-location-pin"></span></div>
                                </td>
                                <td>
                                    <address>28 Green Tower, Street Name New York City, USA</address>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="space-80"></div>
        <div class="row text-white wow fadeInUp">
            <div class="col-xs-12 col-md-6 col-md-offset-3">
                <h3 class="text-uppercase text-center">SubscribE OUR NEWSLETTER</h3>
                <div class="space-15"></div>
                <form id="mc-form" class="subscrie-form">
                    <label class="mt10" for="mc-email"></label>
                    <div class="input-group">
                        <input type="email" class="form-control" id="mc-email" placeholder="Your email address here...">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-info">Subscribe</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="space-80"></div>
        <div class="row text-white wow fadeIn">
            <div class="col-xs-12 text-center">
                <div class="social-menu">
                    <a href="#"><span class="ti-facebook"></span></a>
                    <a href="#"><span class="ti-twitter-alt"></span></a>
                    <a href="#"><span class="ti-linkedin"></span></a>
                    <a href="#"><span class="ti-pinterest-alt"></span></a>
                </div>
                <div class="space-20"></div>
                <p>@ 2017 <a href="https://themeforest.net/user/themectg">ThemeCTG</a> all right resurved. Designed by <a href="https://themeforest.net/user/quomodotheme">Quomodotheme</a></p>
            </div>
        </div>
        <div class="space-20"></div>
    </div>
</footer>
<!--Footer-area-->

<!--Vendor JS-->
<script src="{{asset('packages/landing/js/vendor/jquery-1.12.4.min.js')}}"></script>
<script src="{{asset('packages/landing/js/vendor/bootstrap.min.js')}}"></script>
<!--Plugin JS-->
<script src="{{asset('packages/landing/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('packages/landing/js/scrollUp.min.js')}}"></script>
<script src="{{asset('packages/landing/js/magnific-popup.min.js')}}"></script>
<script src="{{asset('packages/landing/js/ripples-min.js')}}"></script>
<script src="{{asset('packages/landing/js/contact-form.js')}}"></script>
<script src="{{asset('packages/landing/js/spectragram.min.js')}}"></script>
<script src="{{asset('packages/landing/js/particles.min.js')}}"></script>
<script src="{{asset('packages/landing/js/particles-app.js')}}"></script>
<script src="{{asset('packages/landing/js/ajaxchimp.js')}}"></script>
<script src="{{asset('packages/landing/js/wow.min.js')}}"></script>
<script src="{{asset('packages/landing/js/plugins.js')}}"></script>
<!--Active JS-->
<script src="{{asset('packages/landing/js/main.js')}}"></script>
<!--Maps JS-->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTS_KEDfHXYBslFTI_qPJIybDP3eceE-A&sensor=false"></script>
<script src="{{asset('packages/landing/js/maps.js')}}"></script>
</body>

</html>