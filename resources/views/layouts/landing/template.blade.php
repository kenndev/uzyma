<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{getcong('site_name')}}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="{{asset('packages/landing/images/favicon-32x32.png')}}">
    <link rel="shortcut icon" type="image/ico" href="{{asset('packages/landing/images/favicon-32x32.png')}}" />

    <!-- Plugin-CSS -->
    <link rel="stylesheet" href="{{asset('packages/landing/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('packages/landing/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('packages/landing/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('packages/landing/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('packages/landing/css/magnific-popup.css')}}">

    <!-- Main-Stylesheets -->
    <link rel="stylesheet" href="{{asset('packages/landing/css/space.css')}}">
    <link rel="stylesheet" href="{{asset('packages/landing/css/theme.css')}}">
    <link rel="stylesheet" href="{{asset('packages/landing/css/overright.css')}}">
    <link rel="stylesheet" href="{{asset('packages/landing/css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('packages/landing/style.css')}}">
    <link rel="stylesheet" href="{{asset('packages/landing/css/responsive.css')}}">
    <script src="{{asset('packages/landing/js/vendor/modernizr-2.8.3.min.js')}}"></script>
</head>

<body data-spy="scroll" data-target="#mainmenu" data-offset="50">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="preloade">
    <span><i class="ti-mobile"></i></span>
</div>

<!--Header-Area-->
<header class="blue-bg relative fix" id="home">
    <div class="section-bg overlay-bg dewo ripple"></div>
    <div class="particles-js" id="particles-js"></div>
    <!--Mainmenu-->
    <nav class="navbar navbar-default mainmenu-area navbar-fixed-top" data-spy="affix" data-offset-top="60">
        <div class="container">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" class="navbar-toggle" data-target="#mainmenu">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand">
                    <!--<img src="images/logo.png" alt="">-->
                    <h2 class="text-white logo-text">{{getcong('site_name')}}</h2>
                </a>
            </div>
            <div class="collapse navbar-collapse navbar-right" id="mainmenu">
                <ul class="nav navbar-nav">
                    <li><a href="#home">Home</a></li>
                    <li><a href="#work">Work</a></li>
                    <li><a href="#feature">Features</a></li>
                    <li><a href="#team">Team</a></li>
                    <li><a href="#contact">Contact</a></li>
                    <li><a href="{{route('business.register')}}">Become a partner</a> </li>
                </ul>
            </div>
        </div>
    </nav>
    <!--Mainmenu/-->
    <div class="space-100"></div>
    <div class="space-20 hidden-xs"></div>
    <!--Header-Text-->
    <div class="container text-white">
        <div class="row">
            <div class="col-xs-12 col-md-8">
                <div class="space-100"></div>
                <div class="home_screen_slide_main">
                    <div class="home_text_slide">
                        <div class="item">
                            <h1>Shouldn't it be easy?<br /></h1>
                            <div class="space-10"></div>
                            <p>Need someone really good to fix your hair? May be you want your make-up to be super on-point for that wedding you are attending? Or a judgement-free place to work out with your friends?</p>
                            <div class="space-50"></div>
                            <a target="_blank" href="https://play.google.com/store/apps/details?id=com.uzyma&hl=en" class="big-button">
                        <span class="big-button-icon">
                            <span class="ti-android"></span>
                        </span>
                                <span>available on</span>
                                <br>
                                <strong>Play store</strong>
                            </a>
                            {{--<a href="https://www.youtube.com/watch?v=IvtO37rSdB0" class="btn btn-icon video-popup"><span class="ti-control-play"></span>Watch Video</a>--}}
                        </div>
                        <div class="item">
                            <h1>Your lifestyle made easy<br /></h1>
                            <div class="space-10"></div>
                            <p>Uzyma is a lifestyle services marketplace focused on providing users with refreshing weekend experiences by providing access to high quality peer-reviewed fitness, beauty professionals and thrilling adventures.</p>
                            <div class="space-50"></div>
                            <a target="_blank" href="https://play.google.com/store/apps/details?id=com.uzyma&hl=en" class="big-button">
                        <span class="big-button-icon">
                            <span class="ti-android"></span>
                        </span>
                                <span>available on</span>
                                <br>
                                <strong>Play store</strong>
                            </a>

                        </div>

                    </div>
                </div>
            </div>
            <div class="hidden-xs hidden-sm col-md-4">
                <div class="home_screen_slide">
                    <div class="single_screen_slide wow fadeInRight">
                        <div class="item"><img src="{{asset('packages/landing/images/screen/Sign-in-3.png')}}" alt=""></div>
                        <div class="item"><img src="{{asset('packages/landing/images/screen/screen10.jpeg')}}" alt=""></div>
                    </div>
                </div>
                <div class="home_screen_nav">
                    <span class="ti-angle-left testi_prev"></span>
                    <span class="ti-angle-right testi_next"></span>
                </div>
            </div>
        </div>
        <div class="space-80"></div>
    </div>
    <!--Header-Text/-->
</header>
<!--Header-Area/-->

{{--<section>--}}
    {{--<div class="space-80"></div>--}}
    {{--<div class="container">--}}
        {{--<div class="row">--}}
            {{--<div class="col-xs-12 col-md-4 wow fadeInUp" data-wow-delay="0.2s">--}}
                {{--<div class="well well-hover text-center">--}}
                    {{--<p class="md-icon"><span class="ti-paint-bucket"></span></p>--}}
                    {{--<div class="space-10"></div>--}}
                    {{--<h5 class="text-uppercase">Easy to use</h5>--}}
                    {{--<p>Lorem ipsum dolor sit amt, consectet adop adipisicing elit, sed do eiusmod tepo raraincididunt ugt labore.</p>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-xs-12 col-md-4 wow fadeInUp" data-wow-delay="0.4s">--}}
                {{--<div class="well well-hover text-center">--}}
                    {{--<p class="md-icon"><span class="ti-cup"></span></p>--}}
                    {{--<div class="space-10"></div>--}}
                    {{--<h5 class="text-uppercase">Awesoem Design</h5>--}}
                    {{--<p>Lorem ipsum dolor sit amt, consectet adop adipisicing elit, sed do eiusmod tepo raraincididunt ugt labore.</p>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-xs-12 col-md-4 wow fadeInUp" data-wow-delay="0.6s">--}}
                {{--<div class="well well-hover text-center">--}}
                    {{--<p class="md-icon"><span class="ti-headphone-alt"></span></p>--}}
                    {{--<div class="space-10"></div>--}}
                    {{--<h5 class="text-uppercase">Easy to customaize</h5>--}}
                    {{--<p>Lorem ipsum dolor sit amt, consectet adop adipisicing elit, sed do eiusmod tepo raraincididunt ugt labore.</p>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="space-80"></div>--}}
{{--</section>--}}
<!--Work-Section-->
<section class="gray-bg" id="work">
    <div class="space-80"></div>
    <div class="container">
        <div class="row wow fadeInUp">
            <div class="col-xs-12 col-md-6 col-md-offset-3 text-center">
                <h3 class="text-uppercase">How it works?</h3>

            </div>
        </div>
        <div class="space-60"></div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3 text-center wow fadeInUp" data-wow-delay="0.2s">
                <div class="hover-shadow">
                    <div class="space-60">
                        <img src="{{asset('packages/landing/images/icon/icon1.png')}}" alt="">
                    </div>
                    <div class="space-20"></div>
                    <h5 class="text-uppercase">SIGN UP</h5>
                    <p>Sign up to find awesome peer-reviewed beauty, fitness & adveture services that suit you and your friends.</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 text-center wow fadeInUp" data-wow-delay="0.4s">
                <div class="hover-shadow">
                    <div class="space-60">
                        <img src="{{asset('packages/landing/images/icon/icon2.png')}}" alt="">
                    </div>
                    <div class="space-20"></div>
                    <h5 class="text-uppercase">Browse Multiple Services</h5>
                    <p>Scan through a wide range of listed services picking what suits you based on time, location and price.</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 text-center wow fadeInUp" data-wow-delay="0.6s">
                <div class="hover-shadow">
                    <div class="space-60">
                        <img src="{{asset('packages/landing/images/icon/icon3.png')}}" alt="">
                    </div>
                    <div class="space-20"></div>
                    <h5 class="text-uppercase">Book & Pay</h5>
                    <p>Lock it in! Schedule an appointment, enjoy the service then securely pay within the app</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 text-center wow fadeInUp" data-wow-delay="0.8s">
                <div class="hover-shadow">
                    <div class="space-60">
                        <img src="{{asset('packages/landing/images/icon/icon4.png')}}" alt="">
                    </div>
                    <div class="space-20"></div>
                    <h5 class="text-uppercase">Leave A Review</h5>
                    <p>Be a super hero! Let others know about your experience, what you liked about it, and what can be improved to make it even more worthwhile.</p>
                </div>
            </div>
        </div>
        <div class="space-60"></div>
        <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2 text-center wow fadeInUp">
                <div class="down-offset ">
                    <img src="{{asset('packages/landing/images/uzymabg.png')}}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!--Work-Section/-->
<!--Feature-Section-->
<section class="fix">
    <div class="space-60"></div>
    <div class="container" id="feature">
        <div class="space-100"></div>
        <div class="row wow fadeInUp">
            <div class="col-xs-12 col-md-6 col-md-offset-3 text-center">
                <h3 class="text-uppercase">Features</h3>

            </div>
        </div>
        <div class="space-60"></div>
        <div class="row feature-area">
            <div class="col-xs-12 col-sm-6 col-md-4 wow fadeInLeft">
                <div class="space-30"></div>
                <a href="#feature1" data-toggle="tab">
                    <div class="media single-feature">
                        <div class="media-body text-right">
                            <h5>PEER-REVIEWED SERVICES </h5>
                            <p>Search through a wide selection of peer-reviewed and rated services and classes. We shall always make sure you get value for your money and unmatched high quality service from our professional partners.</p>
                        </div>
                        <div class="media-right">
                            <div class="border-icon">
                                <span class="ti-light-bulb"></span>
                            </div>
                        </div>
                    </div>
                </a>
                <div class="space-30"></div>
                <a href="#feature2" data-toggle="tab">
                    <div class="media single-feature">
                        <div class="media-body text-right">
                            <h5>MULTIPLE SERVICE CATEGORIES</h5>
                            <p>How about that one-stop shop? We link you up to our pros who offer services in beauty, fitness and adventure because we know you truly want to enjoy a balanced lifestyle.</p>
                        </div>
                        <div class="media-right">
                            <div class="border-icon">
                                <span class="ti-cup"></span>
                            </div>
                        </div>
                    </div>
                </a>
                <div class="space-30"></div>

            </div>
            <div class="hidden-xs hidden-sm col-md-4 text-center fix wow fadeIn">
                <div class="down-offset relative ">
                    <img src="{{asset('packages/landing/images/mobile2.png')}}" alt="">
                    <div class="screen_image tab-content">
                        <div id="feature1" class="tab-pane fade in active">
                            <img src="{{asset('packages/landing/images/screen/screen1.jpeg')}}" alt="">
                        </div>
                        <div id="feature2" class="tab-pane fade">
                            <img src="{{asset('packages/landing/images/screen/screen2.jpeg')}}" alt="">
                        </div>
                        <div id="feature3" class="tab-pane fade">
                            <img src="{{asset('packages/landing/images/screen/screen3.jpeg')}}" alt="">
                        </div>
                        <div id="feature4" class="tab-pane fade">
                            <img src="{{asset('packages/landing/images/screen/screen4.jpeg')}}" alt="">
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 wow fadeInRight">
                <div class="space-30"></div>
                <a href="#feature4" data-toggle="tab">
                    <div class="media single-feature">
                        <div class="media-left">
                            <div class="border-icon">
                                <span class="ti-eye"></span>
                            </div>
                        </div>
                        <div class="media-body">
                            <h5>BOOK & SCHEDULE</h5>
                            <p>Easly and conveniently book and schedule an appointment with a pro. Pay for your service within the app and leave a review with our seamless checkout process that generates receipts to help you keep track of your expenses.</p>
                        </div>
                    </div>
                </a>
                <div class="space-30"></div>
                <a href="#feature5" data-toggle="tab">
                    <div class="media single-feature">
                        <div class="media-left">
                            <div class="border-icon">
                                <span class="ti-shine"></span>
                            </div>
                        </div>
                        <div class="media-body">
                            <h5>CLEAN USER INTERFACE</h5>
                            <p>Intuitive and easy to navigate user interface</p>
                        </div>
                    </div>
                </a>
                <div class="space-30"></div>

            </div>
        </div>
    </div>
</section>
<!--Feature-Section-->
<!--Video-Section-->
<section class="section-video relative fix">
    <div class="section-bg overlay-bg alpha">
    </div>
    <video class="ivideo" id="cvideo">
        <source src="http://quomodosoft.com/videos/App_Reminisound_Promotion_Video.webm" type="video/webm">
        {{--<source src="https://www.youtube.com/watch?v=IvtO37rSdB0" type="video/webm">--}}
    </video>
    <div class="section-video-text text-center">
        <button type="button" id="vbutton" class="video-button"><span class="ti-control-play"></span></button>
        <div class="space-50"></div>
        <h2 class="text-white">Watch Video Demo</h2>
    </div>
</section>
<!--Video-Section/-->
<!--Screenshot-Section-->
<section>
    <div class="space-80"></div>
    <div class="container">
        <div class="row wow fadeInUp">
            <div class="col-xs-12 col-md-6 col-md-offset-3 text-center">
                <h3 class="text-uppercase">APP SCREENSHOTS</h3>

            </div>
        </div>
        <div class="space-60"></div>
        <div class="row wow fadeIn">
            <div class="col-xs-12">
                <div class="space-60"></div>
                <div class="list_screen_slide">
                    {{--<div class="item">--}}
                        {{--<a href="{{asset('packages/landing/images/screen/screen3.jpg')}}" class="work-popup">--}}
                            {{--<img src="{{asset('packages/landing/images/screen/Sign-in-3.png')}}" alt="">--}}
                        {{--</a>--}}
                    {{--</div>--}}
                    <div class="item">
                        <a href="{{asset('packages/landing/images/screen/screen1.jpeg')}}" class="work-popup">
                            <img src="{{asset('packages/landing/images/screen/screen1.jpeg')}}" alt="">
                        </a>
                    </div>
                    <div class="item">
                        <a href="{{asset('packages/landing/images/screen/screen2.jpeg')}}" class="work-popup">
                            <img src="{{asset('packages/landing/images/screen/screen2.jpeg')}}" alt="">
                        </a>
                    </div>
                    <div class="item">
                        <a href="{{asset('packages/landing/images/screen/screen3.jpeg')}}" class="work-popup">
                            <img src="{{asset('packages/landing/images/screen/screen3.jpeg')}}" alt="">
                        </a>
                    </div>
                    <div class="item">
                        <a href="{{asset('packages/landing/images/screen/screen4.jpeg')}}" class="work-popup">
                            <img src="{{asset('packages/landing/images/screen/screen4.jpeg')}}" alt="">
                        </a>
                    </div>

                    <div class="item">
                        <a href="{{asset('packages/landing/images/screen/screen6.jpeg')}}" class="work-popup">
                            <img src="{{asset('packages/landing/images/screen/screen6.jpeg')}}" alt="">
                        </a>
                    </div>
                    <div class="item">
                        <a href="{{asset('packages/landing/images/screen/screen7.jpeg')}}" class="work-popup">
                            <img src="{{asset('packages/landing/images/screen/screen7.jpeg')}}" alt="">
                        </a>
                    </div>
                    <div class="item">
                        <a href="{{asset('packages/landing/images/screen/screen8.jpeg')}}" class="work-popup">
                            <img src="{{asset('packages/landing/images/screen/screen8.jpeg')}}" alt="">
                        </a>
                    </div>
                    <div class="item">
                        <a href="{{asset('packages/landing/images/screen/screen9.jpeg')}}" class="work-popup">
                            <img src="{{asset('packages/landing/images/screen/screen9.jpeg')}}" alt="">
                        </a>
                    </div>
                    <div class="item">
                        <a href="{{asset('packages/landing/images/screen/screen10.jpeg')}}" class="work-popup">
                            <img src="{{asset('packages/landing/images/screen/screen10.jpeg')}}" alt="">
                        </a>
                    </div>
                    <div class="item">
                        <a href="{{asset('packages/landing/images/screen/screen11.jpeg')}}" class="work-popup">
                            <img src="{{asset('packages/landing/images/screen/screen11.jpeg')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="space-40"></div>
            </div>
        </div>
    </div>
    <div class="space-80"></div>
</section>
<!--Screenshot-Section/-->
<!--Team-Section-->
<section class="gray-bg" id="team">
    <div class="space-80"></div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6 col-md-offset-3 text-center">
                <h3 class="text-uppercase">The Team</h3>

            </div>
        </div>
        <div class="space-60"></div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3 wow fadeInUp" data-wow-delay="0.2s">
                <div class="single-team relative panel fix">
                    <img src="{{asset('packages/landing/images/team/team1.jpg')}}" alt="">
                    <div class="team_details text-center">
                        <h5 class="text-uppercase">VICTOR MOCHENGO</h5>
                        <p>Co. Founder</p>
                        <div class="social-menu">
                            <hr>
                            <a href="#"><span class="ti-facebook"></span></a>
                            <a href="#"><span class="ti-twitter-alt"></span></a>
                            <a href="#"><span class="ti-linkedin"></span></a>
                            <a href="#"><span class="ti-pinterest-alt"></span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 wow fadeInUp" data-wow-delay="0.4s">
                <div class="single-team relative panel fix">
                    <img src="{{asset('packages/landing/images/team/team2.jpg')}}" alt="">
                    <div class="team_details text-center">
                        <h5 class="text-uppercase">Deborah Brown</h5>
                        <p>UX Designer</p>
                        <div class="social-menu">
                            <hr>
                            <a href="#"><span class="ti-facebook"></span></a>
                            <a href="#"><span class="ti-twitter-alt"></span></a>
                            <a href="#"><span class="ti-linkedin"></span></a>
                            <a href="#"><span class="ti-pinterest-alt"></span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 wow fadeInUp" data-wow-delay="0.6s">
                <div class="single-team relative panel fix">
                    <img src="{{asset('packages/landing/images/team/team3.jpg')}}" alt="">
                    <div class="team_details text-center">
                        <h5 class="text-uppercase">Harry Banks</h5>
                        <p>Founder</p>
                        <div class="social-menu">
                            <hr>
                            <a href="#"><span class="ti-facebook"></span></a>
                            <a href="#"><span class="ti-twitter-alt"></span></a>
                            <a href="#"><span class="ti-linkedin"></span></a>
                            <a href="#"><span class="ti-pinterest-alt"></span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 wow fadeInUp" data-wow-delay="0.8s">
                <div class="single-team relative panel fix">
                    <img src="{{asset('packages/landing/images/team/team4.jpg')}}" alt="">
                    <div class="team_details text-center">
                        <h5 class="text-uppercase">Victoria Clark</h5>
                        <p>Creative Director</p>
                        <div class="social-menu">
                            <hr>
                            <a href="#"><span class="ti-facebook"></span></a>
                            <a href="#"><span class="ti-twitter-alt"></span></a>
                            <a href="#"><span class="ti-linkedin"></span></a>
                            <a href="#"><span class="ti-pinterest-alt"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="space-80"></div>
</section>
<!--Team-Section/-->
<!--Client-Section-->
{{--<section id="client">--}}
    {{--<div class="space-80"></div>--}}
    {{--<div class="container">--}}
        {{--<div class="row wow fadeInUp">--}}
            {{--<div class="col-xs-12 col-md-8 col-md-offset-2 text-center">--}}
                {{--<div class="well well-lg">--}}
                    {{--<div class="client-details-content">--}}
                        {{--<div class="client_details">--}}
                            {{--<div class="item">--}}
                                {{--<h3>M S NEWAZ</h3>--}}
                                {{--<p>Ceative Director</p>--}}
                                {{--<q>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incubt consectetur aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut com modo consequat. Duis aute irure dolor in reprehenderit.</q>--}}
                            {{--</div>--}}
                            {{--<div class="item">--}}
                                {{--<h3>M S NEWAZ</h3>--}}
                                {{--<p>Ceative Director</p>--}}
                                {{--<q>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incubt consectetur aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut com modo consequat. Duis aute irure dolor in reprehenderit.</q>--}}
                            {{--</div>--}}
                            {{--<div class="item">--}}
                                {{--<h3>M S NEWAZ</h3>--}}
                                {{--<p>Ceative Director</p>--}}
                                {{--<q>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incubt consectetur aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut com modo consequat. Duis aute irure dolor in reprehenderit.</q>--}}
                            {{--</div>--}}
                            {{--<div class="item">--}}
                                {{--<h3>M S NEWAZ</h3>--}}
                                {{--<p>Ceative Director</p>--}}
                                {{--<q>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incubt consectetur aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut com modo consequat. Duis aute irure dolor in reprehenderit.</q>--}}
                            {{--</div>--}}
                            {{--<div class="item">--}}
                                {{--<h3>M S NEWAZ</h3>--}}
                                {{--<p>Ceative Director</p>--}}
                                {{--<q>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incubt consectetur aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut com modo consequat. Duis aute irure dolor in reprehenderit.</q>--}}
                            {{--</div>--}}
                            {{--<div class="item">--}}
                                {{--<h3>M S NEWAZ</h3>--}}
                                {{--<p>Ceative Director</p>--}}
                                {{--<q>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incubt consectetur aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut com modo consequat. Duis aute irure dolor in reprehenderit.</q>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="space-80"></div>--}}
{{--</section>--}}
<!--Client-Section-->
<!--Price-section-->
{{--<section class="relative fix" id="price">--}}
    {{--<div class="section-bg overlay-bg fix">--}}
        {{--<img src="{{asset('packages/landing/images/bg2.jpg')}}" alt="">--}}
    {{--</div>--}}
    {{--<div class="space-80"></div>--}}
    {{--<div class="container">--}}
        {{--<div class="row wow fadeInUp">--}}
            {{--<div class="col-xs-12 col-md-6 col-md-offset-3 text-center text-white">--}}
                {{--<h3 class="text-uppercase">AFORTABLE PRICE</h3>--}}
                {{--<p>Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli incididunt ut labore Lorem ipsum madolor sit amet.</p>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="space-60"></div>--}}
        {{--<div class="row">--}}
            {{--<div class="col-xs-12 col-sm-4 wow fadeInLeft">--}}
                {{--<div class="panel price-table text-center">--}}
                    {{--<h3 class="text-uppercase price-title">Basic</h3>--}}
                    {{--<hr>--}}
                    {{--<div class="space-30"></div>--}}
                    {{--<ul class="list-unstyled">--}}
                        {{--<li><strong class="amount">&#36; <span class="big">20</span></strong>/Month</li>--}}
                        {{--<li>100 MB Disk Space</li>--}}
                        {{--<li>2 Subdomains</li>--}}
                        {{--<li>5 Email Accounts</li>--}}
                        {{--<li>Webmail Support</li>--}}
                        {{--<li>Customer Support 24/7</li>--}}
                    {{--</ul>--}}
                    {{--<div class="space-30"></div>--}}
                    {{--<hr>--}}
                    {{--<a href="#" class="btn btn-link text-uppercase">Purchase</a>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-xs-12 col-sm-4 wow flipInY">--}}
                {{--<div class="panel price-table active center text-center">--}}
                    {{--<h3 class="text-uppercase price-title">STABDARD</h3>--}}
                    {{--<hr>--}}
                    {{--<div class="space-30"></div>--}}
                    {{--<ul class="list-unstyled">--}}
                        {{--<li><strong class="amount">&#36; <span class="big">39</span></strong>/Month</li>--}}
                        {{--<li>100 MB Disk Space</li>--}}
                        {{--<li>2 Subdomains</li>--}}
                        {{--<li>5 Email Accounts</li>--}}
                        {{--<li>Webmail Support</li>--}}
                        {{--<li>Customer Support 24/7</li>--}}
                    {{--</ul>--}}
                    {{--<div class="space-30"></div>--}}
                    {{--<hr>--}}
                    {{--<a href="#" class="btn btn-link text-uppercase">Purchase</a>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-xs-12 col-sm-4 wow fadeInRight">--}}
                {{--<div class="panel price-table text-center">--}}
                    {{--<h3 class="text-uppercase price-title">UNLIMITED</h3>--}}
                    {{--<hr>--}}
                    {{--<div class="space-30"></div>--}}
                    {{--<ul class="list-unstyled">--}}
                        {{--<li><strong class="amount">&#36; <span class="big">59</span></strong>/Month</li>--}}
                        {{--<li>100 MB Disk Space</li>--}}
                        {{--<li>2 Subdomains</li>--}}
                        {{--<li>5 Email Accounts</li>--}}
                        {{--<li>Webmail Support</li>--}}
                        {{--<li>Customer Support 24/7</li>--}}
                    {{--</ul>--}}
                    {{--<div class="space-30"></div>--}}
                    {{--<hr>--}}
                    {{--<a href="#" class="btn btn-link text-uppercase">Purchase</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="space-80"></div>--}}
{{--</section>--}}
<!--Price-section/-->
<!--Question-section-->
{{--<section class="fix">--}}
    {{--<div class="space-80"></div>--}}
    {{--<div class="container">--}}
        {{--<div class="row wow fadeInUp">--}}
            {{--<div class="col-xs-12 col-md-6 col-md-offset-3 text-center">--}}
                {{--<h3 class="text-uppercase">Frequently asked questions</h3>--}}
                {{--<p>Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli incididunt ut labore Lorem ipsum madolor sit amet.</p>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="space-60"></div>--}}
        {{--<div class="row">--}}
            {{--<div class="col-xs-12 col-md-6 wow fadeInUp">--}}
                {{--<div class="space-60"></div>--}}
                {{--<div class="panel-group" id="accordion">--}}
                    {{--<div class="panel panel-default">--}}
                        {{--<div class="panel-heading">--}}
                            {{--<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Sedeiusmod tempor inccsetetur aliquatraiy? </a></h4>--}}
                        {{--</div>--}}
                        {{--<div id="collapse1" class="panel-collapse collapse in">--}}
                            {{--<div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodas temporo incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrd exercitation ullamco laboris nisi ut aliquip ex comodo consequat. Duis aute dolor in reprehenderit.</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="panel panel-default">--}}
                        {{--<div class="panel-heading">--}}
                            {{--<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Tempor inccsetetur aliquatraiy?</a></h4>--}}
                        {{--</div>--}}
                        {{--<div id="collapse2" class="panel-collapse collapse">--}}
                            {{--<div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="panel panel-default">--}}
                        {{--<div class="panel-heading">--}}
                            {{--<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Lorem ipsum dolor amet, consectetur adipisicing ?</a></h4>--}}
                        {{--</div>--}}
                        {{--<div id="collapse3" class="panel-collapse collapse">--}}
                            {{--<div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="panel panel-default">--}}
                        {{--<div class="panel-heading">--}}
                            {{--<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse4">Lorem ipsum dolor amet, consectetur adipisicing ?</a></h4>--}}
                        {{--</div>--}}
                        {{--<div id="collapse4" class="panel-collapse collapse">--}}
                            {{--<div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="hidden-xs hidden-sm col-md-5 col-md-offset-1 wow fadeInRight ">--}}
                {{--<img src="{{asset('packages/landing/images/2mobile.png')}}" alt="">--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="space-80"></div>--}}
{{--</section>--}}
<!--Question-section/-->
<!--Download-Section-->
<section class="relative fix">
    <div class="space-80"></div>
    <div class="section-bg overlay-bg">
        <img src="{{asset('packages/landing/images/uzymabg.png')}}" alt="">
    </div>
    <div class="container">
        <div class="row wow fadeInUp">
            <div class="col-xs-12 col-md-6 col-md-offset-3 text-center text-white">
                <h3 class="text-uppercase">Download Uzyma</h3>
                <p>You should have it all so easy! Let us get you started</p>
            </div>
        </div>
        <div class="space-60"></div>
        <div class="row text-white wow fadeInUp">
            <div class="col-xs-12 col-sm-4 col-lg-offset-4">
                <a target="_blank" href="https://play.google.com/store/apps/details?id=com.uzyma&hl=en" class="big-button aligncenter">
                        <span class="big-button-icon">
                            <span class="ti-android"></span>
                        </span>
                    <span>available on</span>
                    <br>
                    <strong>Play store</strong>
                </a>
                <div class="space-10"></div>
            </div>

        </div>
    </div>
    <div class="space-80"></div>
</section>
<!--Download-Section/-->
<!--Blog-Section-->
{{--<section id="blog">--}}
    {{--<div class="space-80"></div>--}}
    {{--<div class="container">--}}
        {{--<div class="row wow fadeInUp">--}}
            {{--<div class="col-xs-12 col-md-6 col-md-offset-3 text-center">--}}
                {{--<h3 class="text-uppercase">LATEST FROM BLOG</h3>--}}
                {{--<p>Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli incididunt ut labore Lorem ipsum madolor sit amet.</p>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="space-60"></div>--}}
        {{--<div class="row">--}}
            {{--<div class="col-xs-12 col-sm-6 col-md-4 wow fadeInUp" data-wow-delay="0.2s">--}}
                {{--<div class="panel text-center single-blog">--}}
                    {{--<img src="{{asset('packages/landing/images/blog/blog1.jpg')}}" class="img-full" alt="">--}}
                    {{--<div class="padding-20">--}}
                        {{--<ul class="list-unstyled list-inline">--}}
                            {{--<li><span class="ti-user"></span> By: Admin</li>--}}
                            {{--<li><span class="ti-calendar"></span> Feb 01, 2017</li>--}}
                        {{--</ul>--}}
                        {{--<div class="space-10"></div>--}}
                        {{--<a href="blog-details-left-sidebar.html"><h3>Beautiful Place for your Great Journey</h3></a>--}}
                        {{--<div class="space-15"></div>--}}
                        {{--<p>Lorem dolor sit amet, consectetur floralm adipisicing elit, sed do eiusmod tem aincididunt elauta labore eta dolore magna aliqualy eminem faenimve...</p>--}}
                        {{--<div class="space-20"></div>--}}
                        {{--<a href="blog-details-right-sidebar.html" class="btn btn-link">Read more</a>--}}
                        {{--<div class="space-20"></div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-xs-12 col-sm-6 col-md-4 wow fadeInUp" data-wow-delay="0.4s">--}}
                {{--<div class="panel text-center single-blog">--}}
                    {{--<img src="{{asset('packages/landing/images/blog/blog2.jpg')}}" class="img-full" alt="">--}}
                    {{--<div class="padding-20">--}}
                        {{--<ul class="list-unstyled list-inline">--}}
                            {{--<li><span class="ti-user"></span> By: Admin</li>--}}
                            {{--<li><span class="ti-calendar"></span> Feb 01, 2017</li>--}}
                        {{--</ul>--}}
                        {{--<div class="space-10"></div>--}}
                        {{--<a href="blog-details-right-sidebar.html"><h3>Beautiful Place for your Great Journey</h3></a>--}}
                        {{--<div class="space-15"></div>--}}
                        {{--<p>Lorem dolor sit amet, consectetur floralm adipisicing elit, sed do eiusmod tem aincididunt elauta labore eta dolore magna aliqualy eminem faenimve...</p>--}}
                        {{--<div class="space-20"></div>--}}
                        {{--<a href="blog-details-right-sidebar.html" class="btn btn-link">Read more</a>--}}
                        {{--<div class="space-20"></div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-xs-12 col-sm-6 col-md-4 wow fadeInUp" data-wow-delay="0.6s">--}}
                {{--<div class="panel text-center single-blog">--}}
                    {{--<img src="{{asset('packages/landing/images/blog/blog3.jpg')}}" class="img-full" alt="">--}}
                    {{--<div class="padding-20">--}}
                        {{--<ul class="list-unstyled list-inline">--}}
                            {{--<li><span class="ti-user"></span> By: Admin</li>--}}
                            {{--<li><span class="ti-calendar"></span> Feb 01, 2017</li>--}}
                        {{--</ul>--}}
                        {{--<div class="space-10"></div>--}}
                        {{--<a href="blog-details-left-sidebar.html"><h3>Beautiful Place for your Great Journey</h3></a>--}}
                        {{--<div class="space-15"></div>--}}
                        {{--<p>Lorem dolor sit amet, consectetur floralm adipisicing elit, sed do eiusmod tem aincididunt elauta labore eta dolore magna aliqualy eminem faenimve...</p>--}}
                        {{--<div class="space-20"></div>--}}
                        {{--<a href="blog-details-right-sidebar.html" class="btn btn-link">Read more</a>--}}
                        {{--<div class="space-20"></div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="row">--}}
            {{--<div class="space-60"></div>--}}
            {{--<div class="col-xs-12 text-center">--}}
                {{--<a href="blog.html" class="btn btn-link active">View All</a>--}}
            {{--</div>--}}
            {{--<div class="space-60"></div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</section>--}}
<!--Blog-Section/-->
<hr>
<!--instagram section-->
<div class="container">
    <div class="space-80"></div>
    <div class="row">
        <div class="col-xs-12">
            <ul class="instagram instagram-slide list-unstyle list-inline"></ul>
            <div class="space-80"></div>
        </div>
    </div>
</div>
<!--instagram section/-->
<!--Map-->
<div id="contact"></div>
<div id="maps"></div>
<!--Map/-->
<!--Footer-area-->
<footer class="black-bg">
    <div class="container">
        <div class="row">
            <div class="offset-top">
                <div class="col-xs-12 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                    <div class="well well-lg">
                        <h3>Say something! Talk to us</h3>
                        <div class="space-20"></div>
                        <form action="process.php" id="contact-form" method="post">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="form-name" class="sr-only">Name</label>
                                        <input type="text" class="form-control" id="form-name" name="form-name" placeholder="Name" required>
                                    </div>
                                    <div class="space-10"></div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="form-email" class="sr-only">Email</label>
                                        <input type="email" class="form-control" id="form-email" name="form-email" placeholder="Email" required>
                                    </div>
                                    <div class="space-10"></div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="form-subject" class="sr-only">Email</label>
                                        <input type="text" class="form-control" id="form-subject" name="form-subject" placeholder="Subject" required>
                                    </div>
                                    <div class="space-10"></div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="form-message" class="sr-only">comment</label>
                                        <textarea class="form-control" rows="6" id="form-message" name="form-message" placeholder="Message" required></textarea>
                                    </div>
                                    <div class="space-10"></div>
                                    <button class="btn btn-link no-round text-uppercase" type="submit">Send message</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                    <div class="well well-lg">
                        <h3>Address</h3>
                        <div class="space-20"></div>

                        <div class="space-25"></div>
                        <table class="table">
                            <tbody>
                            <tr>
                                <td>
                                    <div class="border-icon sm"><span class="ti-headphone"></span></div>
                                </td>
                                <td><a href="callto:+254723649624">+254 723 649 624</a></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="border-icon sm"><span class="ti-email"></span></div>
                                </td>
                                <td>
                                    <a href="mailto:info@uzyma.com">info@uzyma.com</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="border-icon sm"><span class="ti-location-pin"></span></div>
                                </td>
                                <td>
                                    <address>6A Belgravia House, 14 Riverside, Nairobi, Kenya</address>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="space-80"></div>
        <div class="row text-white wow fadeInUp">
            <div class="col-xs-12 col-md-6 col-md-offset-3">
                <h3 class="text-uppercase text-center">Want to be first to know about discounts, sales and much more?</h3>
                <div class="space-15"></div>
                <form id="mc-form" class="subscrie-form">
                    <label class="mt10" for="mc-email"></label>
                    <div class="input-group">
                        <input type="email" class="form-control" id="mc-email" placeholder="Your email address here...">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-info">Subscribe</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="space-80"></div>
        <div class="row text-white wow fadeIn">
            <div class="col-xs-12 text-center">
                <div class="social-menu">
                    <a href="https://facebook.com/uzyma/" target="_blank"><span class="ti-facebook"></span></a>
                    <a href="https://instagram.com/uzyma_ke" target="_blank"><span class="ti-instagram"></span></a>
                    <a href="https://play.google.com/store/apps/details?id=com.uzyma&hl=en" target="_blank"><span class="ti-android"></span></a>

                </div>
                <div class="space-20"></div>
                <p>@ {{date('Y')}} Uzyma FitFab Ltd. All rights reserved. </p>
            </div>
        </div>
        <div class="space-20"></div>
    </div>
</footer>
<!--Footer-area-->

<script>
    var baseurl = "{{url('/')}}";
</script>

<!--Vendor JS-->
<script src="{{asset('packages/landing/js/vendor/jquery-1.12.4.min.js')}}"></script>
<script src="{{asset('packages/landing/js/vendor/bootstrap.min.js')}}"></script>
<!--Plugin JS-->
<script src="{{asset('packages/landing/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('packages/landing/js/scrollUp.min.js')}}"></script>
<script src="{{asset('packages/landing/js/magnific-popup.min.js')}}"></script>
<script src="{{asset('packages/landing/js/ripples-min.js')}}"></script>
<script src="{{asset('packages/landing/js/contact-form.js')}}"></script>
<script src="{{asset('packages/landing/js/spectragram.min.js')}}"></script>
<script src="{{asset('packages/landing/js/particles.min.js')}}"></script>
<script src="{{asset('packages/landing/js/particles-app.js')}}"></script>
<script src="{{asset('packages/landing/js/ajaxchimp.js')}}"></script>
<script src="{{asset('packages/landing/js/wow.min.js')}}"></script>
<script src="{{asset('packages/landing/js/plugins.js')}}"></script>
<!--Active JS-->
<script src="{{asset('packages/landing/js/main.js')}}"></script>
<!--Maps JS-->
<script src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyB8WgY93rMVkcDEn3Z64CUcSh3Jx_UMeH4"></script>
<script src="{{asset('packages/landing/js/maps.js')}}"></script>
</body>

</html>