<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ getcong('site_name') }}  - {{ isset($title) ? $title : null }}</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="{{asset('packages/backend/assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('packages/backend/assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('packages/backend/assets/css/core.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('packages/backend/assets/css/components.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('packages/backend/assets/css/colors.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('packages/backend/assets/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" media="screen">


        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="{{asset('packages/backend/assets/js/plugins/loaders/pace.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('packages/backend/assets/js/core/libraries/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('packages/backend/assets/js/core/libraries/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('packages/backend/assets/js/plugins/loaders/blockui.min.js')}}"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script type="text/javascript" src="{{asset('packages/backend/assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('packages/backend/assets/js/plugins/forms/selects/select2.min.js')}}"></script>

        <script type="text/javascript" src="{{asset('packages/backend/assets/js/core/app.js')}}"></script>


        <script type="text/javascript" src="{{asset('packages/backend/assets/js/plugins/ui/moment/moment.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('packages/backend/assets/js/plugins/pickers/daterangepicker.js')}}"></script>
        <script type="text/javascript" src="{{asset('packages/backend/assets/js/plugins/pickers/anytime.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('packages/backend/assets/js/plugins/pickers/pickadate/picker.js')}}"></script>
        <script type="text/javascript" src="{{asset('packages/backend/assets/js/plugins/pickers/pickadate/picker.date.js')}}"></script>
        <script type="text/javascript" src="{{asset('packages/backend/assets/js/plugins/pickers/pickadate/picker.time.js')}}"></script>
        <script type="text/javascript" src="{{asset('packages/backend/assets/js/plugins/pickers/pickadate/legacy.js')}}"></script>

        <!-- /theme JS files -->
        <script type="text/javascript" src="{{asset('packages/backend/assets/js/core/libraries/jquery_ui/interactions.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('packages/backend/assets/js/plugins/forms/selects/select2.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('packages/backend/assets/js/pages/form_select2.js')}}"></script>
        <script type="text/javascript" src="{{asset('packages/backend/assets/js/plugins/uploaders/fileinput.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('packages/backend/assets/js/pages/picker_date.js')}}"></script>


        <script type="text/javascript" src="{{asset('packages/backend/assets/js/pages/uploader_bootstrap.js')}}"></script>
        <script type="text/javascript" src="{{asset('packages/backend/assets/js/plugins/notifications/jgrowl.min.js')}}"></script>

<!--        <script type="text/javascript" src="{{asset('packages/backend/assets/js/plugins/forms/selects/bootstrap_select.min.js')}}"></script>


        <script type="text/javascript" src="{{asset('packages/backend/assets/js/pages/form_bootstrap_select.js')}}"></script>-->

        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

        <meta name="_token" content="{{ csrf_token() }}" />
         @stack('scripts')
        <script>
var base_url = '{{url("/")}}';

$(document).ready(function () {
    $(".status-progress").hide();
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });


});
function checkDelete()
{
    var chk = confirm("Are You Sure you want To Delete This !");
    if (chk)
    {
        return true;
    } else {
        return false;
    }
}
        </script>
        


    </head>

    <body>

        
        @include("layouts.backend.topbar")


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->
                        {{--<div class="sidebar-user">--}}
                            {{--<div class="category-content">--}}
                                {{--<div class="media">--}}
                                    {{--<a href="#" class="media-left"><img src="{{asset('packages/backend/assets/images/placeholder.jpg')}}" class="img-circle img-sm" alt=""></a>--}}
                                    {{--<div class="media-body">--}}
                                        {{--<span class="media-heading text-semibold">{{Auth::user()->name}}</span>--}}

                                    {{--</div>--}}

                                    {{--<div class="media-right media-middle">--}}
                                        {{--<ul class="icons-list">--}}
                                            {{--<li>--}}
                                                {{--<a href="#"><i class="icon-cog3"></i></a>--}}
                                            {{--</li>--}}
                                        {{--</ul>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <!-- /user menu -->


                        <!-- Main navigation -->
                        @include("layouts.backend.sidebar")
                        <!-- /main navigation -->

                    </div>
                </div>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    @yield("content")

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->
        <script type="text/javascript" src="{{asset('packages/backend/assets/js/bootstrap-datetimepicker.js')}}" charset="UTF-8"></script>



        <script type="text/javascript">

$('.form_date').datetimepicker({
    weekStart: 1,
    todayBtn: 1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    minView: 2,
    forceParse: 0
});
$('.form_date2').datetimepicker({
    weekStart: 1,
    todayBtn: 1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    minView: 2,
    forceParse: 0
});
$('.form_date3').datetimepicker({
    weekStart: 1,
    todayBtn: 1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    minView: 2,
    forceParse: 0
});

        </script>
       
    </body>

</html>
