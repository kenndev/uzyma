<div class="footer text-muted">
    &copy; {{date('Y')}}. <a href="#">{{ getcong('site_name') }}</a> by <a href="http://designlabtechnologies.com" target="_blank">205 Investments Limited</a>
</div>
