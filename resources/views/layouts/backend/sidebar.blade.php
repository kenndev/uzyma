<div class="sidebar-category sidebar-category-visible">
    <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">

        @if(Auth::user()->account_type == 221)
            <!-- Main -->
                <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                <li {{ isActiveRoute2('home') }}><a href="{{route('home')}}"><i class="icon-home4"></i>
                        <span>Dashboard</span></a></li>
                <li {{ isActiveRoute2('partners') }}><a href="{{route('partners')}}"><i
                                class="glyphicon glyphicon-user"></i> <span>Partners</span></a></li>
                <li {{ isActiveRoute2('admin.activity') }}><a href="{{route('admin.activity')}}"><i
                                class="glyphicon glyphicon-bullhorn"></i> <span>Activities</span></a></li>
                <li {{ isActiveRoute2('bookings') }}><a href="{{route('bookings')}}"><i
                                class="glyphicon glyphicon-asterisk"></i> <span>Bookings</span></a></li>
                <li {{ isActiveRoute2('slider') }}><a href="{{route('slider')}}"><i
                                class="glyphicon glyphicon-camera"></i> <span>Slider</span></a></li>
                <li {{ isActiveRoute2('product.sell.view') }}><a href="{{route('product.sell.view')}}"><i
                                class="icon-lifebuoy"></i> <span>Products</span></a></li>
                <li>
                    <a href="#"><i class="icon-cog3"></i> <span>Trivia</span></a>
                    <ul>

                        <li {{ isActiveRoute2('questions') }}><a href="{{route('questions')}}">Questions</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="icon-cog3"></i> <span>Settings</span></a>
                    <ul>

                        <li {{ isActiveRoute2('settings') }}><a href="{{route('settings')}}">Settings</a></li>
                        <li {{ isActiveRoute2('help.centre') }}><a href="{{route('help.centre')}}">Help centre</a></li>
                        <li {{ isActiveRoute2('business.types') }}><a href="{{route('business.types')}}">Business
                                types</a></li>
                        <li {{ isActiveRoute2('difficultyLevel.types') }}><a href="{{route('difficultyLevel.types')}}">Difficult
                                level</a></li>
                        <li {{ isActiveRoute2('subscribers') }}><a href="{{route('subscribers')}}">Subscribers</a></li>
                        <li {{ isActiveRoute2('users') }}><a href="{{route('users')}}">App Users</a></li>
                    </ul>
                </li>

            @elseif(Auth::user()->account_type == 101)
                <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                <li {{ isActiveRoute2('home.partners') }}><a href="{{route('home.partners')}}"><i
                                class="icon-home4"></i> <span>Dashboard</span></a></li>
                <li {{ isActiveRoute2('activity') }}><a href="{{route('activity')}}"><i
                                class="glyphicon glyphicon-star"></i> <span>Activity</span></a></li>
                <li {{ isActiveRoute2('bookings.partner') }}><a href="{{route('bookings.partner')}}"><i
                                class="glyphicon glyphicon-asterisk"></i> <span>Bookings</span></a></li>


                <li {{ isActiveRoute2('stylist') }}><a href="{{route('stylist')}}"><i
                                class="glyphicon glyphicon-star"></i> <span>Stylist</span></a></li>
                <li {{ isActiveRoute2('manager') }}><a href="{{route('manager')}}"><i
                                class="glyphicon glyphicon-check"></i> <span>Manager</span></a></li>
                {{--<li {{ isActiveRoute2('manager') }}><a href="{{route('manager')}}"><i--}}
                                {{--class="glyphicon glyphicon-check"></i> <span>Manager</span></a></li>--}}

                <li>
                    <a href="#"><i class="icon-cash"></i> <span>Stylist Activity</span></a>
                    <ul>

                        <li {{ isActiveRoute2('customers.activity') }}><a href="{{route('customers.activity')}}">Stylist
                                Activity | Income</a></li>
                        <li {{ isActiveRoute2('income.target') }}><a href="{{route('income.target')}}">Set income
                                target</a></li>
                        <li {{ isActiveRoute2('customers') }}><a href="{{route('customers')}}">Customers</a></li>

                    </ul>
                </li>
                <li>
                    <a href="#"><i class="icon-wallet"></i> <span>Expenses</span></a>
                    <ul>

                        <li {{ isActiveRoute2('expenses') }}><a href="{{route('expenses')}}">Expenses</a></li>
                        <li {{ isActiveRoute2('expenses.target') }}><a href="{{route('expenses.target')}}">Set expense
                                target</a></li>

                    </ul>
                </li>
                <li {{ isActiveRoute2('profit-loss') }}><a href="{{route('profit-loss')}}"><i
                                class="icon-vector"></i> <span>Profit Loss</span></a></li>
                <li {{ isActiveRoute2('insights') }}><a href="{{route('insights')}}"><i
                                class="icon-rulers"></i> <span>Customer Insights</span></a></li>
                <li>
                    <a href="#"><i class="icon-cog3"></i> <span>Settings</span></a>
                    <ul>

                        {{--<li {{ isActiveRoute2('other') }}><a href="{{route('other')}}">Tags</a></li>--}}
                        <li {{ isActiveRoute2('amenities') }}><a href="{{route('amenities')}}">Amenities</a></li>

                    </ul>
                </li>

            @elseif(Auth::user()->account_type == 333)
                <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                <li {{ isActiveRoute2('home.business') }}><a href="{{route('home.business')}}"><i
                                class="icon-home4"></i> <span>Dashboard</span></a></li>



            @endif

        </ul>
    </div>
</div>
