<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{ getcong('site_name') }}  - {{ isset($title) ? $title : null }}</title>

    <!-- Bootstrap core CSS -->

    <link href="{{asset('packages/frontenduz/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans"/>
    <link href="{{asset('packages/frontenduz/asset/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('packages/backend/assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">


    <!-- Custom styles for this template -->

</head>

<body>

<!-- Navigation -->
<nav class="uzyma-nav navbar navbar-expand-lg navbar-light bg-white fixed-top">
    <div class="container">
        <a class="navbar-brand home {{isActiveRoute3('root.home')}}" href="{{route('root.home')}}">Uzyma</a>


        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item {{isActiveRoute3('login')}}">
                    <a class="nav-link" href="{{route('login')}}">HOME

                    </a>
                </li>
                <li class="nav-item {{isActiveRoute3('privacy')}}">
                    <a class="nav-link" href="{{route('privacy')}}">PRIVACY POLICY

                    </a>
                </li>

                <li class="nav-item {{isActiveRoute3('business.register')}}">
                    <a class="btn nav-link  btn-sm btn-outline-success btn-block" href="{{route('business.register')}}">BECOME A PARTNER</a>
                </li>
            </ul>
        </div>
    </div>
</nav>


{{--<!-- Page Content -->--}}
{{--<div class="container">--}}
    {{--<div class="row logo-bar">--}}
        {{--<div class="col-lg-6">--}}
            {{--<div class="logo">--}}
                {{--<img class="img-responsive logo-img"--}}
                     {{--src="{{asset('packages/frontenduz/asset/images/uzyma.png')}}"/>--}}
            {{--</div>--}}

        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
<div class="container">
    <div class="col-lg-12 col-md-12">
        @yield("content")
    </div>
</div>
<!-- Footer -->
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Uzyma 2017.</p>
    </div>
    <!-- /.container -->
</footer>

<!-- Bootstrap core JavaScript -->
<script src="{{asset('packages/frontenduz/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('packages/frontenduz/vendor/popper/popper.min.js')}}"></script>
<script src="{{asset('packages/frontenduz/vendor/bootstrap/js/bootstrap.min.js')}}"></script>

</body>

</html>