<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Coming Soon</title>
        <script type="text/javascript" src="{{asset('packages/backend/assets/js/core/libraries/jquery.min.js')}}"></script>
        <link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/lato" type="text/css"/>
        <!--<link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'></link>-->
        <!--<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lato-Regular" />-->
        <link href="{{asset('packages/frontend/tools/style.css')}}" rel="stylesheet" type="text/css" media="screen" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css"/>
        <link href='https://fonts.googleapis.com/css?family=Bowlby+One' rel='stylesheet' type='text/css'/>

        <meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no"/>


    </head>
    <body>
        <div class="banner"></div>
        <div class="wrapper">

            <div class="header">
                <h1 id="logo"><img class="img-responsive" style="width: 40%;" src="{{asset('packages/frontend/images/uzyma.png')}}"></img></h1>
            </div> <!-- end header -->

            <div class="content-wrapper">
                <p style="margin-top: 0px; padding-top: 0px;" class="comingsoon">Beauty . Fitness . Wellness about to get <b> EASIER !!!</b></p>


                <div class="content">

                    <div id="successSubscribe" style="color:red"></div>
                    <div class="mainform">
                        <form id="formSubsribe" name="newsletter"  class="animated" data-animation="zoomIn" data-animation-delay="300">
                            {{ csrf_field() }}
                            <div class="field">
                                <input type="hidden" name="inputId">
                                    <input type="text" required name="email" id="test" placeholder="example@youremail.com"/>
                            </div>
                            <div class="submit"><img id="loader" src="{{asset("packages/backend/assets/images/AjaxLoader.gif")}}" style="height:25px;" /><input type="submit" value="Get Notified" /></div>

                        </form>

                    </div>

                </div>

                <div class="social"><a href="https://www.facebook.com/uzyma/" class="facebook">facebook <i class="fa fa-facebook" aria-hidden="true"></i></a><a href="https://twitter.com/Uzyma_Fit" class="twitter">twitter <i class="fa fa-twitter" aria-hidden="true"></i></a><a href="https://www.instagram.com/uzyma_fit/?hl=en" class="linked-in">linked in <i class="fa fa-linkedin" aria-hidden="true"></i></a>

                    <div style="margin-top: 5%; margin-bottom: 10%" class="footer">
                        <p style="color: #03A9F4;"  class="comingsoon2">Phone number: +254723649624 &emsp; Email: info@uzyma.com</p>
                    </div>


                </div>

            </div><!-- end content wrapper -->


        </div></br>



    </body>



    <script type="text/javascript">
var placeholder = $("#test").val();

$("#test").keydown(function () {
    if (this.value == placeholder) {
        this.value = '';
    }
}).blur(function () {
    if (this.value == '') {
        this.value = placeholder;
    }
});
    </script>

    <script>
        $(document).ready(function () {
            var base_url = '{!! url("/") !!}';
            $('#loader').hide();
            $('#loaderc').hide();
            $('#formSubsribe').submit(function (e) {

                e.preventDefault();

                $('#loader').show();
                //Creating an ajax method
                $.ajax({
                    //Getting the url of the uploadphp from action attr of form 
                    //this means currently selected element which is our form 
                    url: base_url + "/subscription",
                    //For file upload we use post request
                    type: "POST",
                    //Creating data from form 
                    data: new FormData(this),
                    //Setting these to false because we are sending a multipart request
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        console.log(data);
                        $('#loader').hide();
                        $('#successSubscribe').html(data);
                    },
                    error: function () {}
                });
            });

            $('#contactForm').submit(function (e) {

                e.preventDefault();
                $("#sendMessage").addClass("disabled");

                $('#loaderc').show();
                //Creating an ajax method
                $.ajax({
                    //Getting the url of the uploadphp from action attr of form 
                    //this means currently selected element which is our form 
                    url: base_url + "/contact",
                    //For file upload we use post request
                    type: "POST",
                    //Creating data from form 
                    data: new FormData(this),
                    //Setting these to false because we are sending a multipart request
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        //If the request is successfull we will get the scripts output in data variable 
                        //Showing the result in our html element

                        $('#loaderc').hide();

                        $("#sendMessage").removeClass("disabled");

                        $('#successContact').html(data);
                    },
                    error: function () {}
                });
            });
        });

    </script>



</html>
