<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="img/favicon.ico">

        <title>{{getcong('site_name')}}</title>

        <!-- Load CSS 
        ================================================== -->
        <link href="{{asset('packages/frontend/css/reset.css')}}" rel="stylesheet">
        <link href="{{asset('packages/frontend/css/bootstrap.css')}}" rel="stylesheet">
        <link href="{{asset('packages/frontend/css/flexslider.css')}}" rel="stylesheet">
        <link href="{{asset('packages/frontend/css/isotope.css')}}" rel="stylesheet">
        <link href="{{asset('packages/frontend/css/animate.css')}}" rel="stylesheet">
        <link href="{{asset('packages/frontend/css/lightbox.css')}}" rel="stylesheet">     
        <link href="{{asset('packages/frontend/fonts/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{asset('packages/frontend/css/flaticon.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('packages/frontend/css/style.css')}}" rel="stylesheet">
        <link href="{{asset('packages/frontend/css/responsive.css')}}" rel="stylesheet">
        <!-- Color Panel -->
        <link href="{{asset('packages/frontend/css/color_panel.css')}}" rel="stylesheet" /> 
        <!-- Skin Colors -->
        <link href="{{asset('packages/frontend/css/color-schemes/green.css')}}" id="changeable-colors" rel="stylesheet" type="text/css" /> 	

        <!-- Google Fonts  
        ================================================== -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Lato:400,300italic,700,900' rel='stylesheet' type='text/css'>

        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug
        ================================================== -->
        <script src="{{asset('packages/frontend/js/ie10-viewport-bug-workaround.js')}}"></script>

        <script src="{{asset('packages/frontend/js/modernizr.js')}}"></script>

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries
        ================================================== -->
        <!--[if lt IE 9]>
                        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
                        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <!-- Page Loader -->
        <div id="pageloader">
            <div class="loader-item fa fa-spin colored-border"></div>
        </div>

        <!-- Header Area Begins 
        ================================================== -->
        <header id="top">
            <div class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container no-container">
                    <!-- Navbar Header -->
                    <div class="navbar-header">
                        <!-- Navbar Toogle -->
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!-- Logo -->
                        <a class="navbar-brand" href="#"><img src="{{asset('packages/frontend/images/logo.png')}}" alt="Opic!" class="logo"></a>
                    </div>
                    <!-- Navbar Menu Section -->
                    <div class="navbar-collapse collapse">
                        <nav>
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#top" class="scroll">Home</a></li>
                                <li><a href="#services" class="scroll">Services</a></li>

                                <li><a href="#contact" class="scroll">Contact</a></li>

                            </ul>
                        </nav>
                    </div><!-- Navbar Menu Section Ends -->
                </div>
            </div>
        </header><!-- Header Ends -->


        <!-- Slider Area
        ================================================== -->
        <div id="header" class="header-section">		
            <!-- Background Slider Begins -->
            <div id="slide" class="home-slider relative">

                <!-- Slides -->
                <div class="slides-container">
                    <!-- Slider Images -->
                    <div class="slide-item1 parallax1bg"></div>	
                    <div class="slide-item2 parallax2bg"></div>
                    <div class="slide-item3 parallax3bg"></div>	
                </div>

                <!-- Slide Text Begins -->
                <div class="text-slider-section absolute container">
                    <div class="text-slider">
                        <ul class="slide-text slides">
                            <li class="capitalize">We <i class="fa fa-1x fa-heart"></i> Responsive 
                                <h5>Wordpress. Joomla. Drupal. Ecommerse.</h5>
                            </li>
                            <li class="capitalize">We <i class="fa fa-1x fa-heart"></i> Quality.
                                <h5>Wordpress. Joomla. Drupal. Ecommerse.</h5>
                            </li>
                            <li class="capitalize">We <i class="fa fa-1x fa-heart"></i> Flat Design
                                <h5>Wordpress. Joomla. Drupal. Ecommerse.</h5>
                            </li>
                        </ul>
                    </div>				
                    <!-- Button -->
                    <span>
                        <a href="#" class="btn btn-cyan">Learn More</a>
                    </span>
                </div><!-- Slide Text Ends -->				
            </div>
        </div><!-- Slider Ends -->


        <!-- Services Area
        ================================================== -->
        <section id="services" style="padding:0px;"> 		  
            <div class="container">
                <div class="row">

                    <div class="col-md-12">					
                        <!-- Title -->
                        <h2 class="text-center animated" data-animation="fadeIn" data-animation-delay="400">Uzyma Services</h2>
                        <!-- Devider -->
                        <div class="devider">
                            <div class="devider-line-h devider-line-h-sea-green animated" data-animation="zoomIn" data-animation-delay="1000"></div>
                            <div class="devider-round-out devider-round-out-sea-green animated" data-animation="zoomIn" data-animation-delay="700">
                                <div class="devider-round-in devider-round-in-sea-green"></div>
                            </div>
                        </div>

                    </div><!-- end col-md-12 -->

                    <!-- CMS Website Services -->
                    <div class="col-md-12 services-inner">

                        <div class="col-md-6 pull-right">

                            <!-- cms icon -->
                            <div class="icon-cms animated" data-animation="zoomIn" data-animation-delay="1700">
                                <div class="icon-line icon-line-cms animated" data-animation="fadeInRight" data-animation-delay="2400"></div>
                                <div class="icon-round icon-round-cms animated" data-animation="zoomIn" data-animation-delay="2900"></div>
                            </div>
                            <!-- cms icon -->

                            <img src="{{asset('packages/frontend/images/cms.png')}}" alt="" class="img-responsive pull-right animated" data-animation="bounceIn" data-animation-delay="700">            
                        </div><!-- end col-xs-6" -->

                        <div class="col-md-6 pull-left animated" data-animation="fadeIn" data-animation-delay="400">
                            <h4 class="fw-200"><span class="fw-600">CMS</span> Website</h4>
                            <h5 class="fw-300">
                                Sed ut perspiciatis unde <span class="span-color"> omnis iste</span> natus error sit voluptatem
                            </h5>
                            <p>
                                Architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.
                            </p>
                            <p><a href="#" class="services-learn-more">Learn more this service <i class="flaticon-arrow209"></i></a></p>
                        </div><!-- end col-xs-6" -->	 

                    </div><!-- end col-md-12 -->

                    <!-- E-Commerse Website Services -->
                    <div class="col-md-12 services-inner">

                        <div class="col-md-6">

                            <!-- ecommerse icon -->
                            <div class="icon-ecommerse animated" data-animation="zoomIn" data-animation-delay="1700">
                                <div class="icon-line icon-line-ecommerse animated" data-animation="fadeInLeft" data-animation-delay="2400"></div>
                                <div class="icon-round icon-round-ecommerse animated" data-animation="zoomIn" data-animation-delay="2900"></div>
                            </div>
                            <!-- ecommerse icon -->
                            <img src="{{asset('packages/frontend/images/ipad.png')}}" alt="" class="img-responsive pull-left animated" data-animation="bounceIn" data-animation-delay="700">						 

                        </div><!-- end col-xs-6" -->

                        <div class="col-md-6 pull-right animated" data-animation="fadeIn" data-animation-delay="400">
                            <h4 class="fw-200"><span class="fw-600">eCommerse</span> Website</h4>
                            <h5 class="fw-300">
                                Sed ut perspiciatis unde <span class="span-color"> omnis iste</span> natus error sit voluptatem
                            </h5>
                            <p>
                                Architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.
                            </p>
                            <p><a href="#" class="services-learn-more">Learn more this service <i class="flaticon-arrow209"></i></a></p>

                        </div><!-- end col-md-6" -->    

                    </div><!-- end col-md-12 -->


                    <!-- HTML Website Services -->
                    <div class="col-md-12 services-inner">

                        <div class="col-md-6 pull-right">

                            <!-- html icon -->
                            <div class="icon-html animated" data-animation="zoomIn" data-animation-delay="1700">
                                <div class="icon-line icon-line-html animated" data-animation="fadeInRight" data-animation-delay="2400"></div>
                                <div class="icon-round icon-round-html animated" data-animation="zoomIn" data-animation-delay="2900"></div>
                            </div>
                            <!-- html icon -->

                            <img src="{{asset('packages/frontend/images/laptop.png')}}" alt="" class="img-responsive pull-right animated" data-animation="bounceIn" data-animation-delay="700">

                        </div><!-- end col-xs-6" -->

                        <div class="col-md-6 pull-left animated" data-animation="fadeIn" data-animation-delay="400">
                            <h4 class="fw-200"><span class="fw-600">HTML</span> Website</h4>
                            <h5 class="fw-300">
                                Sed ut perspiciatis unde <span class="span-color"> omnis iste</span> natus error sit voluptatem
                            </h5>
                            <p>
                                Architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.
                            </p>
                            <p><a href="#" class="services-learn-more">Learn more this service <i class="flaticon-arrow209"></i></a></p>
                        </div><!-- end col-xs-6" -->					   

                    </div><!-- end col-md-12 -->

                </div><!-- end row -->			  
            </div><!-- end container -->		  
        </section><!-- end services -->



        <!-- Newsletter Area
        ================================================== -->
        <section id="newsletter" class="" style="background-color:#45b39c;">
            <div id="successSubscribe"></div>
            <form id="formSubsribe" name="newsletter"  class="animated" data-animation="zoomIn" data-animation-delay="300">
                {{ csrf_field() }}
                <input type="hidden" name="inputId">
                <input class="change" style="" required name="email" type="text" placeholder="Enter your email address here to subscribe for our updates, and press Enter!">
            </form>



            <div class="text-center"><img id="loader" src="{{asset("packages/backend/assets/images/AjaxLoader.gif")}}" style="height:25px;" /></div>

        </section>

        <!-- About Us And Team Area
        ================================================== -->


        <!-- Contact Us Area
        ================================================== -->
        <section id="contact">

            <div class="container">
                <div class="row">

                    <div class="col-md-12">        
                        <h2 class="text-center animated" data-animation="fadeIn" data-animation-delay="400">Get In Touch</h2>

                        <div class="devider">
                            <div class="devider-line-h devider-line-h-sea-green animated" data-animation="zoomIn" data-animation-delay="1000"></div>
                            <div class="devider-round-out devider-round-out-sea-green animated" data-animation="zoomIn" data-animation-delay="700">
                                <div class="devider-round-in devider-round-in-sea-green"></div>
                            </div>
                        </div>

                        <p class="text-center lead op08 animated" data-animation="fadeIn" data-animation-delay="400">
                            Drop us an<span class="span-color"> email.</span> We'd be glad to hear from you.
                        </p>


                        <div class="clearfix"></div>

                    </div><!-- end col-md-12 -->

                    <div class="col-md-12">
                        <div class="col-md-6 animated" data-animation="fadeIn" data-animation-delay="300">
                            <div id="successContact"></div>
                            <form name="contactform" id="contactForm" >
                                {{ csrf_field() }}
                                <input type="text" name="name" id="senderName" placeholder="Your Name" class="pull-right input-text" required>
                                <input type="email" name="email" id="senderEmail" placeholder="Your E-mail" class="pull-right input-text" required>
<!--                                <input type="text" name="senderWebsite" id="senderWebsite" placeholder="Your Website" class="pull-right input-text" required>-->
                                <textarea name="message" id="message" class="text-area pull-right" required></textarea>
                                <input type="submit" name="sendMessage" id="sendMessage" value="Send Message" class="btn-msg pull-right white">	
                            </form>
                            <div class="text-center"><img id="loaderc" src="{{asset("packages/backend/assets/images/AjaxLoader.gif")}}" style="height:25px;" /></div>

<!--                            <div id="sendingMessage" class="statusMessage sending-message"><p>Sending your message. Please wait...</p></div>
                            <div id="successMessage" class="statusMessage success-message"><p>Thanks for sending your message! We'll get back to you shortly.</p></div>
                            <div id="failureMessage" class="statusMessage failure-message"><p>There was a problem sending your message. Please try again.</p></div>
                            <div id="incompleteMessage" class="statusMessage"><p>Please complete all the fields in the form before sending.</p></div>-->

                            <div class="clearfix"></div>
                        </div><!-- end col-md-6-->

                        <div id="contacInfo" class="col-md-6">

                            <div class="contact-info animated" data-animation="fadeInUp" data-animation-delay="300">
                                <i class="fa fa-4x fa-mobile pull-left text-center"></i>
                                <h6 class="fw-200">Toll Free Number</h6>
                                <h5>{{getcong('site_phone')}}</h5>
                            </div>

                            <div class="contact-info animated" data-animation="fadeInUp" data-animation-delay="500">
                                <i class="fa fa-3x fa-envelope pull-left text-center"></i>
                                <h6 class="fw-200">General Enquiry</h6>
                                <h5>{{getcong('site_email')}}</h5>
                            </div>

                            <div class="contact-info animated" data-animation="fadeInUp" data-animation-delay="700">
                                <i class="fa fa-3x fa-map-marker pull-left text-center"></i>
                                <h6 class="fw-200">Address Location</h6>
                                <h5>{{getcong('address')}}</h5>
                            </div>



                        </div><!-- end col-md-6-->

                    </div><!-- end col-md-12 -->

                </div><!-- end row -->
            </div><!-- end container -->

        </section>

        <!-- Socials Area
        ================================================== -->
        <section id="socials" class="bg-dark-blue">

            <div class="container">
                <div class="row text-center">
                    <div class="col-md-12 col-sm-12 text-center">

                        <ul class="text-center footer-social">
                            <li class="animated" data-animation="zoomIn" data-animation-delay="300">
                                <a href="#" class="white"><i class="fa fa-twitter fa-3x"></i></a>
                            </li>
                            <li class="animated" data-animation="zoomIn" data-animation-delay="300">
                                <a href="#" class="white"><i class="fa fa-facebook fa-3x"></i></a>
                            </li>


                            <li class="animated" data-animation="zoomIn" data-animation-delay="300">
                                <a href="#" class="white"><i class="fa fa-google-plus fa-3x"></i></a>
                            </li>

                            <li class="animated" data-animation="zoomIn" data-animation-delay="300">
                                <a href="#" class="white"><i class="fa fa-youtube fa-3x"></i></a>
                            </li>
                        </ul>

                    </div><!-- end col-md-12 -->
                </div><!-- end row -->
            </div><!-- end container -->

        </section>

        <!-- Client Area
        ================================================== -->


        <!-- Copyright Area
        ================================================== -->
        <section id="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">        
                        <p class="text-center copyright">
                            Copyright {{date('Y')}}<a href="#"> {{getcong('site_name')}}</a> All rights reserved. Designed by <a href="#">205 Investments Limited</a>
                        </p>        
                    </div>
                </div>
            </div>	
        </section>  


        <!-- Javascrit Core
        ================================================== -->        
        <script src="{{asset('packages/frontend/js/jquery-1.11.1.min.js')}}"></script>
        <script src="{{asset('packages/frontend/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('packages/frontend/js/jquery.isotope.min.js')}}"></script>
        <script src="{{asset('packages/frontend/js/jquery.flexisel.js')}}"></script>
        <script src="{{asset('packages/frontend/js/jquery.appear.js')}}"></script>
        <script src="{{asset('packages/frontend/js/jquery.easing.1.3.js')}}"></script>		
        <script src="{{asset('packages/frontend/js/jquery.parallax-1.1.3.js')}}"></script>
        <script src="{{asset('packages/frontend/js/parallax-bg.js')}}"></script>
        <script src="{{asset('packages/frontend/js/jquery.lightbox.min.js')}}"></script>
        <script src="{{asset('packages/frontend/js/jquery.superslides.js')}}"></script>
        <script src="{{asset('packages/frontend/js/jquery.flexslider-min.js')}}"></script>
        <script src="{{asset('packages/frontend/js/jquery.custom.js')}}"></script>   
	
        <script>
$(document).ready(function () {
    var base_url = '{!! url("/") !!}';
    $('#loader').hide();
    $('#loaderc').hide();
    $('#formSubsribe').submit(function (e) {

        e.preventDefault();

        $('#loader').show();
        //Creating an ajax method
        $.ajax({
            //Getting the url of the uploadphp from action attr of form 
            //this means currently selected element which is our form 
            url: base_url + "/subscription",
            //For file upload we use post request
            type: "POST",
            //Creating data from form 
            data: new FormData(this),
            //Setting these to false because we are sending a multipart request
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                console.log(data);
                $('#loader').hide();
                $('#successSubscribe').html(data);
            },
            error: function () {}
        });
    });

    $('#contactForm').submit(function (e) {

        e.preventDefault();
        $("#sendMessage").addClass("disabled");

        $('#loaderc').show();
        //Creating an ajax method
        $.ajax({
            //Getting the url of the uploadphp from action attr of form 
            //this means currently selected element which is our form 
            url: base_url + "/contact",
            //For file upload we use post request
            type: "POST",
            //Creating data from form 
            data: new FormData(this),
            //Setting these to false because we are sending a multipart request
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                //If the request is successfull we will get the scripts output in data variable 
                //Showing the result in our html element

                $('#loaderc').hide();

                $("#sendMessage").removeClass("disabled");

                $('#successContact').html(data);
            },
            error: function () {}
        });
    });
});

        </script>
    </body>
</html>
