@extends("layouts.backend.template")

@section("content")
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="text-semibold">Activity new</span></h4>
            </div>

        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{route('home.partners')}}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li><a href="{{route('activity')}}"> Activities</a></li>
                <li class="active">Add new activity</li>
            </ul>


        </div>


    </div>


    <!-- /page header -->
    <div class="content">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (Session::get('error'))
            <div class="alert alert-error alert-danger">
                @if (is_array(Session::get('error')))
                    {{ head(Session::get('error')) }}
                @endif
            </div>
        @endif
        @if(Session::has('flash_message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message') }}
            </div>
        @elseif(Session::has('flash_message_error'))
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message_error') }}
            </div>
        @endif

        <script src="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=AIzaSyB8WgY93rMVkcDEn3Z64CUcSh3Jx_UMeH4"></script>
        <script>

            google.maps.event.addDomListener(window, 'load', initialize);

            function initialize() {
                var input = document.getElementById('pac-input');
                var autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.addListener('place_changed', function () {
                    var place = autocomplete.getPlace();
// place variable will have all the information you are looking for.
                    $('#latitude').val(place.geometry['location'].lat());
                    $('#longitude').val(place.geometry['location'].lng());
                    console.log(place.geometry['location'].lat());
                    console.log(place.geometry['location'].lng());
                });
            }




        </script>

        @if(count($activity))
        <script>
            $(document).ready(function () {
                // Select
                var $mySelectTags = $("#tags").select2({
                    closeOnSelect: true, placeholder: ""
                });

                var selectservice = "<?php echo $activity->service_id ?>";


                var arrayJson = "<?php echo $activity->tags ?>";

                $mySelectTags.val(arrayJson).trigger("change");

                var $mySelectAmenities = $("#amenities").select2({
                    closeOnSelect: true, placeholder: ""
                });

                console.log('Kenn');

                var arrayJsonAmenities = <?php echo $activity->activity_amen ?>;


                $mySelectAmenities.val(arrayJsonAmenities).trigger("change");

                $('.fitness').hide();
                $('#selectsb').change(function () {
                    if ($('#selectsb').val() == '2') {
                        $('.fitness').show();
                    } else {
                        $('.fitness').hide();
                    }
                });

                var selectservice = "<?php echo $activity->service_id ?>";

                var fitness_or_beauty = "<?php echo $activity->fitness_or_beauty ?>";
                var difficult_level = "<?php echo $activity->difficult_level ?>";
                var service_type = "<?php echo $activity->service_type ?>";

                //alert(selectservice +" "+fitness_or_beauty+" "+difficult_level+" " +service_type);

                $('#select_service').select2().val(selectservice).trigger("change");

                $('#levels').select2().val(difficult_level).trigger("change");
                $('#service_type').select2().val(service_type).trigger("change");


            });
        </script>
        @endif

    <!-- Basic datatable -->
        <div class="panel panel-flat">
            <div class="panel-heading">

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <form role="form" enctype="multipart/form-data" method="POST" action="{{route('post.activity')}}"/>
                {{ csrf_field() }}
                <input type="hidden" id="id" name="inputId" value="{{ isset($activity->id) ? $activity->id : null }}">

                <div class="row">
                    <div class="form-group col-lg-6">
                        <label>Activity name:</label>
                        <input value="{{ isset($activity->activity_name) ? $activity->activity_name : null }}"
                               name="activity_name" required id="activity_name" class="form-control"
                               placeholder="Activity name" value="{{old('activity_name')}}"></input>
                        @if ($errors->has('activity_name'))
                            <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('activity_name') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group col-lg-6">
                        <label>Fitness or beauty:</label>
                        <select id="selectsb" name="fitness_or_beauty" class="select-search">
                            <optgroup label="Select">

                                <option value="1">Beauty</option>
                                <option value="2">Fitness</option>

                            </optgroup>
                        </select>
                        @if ($errors->has('fitness_or_beauty'))
                            <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('fitness_or_beauty') }}</strong>
                    </span>
                        @endif
                    </div>

                </div>
                <div class="row">
                    <div class="form-group col-lg-6">
                        <label>Services:</label>
                        <select id="select_service" name="select_service" class="select-search">
                            <optgroup label="Select a service">
                                @foreach($services as $row)
                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                @endforeach
                            </optgroup>
                        </select>
                        @if ($errors->has('select_service'))
                            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('select_service') }}</strong>
                </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-lg-6">
                        <label>Activity Description:</label>
                        <textarea name="description" rows="5" required cols="5" id="description" class="form-control"
                                  placeholder="Activity Description"
                                  value="{{old('description')}}">{{ isset($activity->activity_description) ? $activity->activity_description : null }}</textarea>
                        @if ($errors->has('description'))
                            <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('description') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class=" form-group col-sm-6">
                        <label class="control-label">Amenities </label>
                        <select id="amenities" name="amenities2[]" class="select" multiple="multiple">
                            <optgroup label="Select amenities">
                                @foreach($amenities as $row)
                                    <option value="{{$row->name}}">{{$row->name}}</option>
                                @endforeach
                            </optgroup>
                        </select>
                    </div>


                </div>
                <div class="fitness box">
                    <hr>
                    <div class="row">
                        <div class="form-group col-lg-6">
                            <label>Date:</label>
                            <input value="{{ isset($activity->date) ? $activity->date : null }}" type="text"
                                   class="form-control daterange-single" name="date" id="date"
                                   placeholder="Choose date&hellip;">
                            @if ($errors->has('date'))
                                <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('date') }}</strong>
                    </span>
                            @endif
                        </div>
                        <div class="form-group col-lg-6">
                            <label>Time:</label>
                            <input value="{{ isset($activity->time) ? $activity->time : null }}" name="time" readonly
                                   id="time" class="form-control pickatime" placeholder="Choose time&hellip;"
                                   value="{{old('time')}}"></input>
                            @if ($errors->has('time'))
                                <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('time') }}</strong>
                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-6">
                            <label>Difficult level:</label>
                            <select id="levels" name="levels" class="select-search">
                                <optgroup label="Select a level">
                                    @foreach($levels as $row)
                                        <option value="{{$row->id}}">{{$row->level_name}}</option>
                                    @endforeach
                                </optgroup>
                            </select>
                            @if ($errors->has('levels'))
                                <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('levels') }}</strong>
                    </span>
                            @endif
                        </div>
                    </div>
                    <hr>
                </div>
                <div class="row">
                    <div class="form-group col-lg-6">
                        <label>Duration:</label>
                        <input value="{{ isset($activity->duration) ? $activity->duration : null }}" name="duration"
                               id="" class="form-control" placeholder="duration" value="{{old('duration')}}"></input>
                        @if ($errors->has('duration'))
                            <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('duration') }}</strong>
                    </span>
                        @endif
                    </div>

                    <div class="form-group col-lg-6">
                        <label>Date:</label>
                        <input value="{{ isset($activity->date) ? $activity->date : null }}" type="text"
                               class="form-control daterange-single" name="date" id="date"
                               placeholder="Choose date&hellip;">
                        @if ($errors->has('date'))
                            <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('date') }}</strong>
                    </span>
                        @endif
                    </div>

                </div>

                <hr>


                <div class="row" id="hhome">
                    <div class="form-group col-lg-6">
                        <label>Service type:</label>
                        <select id="service_type" name="service_type" class="select">
                            <optgroup label="Select service type">

                                <option value="home visit">Home visit</option>
                                <option value="salon visit">Salon visit</option>

                            </optgroup>
                        </select>
                        @if ($errors->has('service_type'))
                            <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('service_type') }}</strong>
                    </span>
                        @endif
                    </div>
                </div>
                <hr>


                <div class="row">
                    <div class="form-group col-lg-6">
                        <label>Address:</label>
                        <input value="{{ isset($activity->address) ? $activity->address : null }}" name="address"
                               id="pac-input" class="form-control" placeholder="address"
                               value="{{old('address')}}"></input>
                        @if ($errors->has('address'))
                            <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('address') }}</strong>
                    </span>
                        @endif
                    </div>

                </div>
                <div class="row">
                    <div class="form-group col-lg-6">
                        <label>Latitude:</label>
                        <input value="{{ isset($activity->latitude) ? $activity->latitude : null }}" name="latitude"
                               id="latitude" class="form-control" readonly placeholder="latitude"
                               value="{{old('latitude')}}"></input>
                        @if ($errors->has('latitude'))
                            <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('latitude') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group col-lg-6">
                        <label>Longitude:</label>
                        <input value="{{ isset($activity->longitude) ? $activity->longitude : null }}" name="longitude"
                               id="longitude" class="form-control" readonly placeholder="longitude"
                               value="{{old('longitude')}}"></input>
                        @if ($errors->has('longitude'))
                            <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('longitude') }}</strong>
                    </span>
                        @endif
                    </div>
                </div>
                <hr>


                <div class="row">
                    {{--<div class="form-group col-lg-6">--}}
                    {{--<label>Stylist:</label>--}}
                    {{--<input value="{{ isset($activity->stylist) ? $activity->stylist : null }}" name="stylist"--}}
                    {{--id="stylist" class="form-control" readonly placeholder="stylist"--}}
                    {{--value="{{old('stylist')}}"></input>--}}
                    {{--@if ($errors->has('stylist'))--}}
                    {{--<span class="help-block">--}}
                    {{--<strong class="text-danger">{{ $errors->first('stylist') }}</strong>--}}
                    {{--</span>--}}
                    {{--@endif--}}
                    {{--</div>--}}

                    <div class=" form-group col-sm-6">
                        <label class="control-label">Stylist: </label>
                        <select id="amenities" name="stylist" class="select">
                            <optgroup label="Select stylist">
                                @foreach($stylist as $row)
                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                @endforeach
                            </optgroup>
                        </select>
                    </div>

                    <div class="form-group col-lg-6">
                        <label>Image:</label>
                        <input type="file" name="file" class="file-input" data-show-caption="true"
                               data-show-upload="false">
                    </div>

                </div>
                <div class="row">

                    <div class="form-group col-lg-6">
                        <label>Cancelation deadline:</label>
                        <input value="{{ isset($activity->cancelationdate) ? $activity->cancelationdate : null }}"
                               type="text" class="form-control" name="cancelationdate"
                               id="cancelationdate" placeholder="Cancelation deadline" value="12 hours">
                        @if ($errors->has('cancelationdate'))
                            <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('cancelationdate') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group col-lg-6">
                        <label>Price:</label>
                        <input value="{{ isset($activity->price) ? $activity->price : null }}" type="text"
                               class="form-control" name="price" id="price" required placeholder="Enter Price;">
                        @if ($errors->has('price'))
                            <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('price') }}</strong>
                    </span>
                        @endif
                    </div>
                </div>
                <!-- <div class="row">


                </div> -->
                <div class="row">
                    <div class="form-group col-lg-6">
                        <button class="btn btn-lg btn-primary" type="submit">save</button>
                    </div>
                </div>

                </form>
            </div>


        </div>


        <div class="row ">

        </div>

        <br>

        @include("layouts.backend.footer")
    </div>


@endsection
