@extends("layouts.backend.template")

@section("content")
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="text-semibold">Home</span> - Amenities</h4>
            </div>


        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{route('home.partners')}}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Amenities</li>
            </ul>


        </div>


    </div>


    <!-- /page header -->
    <div class="content">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (Session::get('error'))
            <div class="alert alert-error alert-danger">
                @if (is_array(Session::get('error')))
                    {{ head(Session::get('error')) }}
                @endif
            </div>
        @endif
        @if(Session::has('flash_message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message') }}
            </div>
        @elseif(Session::has('flash_message_error'))
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message_error') }}
            </div>
    @endif



    <!-- Basic datatable -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Amenities</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <a href="javascript: ajaxmodal()" class="btn btn-primary btn-sm pull-right">Add Amenities<i class="icon-play3 position-right"></i></a>
            </div>


            <table id="business" class="table table-hover table-condensed table-bordered table-striped">
                <thead>
                <tr>
                    <th>Name</th>

                </tr>
                </thead>

            </table>
        </div>
        <!-- /basic datatable -->


        <script type="text/javascript">
            function ajaxmodal() {
                $('#id').val("");
                $('#name').val("");
                $('#modal_add').modal('show');
            }

            function ajaxmodaledit(id) {
                var url = base_url + '/partners/activity-details';
                $.get(url + '/' + id, function (data) {
                    $('#id').val(data.id);
                    $('#activity_name').val(data.activity_name);
                    $('#description').val(data.activity_description);
                    $('#date').val(data.date);
                    $('#select_service').select2().val(data.service_id).trigger("change");
                    $('#time').val(data.time);
                    $('#address').val(data.address);
                    $('#latitude').val(data.latitude);
                    $('#longitude').val(data.longitude);
                });
                $('#modal_add').modal('show');
            }

            function ajaxmodaldetails(id) {
                var url = base_url + '/partners/activity-details';
                $.get(url + '/' + id, function (data) {

                    document.getElementById("activity_name2").innerHTML = data.activity_name;
                    document.getElementById("activity_description2").innerHTML = data.activity_description;
                    document.getElementById("service2").innerHTML = data.service;
                    document.getElementById("date").innerHTML = data.date;
                    document.getElementById("time").innerHTML = data.time;
                    document.getElementById("address").innerHTML = data.address;
                    document.getElementById("cancelationdate").innerHTML = data.cancel;
                    document.getElementById("level_name").innerHTML = data.level_name;
                });
                $('#modal_details').modal('show');
            }


            $(document).ready(function () {
                oTable = $('#business').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "ajax": base_url + "/partners/amenitiesTable",
                    "columns": [

                        {data: 'name', name: 'name'},

                    ]
                });
            });

        </script>


        <div class="row ">

        </div>

        <br>

    @include("layouts.backend.footer")


    <!-- Basic modal -->
        <div id="modal_add" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title">Amenities</h5>
                    </div>
                    <div class="modal-body">
                        <form role="form" enctype="multipart/form-data" method="POST"
                              action="{{route('amenities.add')}}"/>
                        {{ csrf_field() }}
                        <input type="hidden" id="id" name="inputId"
                               value="{{ isset($activity->id) ? $activity->id : null }}">

                        <div class="row">
                            <div class="form-group col-lg-8">
                                <label>Name:</label>
                                <input value="{{ isset($activity->name) ? $activity->name : null }}" name="name"
                                       required id="name" class="form-control" placeholder="Name"
                                       value="{{old('name')}}"></input>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('name') }}</strong>
                    </span>
                                @endif
                            </div>


                        </div>


                        <div class="row">
                            <div class="form-group col-lg-6">
                                <button class="btn btn-lg btn-primary" type="submit">save</button>
                            </div>
                        </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>
        <!-- /basic modal -->


    </div>


@endsection
