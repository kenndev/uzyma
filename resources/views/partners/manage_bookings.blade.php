@extends("layouts.backend.template")

@section("content")
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Bookings</span></h4>
        </div>

    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="{{url('/admin')}}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Bookings</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
        @if (is_array(Session::get('error')))
        {{ head(Session::get('error')) }}
        @endif
    </div>
    @endif
    @if(Session::has('flash_message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message') }}
    </div>
    @elseif(Session::has('flash_message_error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message_error') }}
    </div>
    @endif


    <!-- Basic datatable -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Bookings</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">


            <form class="form-validate" accept-charset="UTF-8" >
                {{ csrf_field() }}

                <div class="row">
                    <div class="form-group col-lg-4">
                        <label>From: </label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                            <input type="text" name="start_date" id="start_date" class="form-control daterange-single" >
                        </div>
                    </div>
                    <div class="form-group col-lg-4">
                        <label>To: </label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                            <input type="text" name="end_date" id="end_date" class="form-control daterange-single" >
                        </div>
                    </div>
                </div>
                <div class="row">



                </div>
                <button type="button" id="btn-save" value="add" class="btn btn-primary">Search</button>

            </form>


<!--<a href="javascript: ajaxmodal()" class="btn btn-primary btn-sm pull-right">Add a Difficult Level<i class="icon-play3 position-right"></i></a>-->
        </div>


        <table id="business" class="table table-hover table-condensed table-bordered table-striped">
            <thead>
                <tr>


                    <th>Activity</th>
                    <th>Client name</th>
                    <th>Client email</th>
                    <th>Client phone</th>
                    <th>Total</th>
                    <th>Check out date</th>
                    <th>Action</th>
                </tr>
            </thead>

        </table>
    </div>
    <!-- /basic datatable -->

    <script type="text/javascript">
        function ajaxmodal() {
            $('#id').val("");
            $('#business_type').val("");
            $('#modal_edit').modal('show');
        }

        function ajaxmodaledit(id) {
            var url = base_url + '/admin/difficultyLevel-details';
            $.get(url + '/' + id, function (data) {
                $('#id').val(data.id);
                $('#difficult_level').val(data.level_name);
            });
            $('#modal_edit').modal('show');
        }

        $(document).ready(function () {
            oTable = $('#business').DataTable({
                "processing": true,
                "serverSide": true,
                "destroy": true,
                "ajax": '{!! route('bookings.table.partner') !!}',
                "columns": [
                    {data: 'activity_name', name: 'activity_name'},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'contact', name: 'contact'},
                    {data: 'total', name: 'total'},
                    {data: 'check_out_date', name: 'check_out_date'},
                    {data: 'action', name: 'action', 'searchable': false}
                ]
            });

            searchRevenue();

        });

        function searchRevenue() {
            $("#btn-save").click(function (e) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                e.preventDefault();
                var formData = {
                    start_date: $('#start_date').val(),
                    end_date: $('#end_date').val(),
                };

                console.log(formData);
                searchTable(formData);

            });
        }

        function searchTable(formData) {
            $('#business').DataTable({
                "processing": true,
                "serverSide": true,
                "destroy": true,
                "ajax": {
                            "url": '{!! route('bookings.partner.search') !!}',
                            "data": formData,
                            "type": "POST"
                        },
               
                "columns": [
                    {data: 'activity_name', name: 'activity_name'},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'contact', name: 'contact'},
                    {data: 'total', name: 'total'},
                    {data: 'check_out_date', name: 'check_out_date'},
                    {data: 'action', name: 'action', 'searchable': false}
                ]
            });
        }

    </script>

    <div class="row ">

    </div>

    <br>




    <!-- Footer -->
    @include("layouts.backend.footer")

    <!-- /footer -->


    <!-- Basic modal -->
    <div id="modal_edit" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Business types</h5>
                </div>
                <form action="{{route('difficultyLevel.add')}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="inputId" id="id"></input>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Name:</label>
                            <input name="difficult_level" id="difficult_level" class="form-control" value="{{old('difficult_level')}}" placeholder="eg. bigginer" required></input>
                            @if ($errors->has('difficult_level'))
                            <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('difficult_level') }}</strong>
                            </span>
                            @endif
                        </div>


                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>

                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /basic modal -->



</div>
<!-- /content area -->
@endsection