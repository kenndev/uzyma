@extends('layouts.front.template')

@section('content')
    @if (session('flash_message'))
        <div class="alert alert-success">
            {{ session('flash_message') }}
        </div>
    @endif
    <div class="row">
    <div class="col-lg-12 cont">
    <div class="card">
        <div class="card-header text-center join">
            REGISTER A BUSINESS
        </div>
        <div class="card-block">
            <form class="form-horizontal" id="loginform" role="form" method="POST" action="{{ route('business.add') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="input-group ">
                    <span class="input-group-addon"><i class="icon-diff-ignored text-muted"></i></span>
                    <input name="business_name" placeholder="Business name" id="business_name" class="form-control"
                           value="{{old('business_name')}}" placeholder="Business name" required></input>
                    @if ($errors->has('business_name'))
                        <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('business_name') }}</strong>
                                </span>
                    @endif

                </div>
                <div class=" input-group {{ $errors->has('name') ? ' has-error' : '' }}">
                    <span class="input-group-addon"><i class="icon-user text-muted"></i></span>
                    <input id="name" placeholder="Name" type="text" class="form-control" name="name"
                           value="{{ old('name') }}" required autofocus>

                    @if ($errors->has('name'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                    @endif

                </div>
                </div>

                

                 <div class="row">
                <div class="input-group {{ $errors->has('location') ? ' has-error' : '' }}">
                    <span class="input-group-addon"><i class="icon-user text-muted"></i></span>
                    <input id="name" placeholder="Location" type="text" class="form-control" name="location"
                           value="{{ old('location') }}" required autofocus>

                    @if ($errors->has('location'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('location') }}</strong>
                                    </span>
                    @endif

                </div>
                </div>
 <div class="row">
                <div class="input-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <span class="input-group-addon"><i class="icon-mail5 text-muted"></i></span>
                    <input id="email" placeholder="Email" type="email" class="form-control" name="email"
                           value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif

                </div>
                </div>
 <div class="row">
                <div class="col-lg-6 col input-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <span class="input-group-addon"><i class="icon-lock2 text-muted"></i></span>
                    <input id="password" placeholder="Password" type="password" class="form-control" name="password"
                           required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif

                </div>

                <div class="col-lg-6 input-group">

                    <span class="input-group-addon"><i class="icon-lock2 text-muted"></i></span>
                    <input id="password-confirm" placeholder="Confirm password" type="password" class="form-control"
                           name="password_confirmation" required>

                </div>
                </div>
            
                <div class="input-group row">
                 
                   
                        <select name="country" id="country" class="form-control col-lg-4">
                            <option value="+254">(+254) Kenya</option>
                        </select>
                    
                   
                    <input style="margin-left: 10px;" name="phone_number" placeholder="Phone number" id="phone_number" class="col-lg-8 form-control pull-right"
                           value="{{old('phone_number')}}" placeholder="Phone number e.g 704100200" required></input>
                    @if ($errors->has('phone_number'))
                        <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('phone_number') }}</strong>
                                </span>
                    @endif
                  
                   

                </div>


                <div class="input-group">

                    <button type="submit" class="btn btn-outline-warning btn-block">
                        Register<i class="icon-arrow-right14 position-right"></i>
                    </button>

                </div>
                <div class="content-divider text-muted form-group text-center"><span>Already have an account?</span></div>
                <a href="{!! url('/login') !!}" class="btn btn-default btn-block content-group">Back to login</a>

            </form>
        </div>
    </div>
    </div>
    </div>
@endsection
