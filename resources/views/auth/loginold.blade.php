@extends('layouts.applayout')

@section('content')
    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <div class="panel panel-body login-form">
            <div class="text-center">
                <div class="icon-object border-warning-400 text-warning-400"><i class="icon-people"></i></div>
                <h5 class="content-group-lg">Login to your account <small class="display-block">Enter your credentials</small></h5>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="text" name="email" class="form-control" placeholder="Email">
                <div class="form-control-feedback">
                    <i class="icon-user text-muted"></i>
                </div>
                @if ($errors->has('email'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" name="password" class="form-control" placeholder="Password">
                <div class="form-control-feedback">
                    <i class="icon-lock2 text-muted"></i>
                </div>
                @if ($errors->has('password'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                </span>
                @endif
            </div>

            <div class="form-group login-options">
                <div class="row">
                    <div class="col-sm-6">
                        <label class="checkbox-inline">
                            <input type="checkbox" name="remember"
                                   {{ old('remember') ? 'checked' : '' }} class="styled">
                            Remember
                        </label>
                    </div>

                    <div class="col-sm-6 text-right">
                        <a href="{{ route('password.request') }}">Forgot password?</a>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn bg-blue btn-block">Login <i class="icon-arrow-right14 position-right"></i></button>
            </div>



            <div class="content-divider text-muted form-group"><span>Don't have an account?</span></div>
            <a href="{!! route('business.register') !!}" class="btn btn-default btn-block content-group">Sign up</a>

        </div>
    </form>
    <!-- /advanced login -->

@endsection
