@extends('layouts.applayout')

@section('content')
    <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
        {{ csrf_field() }}
        <div class="panel panel-body login-form">
            <div class="text-center">
                <div class="icon-object border-warning-400 text-warning-400"><i class="icon-people"></i></div>
                <h5 class="content-group-lg">Register an account <small class="display-block">Enter your details</small></h5>
            </div>
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                {{--<label for="name" class="col-md-4 control-label">Name</label>--}}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <input id="name" type="text" placeholder="Name" class="form-control" name="name" value="{{ old('name') }}" required
                           autofocus>
                    <div class="form-control-feedback">
                        <i class="icon-user text-muted"></i>
                    </div>

                    @if ($errors->has('name'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                {{--<label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}


                    <input id="email" placeholder="Email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                           required>
                    <div class="form-control-feedback">
                        <i class="icon-user text-muted"></i>
                    </div>

                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif

            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                {{--<label for="password" class="col-md-4 control-label">Password</label>--}}


                    <input id="password" placeholder="password" type="password" class="form-control" name="password" required>
                <div class="form-control-feedback">
                    <i class="icon-lock2 text-muted"></i>
                </div>
                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif

            </div>

            <div class="form-group">
                {{--<label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>--}}


                    <input id="password-confirm" placeholder="Confirm password" type="password" class="form-control" name="password_confirmation"
                           required>
                <div class="form-control-feedback">
                    <i class="icon-lock2 text-muted"></i>
                </div>

            </div>

            <div class="form-group">

                    <button type="submit" class="btn bg-blue btn-block">
                        Register<i class="icon-arrow-right14 position-right"></i>
                    </button>

            </div>
            <div class="content-divider text-muted form-group"><span>Already have an account?</span></div>
            <a href="{!! url('/login') !!}" class="btn btn-default btn-block content-group">Back to login</a>
        </div>
    </form>
@endsection
