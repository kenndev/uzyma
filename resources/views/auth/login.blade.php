@extends('layouts.front.template')

@section('content')
    <div class="card">
        <div class="card-header text-center join">
            LOGIN
        </div>
        <div class="card-block">
            <form class="form-horizontal" id="loginform" role="form" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}


                <div class="input-group {{ $errors->has('email') ? ' has-error' : '' }}">
                    <span class="input-group-addon"><i class="icon-user text-muted"></i></span>
                    <input type="text" name="email" class="form-control" placeholder="Email">



                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                </span>
                    @endif
                </div>

                <div  class="input-group {{ $errors->has('password') ? ' has-error' : '' }}">
                    <span class="input-group-addon"><i class="icon-lock2 text-muted"></i></span>
                    <input type="password" name="password" class="form-control" placeholder="Password">

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                </span>
                    @endif
                </div>

                <div class="form-group login-options">
                    <div class="row">
                        <div class="col-sm-6">
                            <label class="checkbox-inline">
                                <input type="checkbox" name="remember"
                                       {{ old('remember') ? 'checked' : '' }} class="styled">
                                Remember
                            </label>
                        </div>


                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-outline-warning btn-block">Login <i
                                class="icon-arrow-right14 position-right"></i></button>
                    <div style="margin-top: 15px;" class="text-center">
                        <a href="{{ route('password.request') }}">Forgot password?</a>
                    </div>
                </div>


                {{--<div class="content-divider text-muted form-group"><span>Don't have an account?</span></div>--}}
                {{--<a href="{!! route('business.register') !!}" class="btn btn-default btn-block content-group">Sign--}}
                    {{--up</a>--}}


            </form>
            <!-- /advanced login -->
        </div>
    </div>

@endsection
