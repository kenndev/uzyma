@extends('layouts.applayout')

@section('content')
    @if (session('flash_message'))
        <div class="alert alert-success">
            {{ session('flash_message') }}
        </div>
    @endif
    <form class="form-horizontal" role="form" method="POST" action="{{ route('business.add') }}">
        {{ csrf_field() }}
        <div class="panel panel-body login-form">
            <div class="text-center">
                <div class="icon-object border-warning-400 text-warning-400"><i class="icon-people"></i></div>
                <h5 class="content-group-lg">Register an account <small class="display-block">Enter your details</small></h5>
            </div>
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

                <input id="name" placeholder="Name" type="text" class="form-control" name="name"
                       value="{{ old('name') }}" required autofocus>

                @if ($errors->has('name'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                @endif

            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                <input id="email" placeholder="Email" type="email" class="form-control" name="email"
                       value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif

            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                <input id="password" placeholder="Password" type="password" class="form-control" name="password"
                       required>

                @if ($errors->has('password'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                @endif

            </div>

            <div class="form-group">


                <input id="password-confirm" placeholder="Confirm password" type="password" class="form-control"
                       name="password_confirmation" required>

            </div>
            <div class="form-group">

                <input name="phone_number" placeholder="Phone number" id="phone_number" class="form-control"
                       value="{{old('phone_number')}}" placeholder="Phone number" required></input>
                @if ($errors->has('phone_number'))
                    <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('phone_number') }}</strong>
                                </span>
                @endif

            </div>
            <div class="form-group">

                <input name="business_name" placeholder="Business name" id="business_name" class="form-control"
                       value="{{old('business_name')}}" placeholder="Business name" required></input>
                @if ($errors->has('business_name'))
                    <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('business_name') }}</strong>
                                </span>
                @endif

            </div>

            <div class="form-group">

                    <button type="submit" class="btn bg-blue btn-block">
                        Register<i class="icon-arrow-right14 position-right"></i>
                    </button>

            </div>
            <div class="content-divider text-muted form-group"><span>Already have an account?</span></div>
            <a href="{!! url('/login') !!}" class="btn btn-default btn-block content-group">Back to login</a>
        </div>
    </form>
@endsection
