@extends("layouts.backend.template")

@section("content")
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="text-semibold">Stylist</span></h4>
            </div>

        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{url('/admin')}}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Stylist</li>
            </ul>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (Session::get('error'))
            <div class="alert alert-error alert-danger">
                @if (is_array(Session::get('error')))
                    {{ head(Session::get('error')) }}
                @endif
            </div>
        @endif
        @if(Session::has('flash_message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message') }}
            </div>
        @elseif(Session::has('flash_message_error'))
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message_error') }}
            </div>
    @endif


    <!-- Basic datatable -->
        <div class="panel panel-flat">
            <div class="panel-heading">

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>

                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <a href="javascript: ajaxmodal()" class="btn btn-primary btn-sm pull-right">Add a Stylist<i
                            class="icon-play3 position-right"></i></a>
            </div>


            <table id="business" class="table table-hover table-condensed table-bordered table-striped">
                <thead>
                <tr>

                    <th>Image</th>
                    <th>Name</th>
                    <th>Phone number</th>
                    <th>National id</th>
                    <th>Area of expertise</th>
                    <th>Action</th>
                </tr>
                </thead>

            </table>
        </div>
        <!-- /basic datatable -->

        <script type="text/javascript">
            function ajaxmodal() {
                $('#id').val("");
                $('#name').val("");
                $('#phone_number').val("");
                $('#nationa_id').val("");
                $('#expertise_area').val("");
                $('#nhif').val("");
                $('#nssf').val("");
                $('#kra_pin').val("");
                $('#modal_edit').modal('show');
            }


            function ajaxmodaledit(id) {
                var url = base_url + '/business/stylist';
                $.get(url + '/' + id, function (data) {
                    $('#id').val(data.id);
                    $('#name').val(data.name);
                    $('#phone_number').val(data.phone_number);
                    $('#national_id').val(data.national_id);
                    $('#expertise_area').val(data.area_of_expertise);
                    $('#nhif').val(data.nhif);
                    $('#nssf').val(data.nssf);
                    $('#kra_pin').val(data.kra_pin);
                });
                $('#modal_edit').modal('show');
            }

            $(document).ready(function () {
                oTable = $('#business').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "ajax": base_url + "/business/stylist-table/",
                    "columns": [
                        {
                            data: 'image_url',
                            name: 'image_url',
                            orderable: false,
                            searchable: false,
                            render: function (data, type, full, meta) {
                                if (data) {
                                    var filename = data;
                                    return '<img src="' + filename + '" class="img-bordered img-bordered-orange" width="70" height="70" alt="">';
                                } else {
                                    return '<img src="{!! asset("packages/backend/assets/images/placeholder.jpg") !!}" class="img-bordered img-bordered-orange" width="70" height="70" alt="">';
                                }
                            }
                        },
                        {data: 'name', name: 'name'},
                        {data: 'phone_number', name: 'phone_number'},
                        {data: 'national_id', name: 'national_id'},
                        {data: 'area_of_expertise', name: 'area_of_expertise'},
                        {data: 'action', name: 'action', 'searchable': false}
                    ]
                });
            });

        </script>

        <div class="row ">

        </div>

        <br>


        <!-- Footer -->
    @include("layouts.backend.footer")

    <!-- /footer -->


        <!-- Basic modal -->
        <div id="modal_edit" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title">Stylist</h5>
                    </div>
                    <form action="{{route('stylist.add')}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="inputId" id="id"></input>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Name:</label>
                                <input name="name" id="name" class="form-control" value="{{old('name')}}"
                                       placeholder="eg. Hellen Wanjiru" required></input>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('name') }}</strong>
                            </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Phone number:</label>
                                <input name="phone_number" id="phone_number" class="form-control"
                                       value="{{old('phone_number')}}" placeholder="eg. +2547 704 103 300"
                                       required></input>
                                @if ($errors->has('phone_number'))
                                    <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('phone_number') }}</strong>
                            </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>National ID:</label>
                                <input name="national_id" id="national_id" class="form-control"
                                       value="{{old('national_id')}}" placeholder="" required></input>
                                @if ($errors->has('national_id'))
                                    <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('national_id') }}</strong>
                            </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>KRA Pin:</label>
                                <input name="kra_pin" id="kra_pin" class="form-control" value="{{old('kra_pin')}}"
                                       placeholder=""></input>
                                @if ($errors->has('kra_pin'))
                                    <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('kra_pin') }}</strong>
                            </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Nhif:</label>
                                <input name="nhif" id="nhif" class="form-control" value="{{old('nhif')}}"
                                       placeholder=""></input>
                                @if ($errors->has('nhif'))
                                    <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('nhif') }}</strong>
                            </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Nssf:</label>
                                <input name="nssf" id="nssf" class="form-control" value="{{old('nssf')}}"
                                       placeholder=""></input>
                                @if ($errors->has('nssf'))
                                    <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('nssf') }}</strong>
                            </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Area of expertise:</label>
                                <textarea name="area_expertise" id="expertise_area" class="form-control"
                                          value="{{old('area_expertise')}}" placeholder=""></textarea>
                                @if ($errors->has('area_expertise'))
                                    <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('area_expertise') }}</strong>
                            </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="imageFile">Profile pic</label>
                                <input type="file" class="file-input" data-show-caption="true" data-show-upload="false"
                                       id="imageFile" name="imageFile"
                                       placeholder="Upload an Image" value="{{ old('imageFile') }}" />
                                @if ($errors->has('imageFile'))
                                    <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('imageFile') }}</strong>
                                </span>
                                @endif
                            </div>


                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /basic modal -->


    </div>
    <!-- /content area -->
@endsection