@extends("layouts.backend.template")

@section("content")
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="text-semibold">Income graph</span></h4>
            </div>


        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Income graph</li>
            </ul>


        </div>


    </div>


    <!-- /page header -->
    <div class="content">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (Session::get('error'))
            <div class="alert alert-error alert-danger">
                @if (is_array(Session::get('error')))
                    {{ head(Session::get('error')) }}
                @endif
            </div>
        @endif
        @if(Session::has('flash_message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message') }}
            </div>
        @elseif(Session::has('flash_message_error'))
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message_error') }}
            </div>
        @endif
        @if(Session::has('filter_message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('filter_message') }}
            </div>
        @endif


        <script type="text/javascript">
            google.charts.load('current', {'packages': ['corechart']});
            google.charts.setOnLoadCallback(drawChart);

            function drawChart() {
                var record = {!! json_encode($pchart) !!};
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Date'); // Implicit domain column.
                data.addColumn('number', 'Income'); // Implicit data column.
                //var dayArrayLength = record_final.length;
                console.log(record);
                for (var k in record) {
                    var v = record[k];
                    data.addRow([k, v]);
                }


                var options = {
                    hAxis: {
                        title: 'Dates'
                    },
                    vAxis: {
                        title: 'Values'
                    },
                    title: 'A graph of income',
                    curveType: 'function'
                };


                var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

                chart.draw(data, options);
            }
        </script>


        <!-- Basic pie charts -->


        <!-- /basic pie charts -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel-body">
                    <div class="panel-heading">

                        Filter by date
                    </div>
                    <div class="row">
                        <div class="col-md-12">

                            <form action="{{route('income-filter')}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="col-md-4">
                                    <div class="form-group">

                                        <input id="date" name="date" placeholder="Select a date to filter"
                                               class="form-control daterange-basic"
                                               value="{{old('date')}}" required></input>
                                        @if ($errors->has('date'))
                                            <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('date') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">

                                        <button id="btn-save" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="panel panel-flat">
                    <div class="panel-heading">


                    </div>

                    <div class="panel-body">

                        <div class="chart-container text-center content-group">
                            <div class="" id="curve_chart"></div>
                        </div>
                    </div>
                </div>


            </div>

        </div>


        @include("layouts.backend.footer")
    </div>


@endsection
