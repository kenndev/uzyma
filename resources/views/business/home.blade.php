@extends("layouts.backend.template")

@section("content")
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="text-semibold">Home</span> - Dashboard</h4>
            </div>


        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ul>


        </div>


    </div>


    <!-- /page header -->
    <div class="content">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (Session::get('error'))
            <div class="alert alert-error alert-danger">
                @if (is_array(Session::get('error')))
                    {{ head(Session::get('error')) }}
                @endif
            </div>
        @endif
        @if(Session::has('flash_message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message') }}
            </div>
        @elseif(Session::has('flash_message_error'))
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message_error') }}
            </div>
        @endif

        @if(Session::has('filter_message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('filter_message') }}
            </div>
        @endif


        <div class="row">
            <div class="panel-body">
                {{--<a href="javascript: ajaxmodal()" class="btn btn-primary btn-sm pull-right">Add filter by date<i class="icon-play3 position-right"></i></a>--}}
                <div class="row">
                    <div class="col-md-12">
                        {{--<label>Filter by Date:</label>--}}
                        <form action="{{route('dashboard-filter')}}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="col-md-4">
                                <div class="form-group">

                                    <input id="date" name="date" placeholder="Select a date to filter"
                                           class="form-control daterange-single"
                                           value="{{old('date')}}" required></input>
                                    @if ($errors->has('date'))
                                        <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('date') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">

                                    <button id="btn-save" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel bg-primary">


                    <div class="panel-body">
                        Total amount collected today <h2><code>{!! $today_amount !!} Ksh</code></h2>

                        <a href="{{route('income-graph')}}" class="btn bg-success">View Graph</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel bg-brown">


                    <div class="panel-body">
                        Total expenses today <h2><code>{!! $today_expenses !!} Ksh</code></h2>

                        <a href="{{route('expense-graph')}}" class="btn bg-warning">View Graph</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel bg-success">


                    <div class="panel-body">
                        Total customers today <h2><code>{!! $today_customers !!}</code></h2>

                        <a href="{{route('customers-graph')}}" class="btn bg-primary">View Graph</a>
                    </div>
                </div>
            </div>


        </div>


        @include("layouts.backend.footer")
    </div>


@endsection
