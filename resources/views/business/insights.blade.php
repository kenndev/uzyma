@extends("layouts.backend.template")

@section("content")
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="text-semibold">Home</span> - Insights</h4>
            </div>


        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Insights</li>
            </ul>


        </div>


    </div>


    <!-- /page header -->
    <div class="content">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (Session::get('error'))
            <div class="alert alert-error alert-danger">
                @if (is_array(Session::get('error')))
                    {{ head(Session::get('error')) }}
                @endif
            </div>
        @endif
        @if(Session::has('flash_message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message') }}
            </div>
        @elseif(Session::has('flash_message_error'))
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message_error') }}
            </div>
        @endif
        @if(Session::has('filter_message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('filter_message') }}
            </div>
        @endif


        <script type="text/javascript">
            google.charts.load("current", {packages: ["corechart"]});
            google.charts.setOnLoadCallback(drawChart);

            function drawChart() {

                var record = {!! json_encode($pchart) !!};
                console.log(record);
                // Create our data table.
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'date');
                data.addColumn('number', 'clients served');
                for (var k in record) {
                    var v = record[k];
                    data.addRow([k, v]);
                    console.log(v);
                }
                var options = {
                    title: 'Customer Activity',

                };
                var chart = new google.visualization.LineChart(document.getElementById('piechart_3d'));
                chart.draw(data, options);
            }
        </script>

        <script type="text/javascript">
            google.charts.load("current", {packages: ["corechart"]});
            google.charts.setOnLoadCallback(drawChart);

            function drawChart() {

                var record = {!! json_encode($pchart2) !!};
                console.log(record);
                // Create our data table.
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'date');
                data.addColumn('number', 'clients served');
                for (var k in record) {
                    var v = record[k];
                    data.addRow([k, v]);
                    console.log(v);
                }
                var options = {
                    title: 'Customer Type',
                    is3D: true,

                };
                var chart = new google.visualization.PieChart(document.getElementById('piechart_customer'));
                chart.draw(data, options);
            }
        </script>


        <script type="text/javascript">
            google.charts.load('current', {'packages': ['bar']});
            google.charts.setOnLoadCallback(drawChart);

            function drawChart() {
                var record = {!! json_encode($pchart3) !!};
                var record2 = {!! json_encode($pchart4) !!};
                var record_final = {!! json_encode($pchart) !!};
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Date'); // Implicit domain column.
                data.addColumn('number', 'Refferal'); // Implicit data column.

                data.addColumn('number', 'Walk in');

                for (var k in record_final) {
                    var v = record[k];
                    var f = record2[k];
                    data.addRow([k, v, f]);
                    //data.addRows([k, f]);
                    console.log(v);
                }

                var options = {
                    chart: {
                        title: 'Customer Type',

                    }
                };

                var chart = new google.charts.Bar(
                    document.getElementById('piechart_customer2'));

                chart.draw(data,
                    google.charts.Bar.convertOptions(options));
            }
        </script>
        <script type="text/javascript">
            google.charts.load('current', {'packages': ['bar']});
            google.charts.setOnLoadCallback(drawChart);

            function drawChart() {
                var record = {!! json_encode($pchart6) !!};
                var record2 = {!! json_encode($pchart7) !!};
                var record_final = {!! json_encode($pchart5) !!};
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Month'); // Implicit domain column.
                data.addColumn('number', 'Refferal'); // Implicit data column.

                data.addColumn('number', 'Walk in');

                for (var k in record_final) {
                    var v = record[k];
                    var f = record2[k];
                    data.addRow([k, v, f]);
                    console.log(v);
                }

                var options = {
                    chart: {
                        title: 'Sallon Monthly Activity',
                    }
                };

                var chart = new google.visualization.BarChart(
                    document.getElementById('piechart_customer3'));

                chart.draw(data,
                    google.charts.Bar.convertOptions(options));
            }
        </script>

        <div class="panel-body">
            {{--<a href="javascript: ajaxmodal()" class="btn btn-primary btn-sm pull-right">Add filter by date<i class="icon-play3 position-right"></i></a>--}}
            <div class="row">
                <div class="col-md-12">
                    {{--<label>Filter by Date:</label>--}}
                    <form action="{{route('insights.filter')}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="col-md-4">
                            <div class="form-group">

                                <input id="date" name="date" placeholder="Select a date to filter"
                                       class="form-control daterange-basic"
                                       value="{{old('date')}}" required></input>
                                @if ($errors->has('date'))
                                    <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('date') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">

                                <button id="btn-save" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Basic pie charts -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-heading">

                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>

                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="chart-container text-center content-group">
                            <div class="" id="piechart_3d"></div>
                        </div>
                    </div>
                </div>


            </div>


        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-heading">

                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>

                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="chart-container text-center content-group">
                            <div class="" id="piechart_customer2"></div>
                        </div>
                    </div>
                </div>


            </div>


        </div>

        <!-- /basic pie charts -->
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-flat">
                    <div class="panel-heading">

                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>

                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">

                        <div class="chart-container text-center content-group">
                            <div class="" id="piechart_customer"></div>
                        </div>
                    </div>
                </div>


            </div>

        </div>


        @include("layouts.backend.footer")
    </div>


@endsection
