@extends("layouts.backend.template")

@section("content")
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="text-semibold">Activity</span></h4>
            </div>

        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{url('/admin')}}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Activity</li>
            </ul>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (Session::get('error'))
            <div class="alert alert-error alert-danger">
                @if (is_array(Session::get('error')))
                    {{ head(Session::get('error')) }}
                @endif
            </div>
        @endif
        @if(Session::has('flash_message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message') }}
            </div>
        @elseif(Session::has('flash_message_error'))
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message_error') }}
            </div>
        @endif

        @if(Session::has('filter_message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('filter_message') }}
            </div>
        @endif


    <!-- Basic datatable -->
        <div class="panel panel-flat">
            <div class="panel-heading">

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>

                    </ul>
                </div>
            </div>

            <div class="panel-body">
                {{--<a href="javascript: ajaxmodal()" class="btn btn-primary btn-sm pull-right">Add filter by date<i class="icon-play3 position-right"></i></a>--}}
                <div class="row">
                    <div class="col-md-12">
                        <label>Filter by Date:</label>
                        <form class="form-validate">
                            <div class="col-md-4">
                                <div class="form-group">

                                    <input id="date" name="date" class="form-control daterange-basic"
                                           value="{{old('date')}}" required></input>
                                    @if ($errors->has('date'))
                                        <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('date') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">

                                    <button id="btn-save" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


            <table id="business" class="table table-hover table-condensed table-bordered table-striped">
                <thead>
                <tr>


                    <th>Customer name</th>
                    <th>Customer phone</th>
                    <th>Email</th>
                    <th>Stylist name</th>

                    <th>Date</th>
                    <th>Time taken</th>
                    <th>Style</th>
                    <th>Cost</th>
                    <th>Action</th>
                </tr>
                </thead>

            </table>
        </div>
        <!-- /basic datatable -->

        <script type="text/javascript">
            function ajaxmodal() {
                $('#id').val("");
                $('#date').val("");
                $('#modal_edit').modal('show');
            }

            function ajaxmodalremarket(id) {
                $('#message').val("");
                $('#customer_id').val(id);
                $('#modal_remarket').modal('show');
            }

            $(document).ready(function () {
                oTable = $('#business').DataTable({
                    "destroy": true,
                    "processing": true,
                    "serverSide": true,
                    "ajax": base_url + "/business/customers-activity-table/",
                    "columns": [

                        {data: 'customerName', name: 'customers.name'},
                        {data: 'customerPhone', name: 'customers.phone'},
                        {data: 'email', name: 'email'},
                        {data: 'stylistName', name: 'stylist.name'},

                        {data: 'activity_date', name: 'activity_date'},
                        {data: 'time_taken', name: 'time_taken'},
                        {data: 'style', name: 'style'},
                        {data: 'cost', name: 'cost'},
                        {data: 'action', name: 'action'},

                    ]
                });

                $("#btn-save").click(function (e) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    })
                    e.preventDefault();
                    var formData = {
                        date: $('#date').val(),
                    }
                    //used to determine the http verb to use [add=POST], [update=PUT]
                    console.log(formData);
                    oTable = $('#business').DataTable({
                        "destroy": true,
                        "processing": true,
                        "serverSide": true,
                        "ajax": {
                            "url": base_url + "/business/customers-activity-table-search",
                            "data": formData,
                            "type": "POST"
                        },
//                    "ajax": base_url + "/business/customers-activity-table/",
                        "columns": [

                            {data: 'customerName', name: 'customers.name'},
                            {data: 'customerPhone', name: 'customers.phone'},
                            {data: 'email', name: 'email'},
                            {data: 'stylistName', name: 'stylist.name'},

                            {data: 'activity_date', name: 'activity_date'},
                            {data: 'time_taken', name: 'time_taken'},
                            {data: 'style', name: 'style'},
                            {data: 'cost', name: 'cost'},
                            {data: 'action', name: 'action'},

                        ]
                    });
                });
            });

        </script>


        <div class="row ">

        </div>

        <br>


        <!-- Footer -->
    @include("layouts.backend.footer")

    <!-- /footer -->
        <!-- Basic modal -->

        <div id="modal_edit" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title">Filter by date</h5>
                    </div>
                    <style>
                        .modal-dialog input, .modal-dialog .input-group .form-control .mypick {
                            z-index: inherit;
                        }
                    </style>

                </div>
            </div>
        </div>
        <!-- /basic modal -->

        <!-- Basic modal -->
        <div id="modal_remarket" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title">Remarket</h5>
                    </div>
                    <form action="{{route('remarket')}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="clientId" id="customer_id"></input>
                        <div class="modal-body">

                            <div class="form-group">
                                <label>Subject:</label>
                                <input rows="4" cols="4" name="subject" class="form-control"
                                       value="{{old('subject')}}" placeholder=""></input>
                                @if ($errors->has('subject'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('subject') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label>Remarket message:</label>
                                <textarea rows="4" cols="4" name="remarket" class="form-control"
                                          value="{{old('remarket')}}" placeholder=""></textarea>
                                @if ($errors->has('remarket'))
                                    <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('remarket') }}</strong>
                            </span>
                                @endif
                            </div>

                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /basic modal -->


    </div>
    <!-- /content area -->
@endsection