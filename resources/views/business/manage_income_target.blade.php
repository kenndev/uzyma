@extends("layouts.backend.template")

@section("content")
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Income Targets</span></h4>
        </div>

    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="{{url('/admin')}}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Income Targets</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
        @if (is_array(Session::get('error')))
        {{ head(Session::get('error')) }}
        @endif
    </div>
    @endif
    @if(Session::has('flash_message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message') }}
    </div>
    @elseif(Session::has('flash_message_error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message_error') }}
    </div>
    @endif


    <!-- Basic datatable -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>

                </ul>
            </div>
        </div>

        <div class="panel-body">
            <a href="javascript: ajaxmodal()" class="btn btn-primary btn-sm pull-right">Add an income target<i class="icon-play3 position-right"></i></a>
        </div>


        <table id="business" class="table table-hover table-condensed table-bordered table-striped datatable-responsive">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Amount</th>
                    <th>Action</th>
                </tr>
            </thead>

        </table>
    </div>
    <!-- /basic datatable -->

    <script type="text/javascript">
        function ajaxmodal() {
            $('#id').val("");
            $('#date').val("");
            $('#amount').val("");
            $('#modal_edit').modal('show');
        }

        function ajaxmodaledit(id) {
            var url = base_url + '/business/income-target-detail';
            $.get(url + '/' + id, function (data) {
                $('#id').val(data.id);
                $('#date').val(data.date);
                $('#amount').val(data.amount);
            });
            $('#modal_edit').modal('show');
        }

        $(document).ready(function () {
            oTable = $('#business').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": base_url + "/business/income-target-table",
                "columns": [
                    {data: 'date', name: 'date'},
                    {data: 'amount', name: 'amount'},
                    {data: 'action', name: 'action', 'searchable': false}
                ]
            });
        });

    </script>

    <div class="row ">

    </div>

    <br>




    <!-- Footer -->
    @include("layouts.backend.footer")

    <!-- /footer -->

        <style>
            .modal-backdrop{
                z-index: 20;
            }​
        </style>
    <!-- Basic modal -->
    <div id="modal_edit" class="modal fade" style="z-index:100">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Income Target</h5>
                </div>
                <form action="{{route('income.target.add')}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="inputId" id="id"></input>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>date:</label>
                            <input name="date" id="date" class="form-control daterange-month" value="{{old('date')}}" required></input>
                            @if ($errors->has('date'))
                            <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('date') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Amount (Ksh):</label>
                            <input name="amount" id="amount" class="form-control" value="{{old('amount')}}" placeholder="e.g 1000" required></input>
                            @if ($errors->has('amount'))
                                <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('amount') }}</strong>
                            </span>
                            @endif
                        </div>
                        

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>

                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /basic modal -->



</div>
<!-- /content area -->
@endsection