@extends("layouts.backend.template")

@section("content")
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="text-semibold">Expenses</span></h4>
            </div>

        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{url('/admin')}}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Expenses</li>
            </ul>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (Session::get('error'))
            <div class="alert alert-error alert-danger">
                @if (is_array(Session::get('error')))
                    {{ head(Session::get('error')) }}
                @endif
            </div>
        @endif
        @if(Session::has('flash_message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message') }}
            </div>
        @elseif(Session::has('flash_message_error'))
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message_error') }}
            </div>
    @endif


    <!-- Basic datatable -->
        <div class="panel panel-flat">
            <div class="panel-heading">

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>

                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <a href="javascript: ajaxmodal()" class="btn btn-primary btn-sm pull-right">Add an expense<i
                            class="icon-play3 position-right"></i></a>
            </div>


            <table id="business"
                   class="table table-hover table-condensed table-bordered table-striped datatable-responsive">
                <thead>
                <tr>


                    <th>Item</th>
                    <th>expense type</th>
                    <th>Date</th>
                    <th>Amount</th>
                    <th>Action</th>
                </tr>
                </thead>

            </table>
        </div>
        <!-- /basic datatable -->

        <script type="text/javascript">
            function ajaxmodal() {
                $('#id').val("");
                $('#title').val("");
                $('#date').val("");
                $('#brand').val("");
                $('#description').val("");
                $('#amount').val("");
                $('#expense_type').select2().val("").trigger("change");
                $('#modal_edit').modal('show');
            }


            function ajaxmodaledit(id) {
                var url = base_url + '/business/expenses-detail';
                $.get(url + '/' + id, function (data) {
                    $('#id').val(data.id);
                    $('#title').val(data.expense_title);
                    $('#date').val(data.date);
                    $('#amount').val(data.amount);
                    $('#expense_type').select2().val(data.expense_type_id).trigger("change");
                    $('#brand').val(data.brand);
                    $('#description').val(data.description);
                });
                $('#modal_edit').modal('show');
            }

            $(document).ready(function () {
                oTable = $('#business').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "ajax": base_url + "/business/expenses-table/",
                    "columns": [

                        {data: 'expense_title', name: 'expense_title'},
                        {data: 'expense_type', name: 'expense_type'},
                        {data: 'date', name: 'date'},
                        {data: 'amount', name: 'amount'},
                        {data: 'action', name: 'action', 'searchable': false}
                    ]
                });


                $("select").change(function () {
                    $("select option:selected").each(function () {
                        if ($(this).attr("value") == "1") {
                            $(".box").hide();
                            $(".expense").show();
                        }
                        if ($(this).attr("value") == "2") {
                            $(".box").hide();
                            $(".green").show();
                        }

                        if ($(this).attr("value") == "choose") {
                            $(".box").hide();
                            $(".choose").show();
                        }
                    });
                }).change();


                $('#quantity, #unit_price').keyup(function () {
                    var quantity = parseFloat($('#quantity').val()) || 0;
                    var unit_price = parseFloat($('#unit_price').val()) || 0;

                    $('#amount').val(quantity * unit_price);
                });
            });

        </script>

        <div class="row ">

        </div>

        <br>


        <!-- Footer -->
    @include("layouts.backend.footer")

    <!-- /footer -->

        <style>
            .modal-backdrop {
                z-index: 20;
            }

            ​
        </style>
        <!-- Basic modal -->
        <div id="modal_edit" class="modal fade" style="z-index:100">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title">Expenses</h5>
                    </div>
                    <form action="{{route('expenses.add')}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="inputId" id="id"></input>


                        <div class="modal-body">

                            <div class="form-group">
                                <label>Select expense type:</label>
                                <select class="select" id="expense_type" name="expense_type">
                                    <optgroup label="Select expense type">
                                    @foreach($expense_type as $row)
                                        <option value="{{$row->id}}">{{$row->expense_type}}</option>

                                    @endforeach
                                    </optgroup>

                                </select>
                                @if ($errors->has('expense_type'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('expense_type') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="choose box">
                            </div>

                            <div class="green box"></div>


                            <div class="form-group">
                                <label>Items:</label>
                                <input name="title" id="title" class="form-control" value="{{old('title')}}"
                                       required></input>
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('title') }}</strong>
                            </span>
                                @endif
                            </div>

                            <div class="expense box">
                                <div class="form-group">
                                    <label>Brand:</label>
                                    <input name="brand" id="brand" class="form-control"
                                           value="{{old('brand')}}"
                                           ></input>
                                    @if ($errors->has('brand'))
                                        <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('brand') }}</strong>
                            </span>
                                    @endif
                                </div>

                            </div>
                            <div class="form-group">
                                <label>Description:</label>
                                <textarea cols="5" rows="5" name="description" id="description" class="form-control" value="{{old('description')}}"
                                       placeholder=""></textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('description') }}</strong>
                            </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>date:</label>
                                <input name="date" id="date" class="form-control daterange-single"
                                       value="{{old('date')}}" required></input>
                                @if ($errors->has('date'))
                                    <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('date') }}</strong>
                            </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Amount (Ksh):</label>
                                <input name="amount" id="amount" class="form-control" value="{{old('amount')}}"
                                       placeholder="e.g 1000" required></input>
                                @if ($errors->has('amount'))
                                    <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('amount') }}</strong>
                            </span>
                                @endif
                            </div>


                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /basic modal -->


    </div>
    <!-- /content area -->
@endsection