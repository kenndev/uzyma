@extends("layouts.backend.template")

@section("content")
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="text-semibold">{{$product->name}} Images</span></h4>
            </div>

        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li><a href="{{route('product.sell.view')}}"> Products</a></li>
                <li class="active">{{$product->name}} Images</li>
            </ul>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (Session::get('error'))
            <div class="alert alert-error alert-danger">
                @if (is_array(Session::get('error')))
                    {{ head(Session::get('error')) }}
                @endif
            </div>
        @endif
        @if(Session::has('flash_message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message') }}
            </div>
        @elseif(Session::has('flash_message_error'))
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message_error') }}
            </div>
    @endif


    <!-- Basic datatable -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">{{$product->name}} Images</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <a href="javascript: ajaxmodal()" class="btn btn-outline btn-primary"><i
                            class="glyphicon glyphicon-plus-sign" aria-hidden="true"></i>Add an image</a>

            </div>


            <table class=" table table-hover table-striped dataTable width-full" id="category-table">
                <thead>
                <tr>
                    <th>Image</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /basic datatable -->


        <script>

            $(document).ready(function () {
                $('#category-table').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "ajax": base_url + "/admin/productSellImagesTable/" + {{$product->id}},
                    columns: [
                        {
                            data: 'image_url',
                            name: 'image_url',
                            orderable: false,
                            searchable: false,
                            render: function (data, type, full, meta) {
                                if (data) {
                                    var filename = data;
                                    return '<img src="' + filename + '" class="img-bordered img-bordered-orange" width="70" height="70" alt="">';
                                } else {
                                    return '<img src="{!! asset("packages/backend/assets/photos/placeholder.png") !!}" class="img-bordered img-bordered-orange" width="70" height="70" alt="">';
                                }
                            }
                        },
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
            });
        </script>
        <script>
            function checkDelete() {
                var chk = confirm("Are You Sure To Delete This !");
                if (chk) {
                    return true;
                } else {
                    return false;
                }
            }
        </script>
        <script type="text/javascript">
            function ajaxmodal() {
                $('#id').val("");
                $('#exampleNiftySideFall').modal('show');
            }
        </script>


        <div class="row ">

        </div>

        <br>


        <!-- Footer -->
    @include("layouts.backend.footer")

    <!-- /footer -->


        <!-- Modal -->
        <div class="modal fade modal-slide-in-right" id="exampleNiftySideFall" aria-hidden="true"
             aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title">Activity Images</h4>
                    </div>
                    <div class="modal-body">
                        <!-- Example Basic Form -->

                        <form autocomplete="off" role="form" method="POST" action="{{ route('product.sell.ImagesAdd') }}"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" class="form-control" id="id" name="inputId"/>
                            <input type="hidden" class="form-control" value="{{$product->id}}" name="product_id"/>
                            <div class="form-group row">

                                <div class="col-md-12">
                                    <label class="control-label" for="imageFile">Upload Image</label>
                                    <input type="file" class="file-input" data-show-caption="true"
                                           data-show-upload="false" id="imageFile" name="imageFile"
                                           placeholder="Upload an Image" value="{{ old('imageFile') }}" required/>
                                    @if ($errors->has('imageFile'))
                                        <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('imageFile') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>

                            <!-- End Example Basic Form -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default margin-0" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Modal -->


    </div>
    <!-- /content area -->
@endsection
