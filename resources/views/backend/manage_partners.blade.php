@extends("layouts.backend.template")

@section("content")
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Partners</span></h4>
        </div>

    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="{{url('/admin')}}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Partners</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
        @if (is_array(Session::get('error')))
        {{ head(Session::get('error')) }}
        @endif
    </div>
    @endif
    @if(Session::has('flash_message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message') }}
    </div>
    @elseif(Session::has('flash_message_error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message_error') }}
    </div>
    @endif


    <!-- Basic datatable -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Partners</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <a href="javascript: ajaxmodal()" class="btn btn-primary btn-sm pull-right">Add a Partner<i class="icon-play3 position-right"></i></a>
        </div>


        <table id="business" class="table table-hover table-condensed table-bordered table-striped">
            <thead>
                <tr>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Phone number</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>

        </table>
    </div>
    <!-- /basic datatable -->

    <script type="text/javascript">
        function ajaxmodal() {
            $('#id').val("");
            $('#name').val("");
            $('#email').val("");
            $('#phone_number').val("");
            $('#password').val("");
            $('#password-confirm').val("");
            $('#modal_edit').modal('show');
        }




        function ajaxmodaledit(id) {
            var url = base_url + '/admin/business-types-details';
            $.get(url + '/' + id, function (data) {
                $('#id').val(data.id);
                $('#business_type').val(data.name);
            });
            $('#modal_edit').modal('show');
        }

        $(document).ready(function () {
            oTable = $('#business').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": base_url + "/admin/partners-table/",
                "columns": [
                    {data: 'image', name: 'image', orderable: false, searchable: false, render: function (data, type, full, meta) {
                            if (data) {

                                return '<img src="' + data + '" class="img-preview" alt="">';
                            } else {
                                return '<img src="{!! asset("packages/backend/assets/images/placeholder.jpg") !!}" class="img-preview" alt="">';
                            }
                        }},
                    {data: 'partner_name', name: 'partner_name'},
                    {data: 'phone_number', name: 'phone_number'},
                    {data: 'email', name: 'email'},
                    {data: 'status', name: 'status', orderable: false, searchable: false, render: function (data, type, full, meta) {
                            if (data == 0) {
                                return '<span class="label label-warning">Not Activated</span>';
                            } else {
                                return '<span class="label label-success">Activated</span>';
                            }
                        }},
                    {data: 'action', name: 'action', 'searchable': false}
                ]
            });
        });

    </script>

    <div class="row ">

    </div>

    <br>




    <!-- Footer -->
    @include("layouts.backend.footer")

    <!-- /footer -->


    <!-- Basic modal -->
    <div id="modal_edit" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Partners</h5>
                </div>
                <form action="{{route('partners.add')}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="inputId" id="id"></input>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Name:</label>
                            <input name="name" id="name" class="form-control" value="{{old('name')}}" placeholder="partner name" required></input>
                            @if ($errors->has('name'))
                            <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label>E-Mail Address</label>
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label>Password</label>
                            <input id="password" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Confirm Password</label>

                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        </div>
                        <div class="form-group">
                            <label>Phone number:</label>
                            <input name="phone_number" id="phone_number" class="form-control" value="{{old('phone_number')}}" placeholder="Phone number" required></input>
                            @if ($errors->has('phone_number'))
                            <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('phone_number') }}</strong>
                            </span>
                            @endif
                        </div>



                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>

                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /basic modal -->



</div>
<!-- /content area -->
@endsection