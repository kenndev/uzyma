@extends("layouts.backend.template")

@section("content")
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Help Centre</span></h4>
        </div>

    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="{{url('/admin')}}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Help Centre</li>
        </ul>

    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if(Session::has('flash_message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message') }}
    </div>
    @endif
    <!-- Basic datatable -->
    <div class="panel panel-flat">
        <div class="panel-heading">
     <h5 class="panel-title">Respond to App</h5>
         
        </div>

        <div class="panel-body">
                    <a href="javascript: ajaxmodal()" class="btn btn-outline btn-primary"><i class="glyphicon glyphicon-plus-sign" aria-hidden="true"></i>Add Record</a>
           
             <div class="col-lg-12">
           
                <table class="table table-stripped">
                <thead>
                 <tr>
                    <th>Title</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                @foreach($help2 as $row2)
                <tr>
                    <td>{{$row2->title}}</td>
                     <td>{{$row2->description}}</td>
                     </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
            
        </div>
        </div>
          <div class="panel panel-flat">
        <div class="panel-heading">
     <h5 class="panel-title">Details from App</h5>
            
        </div>
        <div class="panel-body">

            <div class="col-lg-12">
            
                <table class="table table-stripped">
                <thead>
                 <tr>
                    <th>Title</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                @foreach($help as $row)
                    <tr>
                    <td>{{$row->title}}</td>
                     <td>{{$row->description}}</td>
                     </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
            
            
        </div>


    </div>
    <!-- /basic datatable -->

    <div class="row">

    </div>

    <br>

        <script type="text/javascript">
        function ajaxmodal() {
            $('#id').val("");


            $('#modal_edit').modal('show');
        }
    </script>

     <!-- Basic modal -->
    <div id="modal_edit" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Help Centre</h5>
                </div>
                <form action="{{route('help.center.add')}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="inputId" id="id"></input>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Title:</label>
                            <input name="title" id="title" class="form-control" value="{{old('title')}}" placeholder="Tile" required></input>
                            @if ($errors->has('title'))
                            <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('title') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label>Description</label>
                            <textarea id="description" class="form-control" name="description" value="{{ old('description') }}" required ></textarea>

                            @if ($errors->has('description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                            @endif
                        </div>




                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>

                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /basic modal -->



    <!-- Footer -->
    @include("layouts.backend.footer")

    <!-- /footer -->

</div>


<!-- /content area -->
@endsection