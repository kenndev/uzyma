@extends("layouts.backend.template")

@section("content")
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Settings</span></h4>
        </div>

    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="{{url('/admin')}}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Settings</li>
        </ul>

    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if(Session::has('flash_message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message') }}
    </div>
    @endif
    <!-- Basic datatable -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Settings</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">

            <form role="form" enctype="multipart/form-data" method="POST" action="{{route('settingsAdd')}}" />
                {{ csrf_field() }}
                <input type="hidden" name="inputId" value="{{ isset($settings->id) ? $settings->id : null }}">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Site name:</label>
                        <input name="site_name" id="service_name" class="form-control" placeholder="Site name" value="{{ isset($settings->site_name) ? $settings->site_name : null }}"></input>
                    </div>
                    <div class="form-group">
                        <label>Site email:</label>
                        <input name="site_email" id="service_name" class="form-control" placeholder="Site email" value="{{ isset($settings->site_email) ? $settings->site_email : null }}"></input>
                    </div>
                    <div class="form-group">
                        <label>Site phone:</label>
                        <input name="site_phone" id="site_phone" class="form-control" placeholder="Site phone" value="{{ isset($settings->site_phone) ? $settings->site_phone : null }}"></input>
                    </div>

                    <div class="form-group">
                        <label>Address:</label>
                        <textarea rows="5" cols="5" id="site_address" name="site_address" class="form-control" placeholder="Enter address">{{ isset($settings->address) ? $settings->address : null }}</textarea>
                    </div>


                </div>

                <div class="modal-footer">

                    <button type="submit" class="btn btn-primary" id="btn-save" value="add">Submit form</button>
                </div>
            </form>
        </div>


    </div>
    <!-- /basic datatable -->

    <div class="row">

    </div>

    <br>




    <!-- Footer -->
    @include("layouts.backend.footer")

    <!-- /footer -->

</div>


<!-- /content area -->
@endsection