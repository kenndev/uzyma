@extends("layouts.backend.template")

@section("content")
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Business types</span></h4>
        </div>

    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="{{url('/admin')}}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Business types</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
        @if (is_array(Session::get('error')))
        {{ head(Session::get('error')) }}
        @endif
    </div>
    @endif
    @if(Session::has('flash_message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message') }}
    </div>
    @elseif(Session::has('flash_message_error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message_error') }}
    </div>
    @endif


    <!-- Basic datatable -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Business types</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <a href="javascript: ajaxmodal()" class="btn btn-primary btn-sm pull-right">Add a Business type<i class="icon-play3 position-right"></i></a>
        </div>


        <table id="business" class="table table-hover table-condensed table-bordered table-striped">
            <thead>
                <tr>

                   
                    <th>Name</th>
                    <th>Action</th>
                </tr>
            </thead>

        </table>
    </div>
    <!-- /basic datatable -->

    <script type="text/javascript">
        function ajaxmodal() {
            $('#id').val("");
            $('#business_type').val("");
            $('#modal_edit').modal('show');
        }




        function ajaxmodaledit(id) {
            var url = base_url + '/admin/business-types-details';
            $.get(url + '/' + id, function (data) {
                $('#id').val(data.id);
                $('#business_type').val(data.name);
            });
            $('#modal_edit').modal('show');
        }

        $(document).ready(function () {
            oTable = $('#business').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": base_url + "/admin/business-types-table/",
                "columns": [
                   
                    {data: 'name', name: 'name'},
                    {data: 'action', name: 'action', 'searchable': false}
                ]
            });
        });

    </script>

    <div class="row ">

    </div>

    <br>




    <!-- Footer -->
    @include("layouts.backend.footer")

    <!-- /footer -->


    <!-- Basic modal -->
    <div id="modal_edit" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Business types</h5>
                </div>
                <form action="{{route('business.types.add')}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="inputId" id="id"></input>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Name:</label>
                            <input name="business_type" id="business_type" class="form-control" value="{{old('business_type')}}" placeholder="eg. gym" required></input>
                            @if ($errors->has('business_type'))
                            <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('business_type') }}</strong>
                            </span>
                            @endif
                        </div>
                        

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>

                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /basic modal -->



</div>
<!-- /content area -->
@endsection