@extends("layouts.backend.template")

@section("content")
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Users</span></h4>
        </div>

    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="{{url('/admin')}}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Users</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
        @if (is_array(Session::get('error')))
        {{ head(Session::get('error')) }}
        @endif
    </div>
    @endif
    @if(Session::has('flash_message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message') }}
    </div>
    @elseif(Session::has('flash_message_error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message_error') }}
    </div>
    @endif


    <!-- Basic datatable -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Users</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <!--<a href="javascript: ajaxmodal()" class="btn btn-primary btn-sm pull-right">Add a Difficult Level<i class="icon-play3 position-right"></i></a>-->
        </div>


        <table id="business" class="table table-hover table-condensed table-bordered table-striped">
            <thead>
                <tr>


                    <th>Email</th>
                    <th>Name</th>
                    <th>Contact</th>
                    <th>Gender</th>
                    <th>Age</th>
                    <th>Date</th>
<!--                    <th>Action</th>-->
                </tr>
            </thead>

        </table>
    </div>
    <!-- /basic datatable -->

    <script type="text/javascript">
        function ajaxmodal() {
            $('#id').val("");
            $('#business_type').val("");
            $('#modal_edit').modal('show');
        }




        function ajaxmodaledit(id) {
            var url = base_url + '/admin/difficultyLevel-details';
            $.get(url + '/' + id, function (data) {
                $('#id').val(data.id);
                $('#difficult_level').val(data.level_name);
            });
            $('#modal_edit').modal('show');
        }
        
        function getAge(fromdate, todate) {
    if (todate)
        todate = new Date(todate);
    else
        todate = new Date();

    var age = [], fromdate = new Date(fromdate),
            y = [todate.getFullYear(), fromdate.getFullYear()],
            ydiff = y[0] - y[1],
            m = [todate.getMonth(), fromdate.getMonth()],
            mdiff = m[0] - m[1],
            d = [todate.getDate(), fromdate.getDate()],
            ddiff = d[0] - d[1];

    if (mdiff < 0 || (mdiff === 0 && ddiff < 0))
        --ydiff;
    if (mdiff < 0)
        mdiff += 12;
    if (ddiff < 0) {
        fromdate.setMonth(m[1] + 1, 0);
        ddiff = fromdate.getDate() - d[1] + d[0];
        --mdiff;
    }
    if (ydiff > 0)
        age.push(ydiff + ' year' + (ydiff > 1 ? 's ' : ' '));
    if (mdiff > 0)
        age.push(mdiff + ' month' + (mdiff > 1 ? 's' : ''));
    if (ddiff > 0)
        age.push(ddiff + ' day' + (ddiff > 1 ? 's' : ''));
    if (age.length > 1)
        age.splice(age.length - 1, 0, ' and ');
    return age.join('');
}

        $(document).ready(function () {
            oTable = $('#business').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": '{!! route('users.table') !!}',
                "columns": [
                    {data: 'email', name: 'email'},
                    {data: 'name', name: 'name'},
                    {data: 'contact', name: 'contact'},
                    {data: 'gender', name: 'gender'},
                    {data: 'birth_date', name: 'birth_date', orderable: false, searchable: false, render: function (data, type, full, meta) {
                            return getAge(data);
                        }},
                   {data: 'created_at', name: 'created_at', 'searchable': false}
                ]
            });
        });

    </script>

    <div class="row ">

    </div>

    <br>




    <!-- Footer -->
    @include("layouts.backend.footer")

    <!-- /footer -->


    <!-- Basic modal -->
    <div id="modal_edit" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Business types</h5>
                </div>
                <form action="{{route('difficultyLevel.add')}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="inputId" id="id"></input>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Name:</label>
                            <input name="difficult_level" id="difficult_level" class="form-control" value="{{old('difficult_level')}}" placeholder="eg. bigginer" required></input>
                            @if ($errors->has('difficult_level'))
                            <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('difficult_level') }}</strong>
                            </span>
                            @endif
                        </div>


                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>

                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /basic modal -->



</div>
<!-- /content area -->
@endsection