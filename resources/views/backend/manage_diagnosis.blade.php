@extends("layouts.backend.template")

@section("content")
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Diagnosis for {{$patient->names}}</span></h4>
        </div>

    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="{{url('/admin')}}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">{{$patient->names}}</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
        @if (is_array(Session::get('error')))
        {{ head(Session::get('error')) }}
        @endif
    </div>
    @endif
    @if(Session::has('flash_message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message') }}
    </div>
    @elseif(Session::has('flash_message_error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message_error') }}
    </div>
    @endif


    <!-- Basic datatable -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Diagnosis for {{$patient->names}}</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <a href="{{route('diagnosisPatientAdd', ['id' => $patient->id])}}" class="btn btn-primary btn-sm pull-right ">Add Diagnosis<i class="icon-play3 position-right"></i></a>
        </div>


        <table id="patient" class="table table-hover table-condensed table-bordered table-striped">
            <thead>
                <tr>
                    <th>Date Operation</th>
                    <th>Action</th>
                </tr>
            </thead>

        </table>
    </div>
    <!-- /basic datatable -->

    <script type="text/javascript">

        function ajaxmodaldetails(id) {
        jQuery('#modal_ajax .modal-body').html('<div style="text-align:center;margin-top:200px;"><img src="{{asset("packages/backend/assets/images/preloader.gif")}}" style="height:25px;" /></div>');
        jQuery('#modal_ajax').modal('show', {backdrop: 'true'});
        // SHOW AJAX RESPONSE ON REQUEST SUCCESS
        $.ajax({
        url: base_url + "/admin/diagnosisD/" + id,
                success: function (response)
                {
                jQuery('#modal_ajax .modal-body').html(response);
                }
        });
        }

        $(document).ready(function () {
        oTable = $('#patient').DataTable({
        "processing": true,
                "serverSide": true,
                "ajax": base_url + "/admin/diagnosisTable/" + {{$patient->id}},
                "columns": [
                {data: 'date_opperation', name: 'date_opperation'},
                {data: 'action', name: 'action', 'searchable': false}
                ]
        });
        });

    </script>

    <div class="row ">

    </div>

    <br>




    <!-- Footer -->
    @include("layouts.backend.footer")

    <!-- /footer -->


    <div class="modal fade" id="modal_ajax">
        <div class="modal-dialog modal-lg" >
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Treatment Details</h4>
                </div>

                <div class="modal-body" style="height:500px; overflow:auto;">



                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


</div>
<!-- /content area -->
@endsection