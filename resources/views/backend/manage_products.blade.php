@extends("layouts.backend.template")

@section("content")
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="text-semibold">Home</span> - Products</h4>
            </div>


        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Products</li>
            </ul>


        </div>


    </div>


    <!-- /page header -->
    <div class="content">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (Session::get('error'))
            <div class="alert alert-error alert-danger">
                @if (is_array(Session::get('error')))
                    {{ head(Session::get('error')) }}
                @endif
            </div>
        @endif
        @if(Session::has('flash_message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message') }}
            </div>
        @elseif(Session::has('flash_message_error'))
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message_error') }}
            </div>
    @endif



    <!-- Basic datatable -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Products</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <a href="{{route('product.sell.new')}}" class="btn btn-primary btn-sm pull-right">Add product<i
                            class="icon-play3 position-right"></i></a>
            </div>


            <table id="business" class="table table-hover table-condensed table-bordered table-striped">
                <thead>
                <tr>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>

            </table>
        </div>
        <!-- /basic datatable -->


        <script type="text/javascript">
            
            function ajaxmodaldetails(id) {
                var url = base_url + '/admin/activity-details';
                $.get(url + '/' + id, function (data) {

                    document.getElementById("activity_name2").innerHTML = data.activity_name;
                    document.getElementById("activity_description2").innerHTML = data.activity_description;
                    document.getElementById("service2").innerHTML = data.service;
                    document.getElementById("date").innerHTML = data.date;
                    document.getElementById("time").innerHTML = data.time;
                    document.getElementById("address").innerHTML = data.address;
                    document.getElementById("cancelationdate").innerHTML = data.cancel;
                    document.getElementById("level_name").innerHTML = data.level_name;
                });
                $('#modal_details').modal('show');
            }

            $(document).ready(function () {

                oTable = $('#business').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "ajax": '{!! route('product.sell.table') !!}',
                    "columns": [
                        {
                            data: 'image',
                            name: 'image',
                            orderable: false,
                            searchable: false,
                            render: function (data, type, full, meta) {
                                if (data) {

                                    return '<img src="' + data + '" class="img-preview" alt="">';
                                } else {
                                    return '<img src="{!! asset("packages/backend/assets/images/placeholder.jpg") !!}" class="img-preview" alt="">';
                                }
                            }
                        },
                        {data: 'name', name: 'name'},
                        {data: 'price', name: 'price'},
                        {data: 'quantity', name: 'quantity'},
                        {
                            data: 'activated',
                            name: 'activated',
                            orderable: false,
                            searchable: false,
                            render: function (data, type, full, meta) {
                                if (data == 0) {
                                    return '<span class="label label-warning">Not Activated</span>';
                                } else {
                                    return '<span class="label label-success">Activated</span>';
                                }
                            }
                        },
                        {data: 'action', name: 'action', 'searchable': false}
                    ]
                });
            });

        </script>


        <div class="row ">

        </div>

        <br>

    @include("layouts.backend.footer")




    <!-- Basic modal -->
        <div id="modal_details" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title">Activity</h5>
                    </div>
                    <div class="modal-body">
                        <table class="table table-bordered table-striped">
                            <tr>
                                <th>
                                    Activity name
                                </th>
                                <td>
                                    <p id="activity_name2"></p>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Activity description
                                </th>
                                <td>
                                    <p id="activity_description2"></p>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Service type
                                </th>
                                <td>
                                    <p id="service2"></p>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Date
                                </th>
                                <td>
                                    <p id="date"></p>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Time
                                </th>
                                <td>
                                    <p id="time"></p>
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    Address
                                </th>
                                <td>
                                    <p id="address"></p>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Cancelation date
                                </th>
                                <td>
                                    <p id="cancelationdate"></p>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Difficulty level
                                </th>
                                <td>
                                    <p id="level_name"></p>
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>
        </div>
        <!-- /basic modal -->


    </div>


@endsection
