@extends("layouts.backend.template")

@section("content")
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Home</span> - Dashboard</h4>
        </div>


    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ul>


    </div>



</div>


<!-- /page header -->
<div class="content">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
        @if (is_array(Session::get('error')))
        {{ head(Session::get('error')) }}
        @endif
    </div>
    @endif
    @if(Session::has('flash_message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message') }}
    </div>
    @elseif(Session::has('flash_message_error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message_error') }}
    </div>
    @endif



    <div class="row">
        <div class="col-lg-4">
            <div class="panel bg-info">
                <div class="panel-heading">
                    <h6 class="panel-title">Partners</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    Number of partners 
                    <a href="{{route('partners')}}" class="btn bg-indigo-300 btn-rounded btn-icon btn-xs">
                        <span class="letter-icon">{{$partners}}</span>
                    </a> 

                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="panel bg-purple-300">
                <div class="panel-heading">
                    <h6 class="panel-title">Activities</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    Number of activities 
                    <a href="{{route('admin.activity')}}" class="btn bg-pink-300 btn-rounded btn-icon btn-xs">
                        <span class="letter-icon">{{$activities}}</span>
                    </a> 
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="panel bg-brown-300">
                <div class="panel-heading">
                    <h6 class="panel-title">Bookings</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    Number of bookings
                    <a href="{{route('bookings')}}" class="btn bg-pink-300 btn-rounded btn-icon btn-xs">
                        <span class="letter-icon">{{$bookings}}</span>
                    </a> 
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <div class="panel bg-green-300">
                <div class="panel-heading">
                    <h6 class="panel-title">Users</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    Number of users 
                    <a href="{{route('users')}}" class="btn bg-indigo-300 btn-rounded btn-icon btn-xs">
                        <span class="letter-icon">{{$users}}</span>
                    </a> 

                </div>
            </div>
        </div>

    </div>





    @include("layouts.backend.footer")
</div>




@endsection
