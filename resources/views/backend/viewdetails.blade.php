<?php
$patient = \App\Diagnosis::leftJoin('patient', 'diagnosis_new.patient_no', '=', 'patient.id')
        ->leftJoin('theatre', 'diagnosis_new.theatre', '=', 'theatre.id')
        ->select('diagnosis_new.id', 'theatre.theatre_name', 'patient.names', 'patient.patient_no', 'diagnosis_new.date_opperation', 'diagnosis_new.treatment', 'diagnosis_new.treatment1', 'diagnosis_new.treatment2', 'diagnosis_new.treatment3', 'diagnosis_new.investigation')
        ->where('diagnosis_new.id', $param2)
        ->first();
?>
<table class="table table-bordered table-striped">
    <tr>
        <th>Patient Number</th>
        <td><p id="patient_no">{{$patient->patient_no}}</p></td>
    </tr>
    <tr>
        <th>Patient Name</th>
        <td><p id="names"></p>{{$patient->names}}</td>
    </tr>
    <tr>
        <th>Date of Operation</th>
        <td><div id="date_opperation"></div>{{$patient->date_opperation}}</td>
    </tr>
    <tr>
        <th>Theatre</th>
        <td><div id="theatre"></div>{{$patient->theatre_name}}</td>
    </tr>
    <tr>
        <th>Diagnosis</th>
        <?php
        $treatmentTreat = \App\Treatment::join('diagnosis_ailments', 'treatment.treatment_id', '=', 'diagnosis_ailments.id')
                ->where('diagnosis_id', $param2)
                ->select('diagnosis_ailments.diagnosis')
                ->get();
        ?>
        <td> <?php foreach ($treatmentTreat as $row) { ?> {{$row->diagnosis}} <br> <?php } ?></td>
    </tr>

</table>

<fieldset class="content-group">
    <legend class="text-bold">Investigations</legend>
    <table class="table table-bordered" width="100%" border="1" style="border-collapse:collapse;">
        <thead>
            <tr>
                <th>Image</th>
                <th>Type</th>
                <th>Name</th>
            </tr>
        </thead>

        <tbody>
            <?php foreach (json_decode($patient->investigation) as $inv) {
                ?>
                <tr>
                    <td>@if($inv->image)<a target="_blank" class="btn btn-xs btn-success" href="{{asset('packages/uploads/investigations/'.$inv->image)}}">Download file</a>@endif</td>
                    <td>{{$inv->type}}</td>
                    <td>{{$inv->name}}</td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</fieldset>