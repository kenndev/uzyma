@extends("layouts.backend.template")

@section("content")
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="text-semibold">Add Questions</span></h4>
            </div>


        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li><a href="{{route('questions')}}"> Questions</a></li>
                <li class="active">Add a question</li>
            </ul>


        </div>


    </div>


    <!-- /page header -->
    <div class="content">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (Session::get('error'))
            <div class="alert alert-error alert-danger">
                @if (is_array(Session::get('error')))
                    {{ head(Session::get('error')) }}
                @endif
            </div>
        @endif
        @if(Session::has('flash_message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message') }}
            </div>
        @elseif(Session::has('flash_message_error'))
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message_error') }}
            </div>
        @endif


    <!-- Basic datatable -->
        <div class="panel panel-flat">
            <div class="panel-heading">

                <div class="heading-elements">

                </div>
            </div>

            <div class="panel-body">
                <form role="form" enctype="multipart/form-data" method="POST"
                      action="{{route('questions.add')}}"/>
                {{ csrf_field() }}
                <input type="hidden" id="id" name="inputId" value="{{ isset($activity->id) ? $activity->id : null }}">


                <div class="form-group col-lg-8">
                    <label>Question:</label>
                    <input value="{{ isset($activity->title) ? $activity->title : null }}" name="title" required
                           id="name" class="form-control" placeholder="Question"
                           value="{{old('title')}}"></input>
                    @if ($errors->has('title'))
                        <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('title') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group col-lg-8">
                    <label>Date:</label>
                    <input value="{{ isset($activity->date) ? $activity->date : null }}" type="text"
                           class="form-control daterange-single" name="date" id="date"
                           placeholder="Choose date&hellip;">
                    @if ($errors->has('date'))
                        <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('date') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group col-lg-8">
                    <label>Period:</label>
                    <input value="{{ isset($activity->period) ? $activity->period : null }}" name="period" required
                           id="name" class="form-control" placeholder="Period e.g morning"
                           value="{{old('period')}}"></input>
                    @if ($errors->has('period'))
                        <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('period') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group col-lg-6">
                    <label>Answer 1:</label>
                    <input value="{{ isset($activity->answer1) ? $activity->answer1 : null }}" name="answer[]" required
                           id="name" class="form-control" placeholder="Answer 1"
                           value="{{old('answer1')}}"></input>
                    @if ($errors->has('answer1'))
                        <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('answer1') }}</strong>
                    </span>
                    @endif

                </div>
                <div class="form-group col-lg-4">
                    <input style="margin-top: 13%" type="radio" id="contactChoice1"
                           name="correct" value="1">
                    <label for="contactChoice1">correct</label>
                </div>

                <div class="form-group col-lg-6">
                    <label>Answer 2:</label>
                    <input value="{{ isset($activity->answer2) ? $activity->answer2 : null }}" name="answer[]" required
                           id="name" class="form-control" placeholder="Answer 2"
                           value="{{old('answer2')}}"></input>
                    @if ($errors->has('answer2'))
                        <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('answer2') }}</strong>
                    </span>
                    @endif

                </div>
                <div class="form-group col-lg-4">
                    <input style="margin-top: 13%" type="radio" id="contactChoice1"
                           name="correct" value="2">
                    <label for="contactChoice1">correct</label>
                </div>

                <div class="form-group col-lg-6">
                    <label>Answer 3:</label>
                    <input value="{{ isset($activity->answer3) ? $activity->answer3 : null }}" name="answer[]" required
                           id="name" class="form-control" placeholder="Answer 3"
                           value="{{old('answer3')}}"></input>
                    @if ($errors->has('answer3'))
                        <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('answer3') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group col-lg-4">
                    <input style="margin-top: 13%" type="radio" id="contactChoice1"
                           name="correct" value="3">
                    <label for="contactChoice1">correct</label>
                </div>

                <div class="form-group col-lg-6">
                    <label>Answer 4:</label>
                    <input value="{{ isset($activity->answer4) ? $activity->answer4 : null }}" name="answer[]"
                           id="name" class="form-control" placeholder="Answer 4"
                           value="{{old('answer4')}}"></input>
                    @if ($errors->has('answer4'))
                        <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('answer4') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group col-lg-4">
                    <input style="margin-top: 13%" type="radio" id="contactChoice1"
                           name="correct" value="4">
                    <label for="contactChoice1">correct</label>
                </div>

                <div class="row">
                    <div class="form-group col-lg-6">
                        <button class="btn btn-lg btn-primary" type="submit">save</button>
                    </div>
                </div>

                </form>
            </div>


        </div>


        <div class="row ">

        </div>

        <br>

        @include("layouts.backend.footer")
    </div>


@endsection
