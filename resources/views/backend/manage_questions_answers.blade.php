@extends("layouts.backend.template")

@section("content")
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="text-semibold">Questions and Answers</span></h4>
            </div>

        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{url('/admin')}}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Questions</li>
            </ul>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (Session::get('error'))
            <div class="alert alert-error alert-danger">
                @if (is_array(Session::get('error')))
                    {{ head(Session::get('error')) }}
                @endif
            </div>
        @endif
        @if(Session::has('flash_message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message') }}
            </div>
        @elseif(Session::has('flash_message_error'))
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message_error') }}
            </div>
    @endif


    <!-- Basic datatable -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Questions</h5>
                <div class="heading-elements">

                </div>
            </div>

            <div class="panel-body">
                <a href="{{route('questions.add.view')}}" class="btn btn-outline btn-primary"><i class="glyphicon glyphicon-plus-sign" aria-hidden="true"></i>Add a questions</a>

            </div>

            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th>Question</th>
                    <th>Answer</th>
                    <th>Correct answer</th>
                </tr>
                </thead>
                <tbody>
                @foreach($questions as $row)
                    <tr>
                        <td>{{$row->id}}</td>

                        <td>
                            {{getCorrectAnswer($row->id)}}

                        </td>
                        <td>
                            <a href="#">
                                @foreach(getAnswers($row->id) as $row)
                                    {{$row->answer}} <br>
                                @endforeach
                            </a>
                        </td>

                    </tr>
                @endforeach

                </tbody>
            </table>

            {{--<table class=" table table-hover table-striped dataTable width-full" id="category-table">--}}
            {{--<thead>--}}
            {{--<tr>--}}
            {{--<th>Questions</th>--}}
            {{--<th>Answers</th>--}}
            {{--<th>Action</th>--}}
            {{--</tr>--}}
            {{--</thead>--}}
            {{--<tbody>--}}
            {{--@foreach($questions as $row)--}}
            {{--<tr>--}}
            {{--{{$row->title}}--}}
            {{--</tr>--}}
            {{--<tr>--}}
            {{--@foreach(getAnswers($row->id) as $row)--}}
            {{--{{$row->answer}}--}}
            {{--@endforeach--}}
            {{--</tr>--}}
            {{--<tr>--}}

            {{--</tr>--}}
            {{--@endforeach--}}
            {{--</tbody>--}}

            {{--</table>--}}
        </div>
        <!-- /basic datatable -->

        <div class="row ">

        </div>

        <br>


        <!-- Footer -->
    @include("layouts.backend.footer")

    <!-- /footer -->


        <!-- Modal -->
        <div class="modal fade modal-slide-in-right" id="exampleNiftySideFall" aria-hidden="true"
             aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title">Slider</h4>
                    </div>
                    <div class="modal-body">
                        <!-- Example Basic Form -->

                        <form autocomplete="off" role="form" method="POST" action="{{ route('slider.add') }}"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" class="form-control" id="id" name="inputId"/>


                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label class="control-label" for="imageFile">Slider name</label>
                                    <input type="text" class="form-control" id="slider_name" name="slider_name"
                                           placeholder="Slider name" value="{{ old('slider_name') }}"/>
                                    @if ($errors->has('slider_name'))
                                        <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('slider_name') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label class="control-label" for="imageFile">Upload Image</label>
                                    <input type="file" class="file-input" data-show-caption="true"
                                           data-show-upload="false" id="imageFile" name="imageFile"
                                           placeholder="Upload an Image" value="{{ old('imageFile') }}"/>
                                    @if ($errors->has('imageFile'))
                                        <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('imageFile') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>

                            <!-- End Example Basic Form -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default margin-0" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Modal -->


    </div>
    <!-- /content area -->
@endsection



