@extends("layouts.backend.template")

@section("content")
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Add Products</span></h4>
        </div>


    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="{{route('product.sell.view')}}"> Products</a></li>
            <li class="active">Add Products</li>
        </ul>


    </div>



</div>


<!-- /page header -->
<div class="content">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
        @if (is_array(Session::get('error')))
        {{ head(Session::get('error')) }}
        @endif
    </div>
    @endif
    @if(Session::has('flash_message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message') }}
    </div>
    @elseif(Session::has('flash_message_error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message_error') }}
    </div>
    @endif


    <!-- Basic datatable -->
    <div class="panel panel-flat">
        <div class="panel-heading">

            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <form role="form" enctype="multipart/form-data" method="POST" action="{{route('product.sell.addedit')}}" />
            {{ csrf_field() }}
            <input type="hidden" id="id" name="inputId" value="{{ isset($activity->id) ? $activity->id : null }}">

            <div class="row"> 
                <div class="form-group col-lg-8">
                    <label>Product name:</label>
                    <input value="{{ isset($activity->name) ? $activity->name : null }}" name="name" required id="name" class="form-control" placeholder="Product name" value="{{old('name')}}"></input>
                    @if ($errors->has('name'))
                    <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>

                
            </div>

            <div class="row">
                <div class="form-group col-lg-8">
                    <label>Product Description:</label>
                    <textarea name="description" rows="5" required cols="5" id="description" class="form-control" placeholder="Activity Description" value="{{old('description')}}">{{ isset($activity->description) ? $activity->description : null }}</textarea>
                    @if ($errors->has('description'))
                    <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('description') }}</strong>
                    </span>
                    @endif
                </div>

            </div>
            <div class="row">
            <div class="form-group col-lg-8">
                <label>Price:</label>
                <input value="{{ isset($activity->price) ? $activity->price : null }}" name="price" required id="price" class="form-control" placeholder="Price" value="{{old('price')}}"></input>
                @if ($errors->has('price'))
                    <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('price') }}</strong>
                    </span>
                @endif
            </div>
            </div>
            <div class="row">
            <div class="form-group col-lg-8">
                <label>Quantity:</label>
                <input value="{{ isset($activity->quantity) ? $activity->quantity : null }}" name="quantity" required id="quantity" class="form-control" placeholder="quantity" value="{{old('quantity')}}"></input>
                @if ($errors->has('quantity'))
                    <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('quantity') }}</strong>
                    </span>
                @endif
            </div>
            </div>
            <div class="row">
            <div class="form-group col-lg-8">
                <label>Image:</label>
                <input type="file"  name="file" class="file-input" data-show-caption="true" data-show-upload="false">
            </div>
            </div>

            <div class="row">
                <div class="form-group col-lg-6">
                    <button class="btn btn-lg btn-primary" type="submit">save</button>
                </div>
            </div>

            </form>
        </div>


    </div>


    <div class="row ">

    </div>

    <br>

    @include("layouts.backend.footer")
</div>


@endsection
