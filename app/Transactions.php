<?php

namespace Uzyma;

use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
    protected $table = 'transactions';

    protected $fillable = ['id', 'userid', 'transaction', 'amount', 'created_at',];

	public $timestamps = false;
}
