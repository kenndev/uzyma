<?php

namespace Uzyma;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    protected $fillable = [];
    protected $table = 'businesses';
}
