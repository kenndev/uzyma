<?php

namespace Uzyma;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $table = 'wallet';

    protected $fillable = ['id', 'userid', 'amount', 'created_at',];

	public $timestamps = false;
}
