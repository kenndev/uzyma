<?php

namespace Uzyma;

use Illuminate\Database\Eloquent\Model;

class Other extends Model
{
    protected $fillable = [];
    protected $table = 'other';
}
