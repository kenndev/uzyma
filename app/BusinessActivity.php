<?php

namespace Uzyma;

use Illuminate\Database\Eloquent\Model;

class BusinessActivity extends Model
{
    protected $fillable = [];
    protected $table = 'business_activity';
}
