<?php

namespace Uzyma;

use Illuminate\Database\Eloquent\Model;

class BusinessType extends Model
{
    protected $fillable = [];
    protected $table = 'business_type';
}
