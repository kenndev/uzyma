<?php

namespace Uzyma;

use Illuminate\Database\Eloquent\Model;

class IncomeTarget extends Model
{
    protected $fillable = [];
    protected $table = 'income_target';
}
