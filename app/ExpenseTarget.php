<?php

namespace Uzyma;

use Illuminate\Database\Eloquent\Model;

class ExpenseTarget extends Model
{
    protected $fillable = [];
    protected $table = 'expense_target';
}
