<?php

namespace Uzyma;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = [];
    protected $table = 'slider';
}
