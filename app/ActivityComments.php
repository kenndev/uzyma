<?php

namespace Uzyma;

use Illuminate\Database\Eloquent\Model;

class ActivityComments extends Model
{
    protected $fillable = [];
    protected $table = 'comments';
}
