<?php

namespace Uzyma;

use Illuminate\Database\Eloquent\Model;

class Manager extends Model
{
    protected $fillable = [];
    protected $table = 'managers';
}
