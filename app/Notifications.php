<?php

namespace Uzyma;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    protected $table = 'notification';

    protected $fillable = ['id', 'title', 'message', 'notifierid', 'notifieeid', 'created_at',];

	public $timestamps = false;
}
