<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function isActiveRoute($route)
{
    return Request::is($route) ? 'class=active' : '';
}

function isActiveRoute2($route)
{
    if (strcmp(Route::currentRouteName(), $route) == 0) {
        return 'class=active';
    }
}

function isActiveRoute3($route)
{
    if (strcmp(Route::currentRouteName(), $route) == 0) {
        return 'active';
    }
}

if (!function_exists('getcong')) {

    function getcong($key)
    {

        $settings = \Uzyma\Settings::first();

        return isset($settings->$key) ? $settings->$key : null;
    }

}

function getAnswers($id)
{
    $answers = \Uzyma\Answers::select('answer')->where('question_id', $id)->get();

    return $answers;
}

function getCorrectAnswer($id)
{
    $correct = \Uzyma\Answers::where('question_id', $id)
        ->where('correct', 1)
        ->first();

    echo $correct->answer;
}
