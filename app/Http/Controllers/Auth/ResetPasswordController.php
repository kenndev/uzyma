<?php

namespace Uzyma\Http\Controllers\Auth;

use Uzyma\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Uzyma\User;
use Lang;
use Illuminate\Support\Facades\Auth;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    //protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

        protected function redirectTo() {
        /**
         * Admin = 221
         * Normal user = 101
         */
        if (Auth::user()->account_type == 221) {
            //Admin
            return '/admin/home';
        } else if (Auth::user()->account_type == 101) {
            //Doctor
            return '/partners/home';
        }else if (Auth::user()->account_type == 333) {
            //Doctor
            return '/business/home';
        }else{
            return '/logout';
        }
    }
}
