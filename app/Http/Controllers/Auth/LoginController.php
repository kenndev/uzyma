<?php

namespace Uzyma\Http\Controllers\Auth;

use Uzyma\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Uzyma\User;
use Lang;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = 'admin/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function credentials(Request $request) {
        return [
            'email' => $request->email,
            'password' => $request->password,
            'verified' => 1,
        ];
    }

    protected function sendFailedLoginResponse(Request $request) {

        if (!User::where('email', $request->email)->first()) {
            return redirect()->back()
                            ->withInput($request->only($this->username(), 'remember'))
                            ->withErrors([
                                $this->username() => Lang::get('auth.email'),
            ]);
        }

        if (!User::where('email', $request->email)->where('verified', 1)->first()) {
            return redirect()->back()
                            ->withInput($request->only($this->username(), 'remember'))
                            ->withErrors([
                                $this->username() => Lang::get('auth.activated'),
            ]);
        }

        if (!User::where('email', $request->email)->where('password', bcrypt($request->password))->first()) {
            return redirect()->back()
                            ->withInput($request->only($this->username(), 'remember'))
                            ->withErrors([
                                'password' => Lang::get('auth.password'),
            ]);
        }
    }

    protected function redirectTo() {
        /**
         * Admin = 221
         * Normal user = 101
         */
        if (Auth::user()->account_type == 221) {
            //Admin
            return '/admin/home';
        } else if (Auth::user()->account_type == 101) {
            //Doctor
            return '/partners/home';
        }else if (Auth::user()->account_type == 333) {
            //Doctor
            return '/business/home';
        }else{
            return '/logout';
        }
    }

}
