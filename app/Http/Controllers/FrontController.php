<?php

namespace Uzyma\Http\Controllers;

use Illuminate\Http\Request;

class FrontController extends Controller {

    public function home() {

        return view('layouts.frontend.front');
    }

    public function subscribe(Request $request) {
        $inputs = $request->all();
        $rule = array(
            'email' => 'required',
        );

        $validator = \Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withErrors($validator->messages());
        }
        $subcheck = \Uzyma\Subscription::where('email', $request->input('email'))->first();
        if (count($subcheck)) {
            $rescheck = "<p style=\"color:red\">
                You have already subsribed. Thank you for subscribing.
            </p>";
            return \Response::json($rescheck);
        } else {

            if (!empty($request->input('inputId'))) {
                $sub = \Uzyma\Subscription::findOrFail($request->input('inputId'));
            } else {
                $sub = new \Uzyma\Subscription;
            }

            $sub->email = $request->input('email');
            $sub->save();
            if (!empty($request->input('inputId'))) {
                $res = "<p style=\"color:red\">
                Thank you for subscribing.
            </p>";

                return \Response::json($res);
            } else {
                $res = "<p style=\"color:red\">
                Thank you for subscribing.
            </p>";

                return \Response::json($res);
            }
        }
    }

    public function sendMail(Request $request) {
        $data = array('name' => $request->input('name'), 'email' => $request->input('email'), 'message_user' => $request->input('message'));

        \Mail::send('emails.frontendmail', $data, function( $message ) use ($data) {
            $message->to($data['email'], getcong('site_name'))->subject(getcong('site_name') . ' Contact')->from(getcong('site_email'));
        });

        $res = "<p style=\"color:red\">
                We have received your message. We will get back to you shortly.
            </p>";

        return \Response::json($res);
    }
    
    public function businessRegister(){
        $title = "Business Register";
        return view('auth.register_business',compact('title'));
    }

}
