<?php

namespace Uzyma\Http\Controllers;


use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Requests;
use Uzyma\MobilePayments;
use Uzyma\Notifications;
use Uzyma\Transactions;
use Uzyma\Wallet;
use Uzyma\AppUser;
use Carbon\Carbon;

class PaymentController extends Controller
{
    protected  $key;
    protected  $secret;
    protected  $token;
    protected  $password;
    protected  $passkey;
    protected  $merchant_id;
    protected  $datetime;

    public function __construct()
    {
        $TIMESTAMP=new \DateTime();
        $datetime=$TIMESTAMP->format('YmdHis');
        
        $this->key    =   'DCB391BGltijFKblofAkK0dWhUu1mVSG';
        $this->secret =   'e8egZgftSZ0yqdfb';
        $this->token  =   $this->mpesaOAUTH();
        $this->passkey = '9360c025c16ea6765d4452bdf236a19bc3249d6da5ab74d6a894023b9911546e';
        $this->merchant_id = '875777';
        $this->datetime =$datetime;

    }

    // public function __construct()
    // {
    //     $TIMESTAMP=new \DateTime();
    //     $datetime=$TIMESTAMP->format('YmdHis');
        
    //     $this->key    =   'Mi8SENHWTe2f6qRClrnVODnP5m62Tw6O';
    //     $this->secret =   'w5FA3KCzxRWMmGn1';
    //     $this->token  =   $this->mpesaOAUTH();
    //     $this->passkey = 'b31f7f4a8e8c3566c5442c3d0f1f5ceca1b5de8a8e35fa765aaab7f9b54eb007';
    //     $this->merchant_id = '174379';
    //     $this->datetime =$datetime;

    // }

    public function mpesaOAUTH()
    {
    	$url = 'https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

        $credentials = base64_encode('DCB391BGltijFKblofAkK0dWhUu1mVSG:e8egZgftSZ0yqdfb');
        $post_string='';

        $headers = array(
            "Authorization: Basic ".$credentials,
        );

        $res=$this->tokenRequest($url,$post_string,$headers,"GET");
        $result = json_decode($res,true);
        $token_id=$result['access_token'];
        return $token_id;
    }

    function tokenRequest($url,$post_string,$headers, $type)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //curl_setopt($ch, CURLOPT_HEADER, true);

        if($type=="POST")
        {
            curl_setopt($ch, CURLOPT_POST,TRUE);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS,  $post_string);
        }

        $data = curl_exec($ch);
        if($data === FALSE)
        {
            $err = 'Curl error: ' . curl_error($ch);
            curl_close($ch);
            echo "Error \n".$err;
        }
        else
        {
            curl_close($ch);
            $body = $data;

        }

        return  $body;
    }

    public function stkpush(Request $request)
    {
    	$PhoneNumber = Input::get("phonenumber");
    	$amount = Input::get("amount");

        $password = base64_encode($this->merchant_id.$this->passkey.$this->datetime);

        $url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$this->token)); //setting custom header


        $curl_post_data = array(
            //Fill in the request parameters with valid values
            'BusinessShortCode' => $this->merchant_id,
            'Password' => $password,
            'Timestamp' => $this->datetime,
            'TransactionType' => 'CustomerPayBillOnline',
            'Amount' => $amount,
            'PartyA' => $PhoneNumber,
            'PartyB' => $this->merchant_id,
            'PhoneNumber' => $PhoneNumber,
            'CallBackURL' => 'https://www.uzyma.com/api/uz/callback',
            'AccountReference' => $this->merchant_id,
            'TransactionDesc' => 'Pitch Ke E-Wallet payment.'
        );

        $data_string = json_encode($curl_post_data);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        $curl_response = curl_exec($curl);

        $response['error'] = false;
        $response['message'] = response()->json($curl_response);
        echo json_encode($response);
    }

    public function callback(Request $request)
    {
        $data = $request->all();
        $data = (object)($data);

        //Log Payments
        $req_dump = print_r($data, TRUE);
        //log file
        $filePath = "messages.log";
        //open text file for logging messages by appending
        $file = fopen($filePath,"a");
        //log incoming request
        fwrite($file, $req_dump);
        fwrite($file,"\r\n");

        fclose($file);

        $data =json_encode($data->Body['stkCallback'],true);
        $data = (object)json_decode($data, true);

        $resultcode = $data->ResultCode;

        if($resultcode == 0)
        {

	        $transactionid = $data->CheckoutRequestID;
	        $resultdesc = $data->ResultDesc;

            $metadata = $data->CallbackMetadata;
            $items = $metadata['Item'];

            $result = [];
            foreach ($items as $item) {
                //dd($item['Value']);
                if (array_key_exists("Value", $item)) {
                    $result[$item['Name']] = $item['Value'];
                }
            }

            $transactionid = $data->CheckoutRequestID;
	        $transactionPhoneNumber = $result['PhoneNumber'];
	        $transactionAmount = $result['Amount'];
	        $transactionMpesaId = $result['MpesaReceiptNumber'];
	        $transactionDate = $result['TransactionDate'];
	        $transactionStatus = "Success";

	        $mpexists = MobilePayments::where('transactionMpesaID', $transactionMpesaId)->count();
	        if($mpexists == 0){
	            $mp = new MobilePayments;
		        $mp->transactionID = $transactionid;
		        $mp->transactionPhoneNumber = $transactionPhoneNumber;
		        $mp->transactionAmount = $transactionAmount;
		        $mp->transactionDate = $transactionDate;
		        $mp->transactionMpesaID = $transactionMpesaId;
		        $mp->transactionStatus = $transactionStatus;
		        $mp->created_at = Carbon::today()->format('Y-m-d');
		        $mp->save();

		        $transaction_phone = str_replace("254", "0", $transactionPhoneNumber);

		        $user = AppUser::where('contact', 'like', '%'.$transaction_phone.'%')->first();
		        $usercount = AppUser::where('contact', 'like', '%'.$transaction_phone.'%')->count();

		        if($usercount > 0){
		            $wallet = Wallet::where('userid', $user->id)->first();
		            $amnt = $wallet->amount + $transactionAmount;

		            $walletup = Wallet::where('userid', $user->id)->update(['amount' => $amnt]);

                    $trans = new Transactions;
                    $trans ->userid = $user->id;
                    $trans ->transaction = "Acc Deposit.";
                    $trans ->amount = $transactionAmount;
                    $trans ->created_at = Carbon::today()->format('Y-m-d');
                    $trans ->save();
		        }
	        }

        }

    }

    public function c2b_register_url()
    {
        $url = 'https://api.safaricom.co.ke/mpesa/c2b/v1/registerurl';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$this->token)); //setting custom header


        $curl_post_data = array(
            //Fill in the request parameters with valid values
            'ShortCode' => '875777',
            'ResponseType' => 'Completed',//Completed|Cancelled
            'ConfirmationURL' => 'https://www.uzyma.com/api/uz/confirm',
            'ValidationURL' => 'https://www.uzyma.com/api/uz/validate'
        );

        $data_string = json_encode($curl_post_data);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        $curl_response = curl_exec($curl);

        echo $curl_response;
    }

    public function testAppUser()
    {
        $transaction_account = "0718933367";

        $user = AppUser::where('contact', 'like', '%'.$transaction_account.'%')->first();

        $response["userid"] = $user->id;

        echo json_encode($response);
    }

    public function c2b_confirm_url(Request $request)
    {
        try
        {
            //Set the response content type to application/json
            header("Content-Type:application/json");
            $resp = '{"ResultCode":0,"ResultDesc":"Confirmation recieved successfully"}';
            //read incoming request
            $postData = file_get_contents('php://input');
            //log file
            $filePath = "messages.log";
            //error log
            $errorLog = "errors.log";
            //Parse payload to json
            $jdata = json_decode($postData,true);
            //open text file for logging messages by appending
            $file = fopen($filePath,"a");
            $transactionId = $jdata["TransID"];
            //log response and close file
            fwrite($file,$postData);
            fwrite($file,"\r\n");
            //log response and close file
            fwrite($file,$resp);
            fwrite($file,"\r\n");
            fclose($file);

            $transactionid = $jdata["TransID"];
            $transactionAccount = $jdata["BillRefNumber"];
            $transactionPhoneNumber = $jdata["MSISDN"];
            $transactionAmount = $jdata["TransAmount"];
            $transactionMpesaId = $jdata["TransID"];
            $transactionDate = $jdata["TransTime"];
            $transactionStatus = "Success";

            $mpexists = MobilePayments::where('transactionMpesaID', $transactionMpesaId)->count();
            if($mpexists == 0){

                $transaction_account = str_replace("2547", "07", $transactionAccount);
                $transaction_phone = str_replace("2547", "07", $transactionPhoneNumber);

                $mp = new MobilePayments;
                $mp->transactionID = $transaction_account;
                $mp->transactionPhoneNumber = $transaction_phone;
                $mp->transactionAmount = $transactionAmount;
                $mp->transactionDate = $transactionDate;
                $mp->transactionMpesaID = $transactionMpesaId;
                $mp->transactionStatus = $transactionStatus;
                $mp->created_at = Carbon::today()->format('Y-m-d');
                $mp->save();

                if($transaction_account == $transaction_phone){
                    $user = AppUser::where('contact', 'like', '%'.$transaction_account.'%')->first();
                    $usercount = AppUser::where('contact', 'like', '%'.$transaction_account.'%')->count();

                    if($usercount > 0){
                        $wallet = Wallet::where('userid', $user->id)->first();
                        $amnt = $wallet->amount + $transactionAmount;

                        $walletup = Wallet::where('userid', $user->id)->update(['amount' => $amnt]);

                        $trans = new Transactions;
                        $trans ->userid = $user->id;
                        $trans ->transaction = "Acc Deposit.";
                        $trans ->amount = $transactionAmount;
                        $trans ->created_at = Carbon::today()->format('Y-m-d');
                        $trans ->save();
                    }

                    $notification = new Notifications;
                    $notification ->title = "Account Deposit!";
                    $notification ->message = $transactionAmount . " KES was successfully deposited in your wallet.";
                    $notification ->notifierid = "0";
                    $notification ->notifieeid = $user->id;
                    $notification ->created_at = Carbon::today()->format('Y-m-d');
                    $notification ->save();

                }else{
                    $user = AppUser::where('contact', 'like', '%'.$transaction_account.'%')->first();
                    $usercount = AppUser::where('contact', 'like', '%'.$transaction_account.'%')->count();

                    if($usercount > 0){
                        $wallet = Wallet::where('userid', $user->id)->first();
                        $amnt = $wallet->amount + $transactionAmount;

                        $walletup = Wallet::where('userid', $user->id)->update(['amount' => $amnt]);

                        $trans = new Transactions;
                        $trans ->userid = $user->id;
                        $trans ->transaction = "Acc Deposit by " . $transaction_phone;
                        $trans ->amount = $transactionAmount;
                        $trans ->created_at = Carbon::today()->format('Y-m-d');
                        $trans ->save();
                    }

                    $notification = new Notifications;
                    $notification ->title = "Account Deposit!";
                    $notification ->message = $transactionAmount . " KES was successfully deposited in your wallet.";
                    $notification ->notifierid = "0";
                    $notification ->notifieeid = $user->id;
                    $notification ->eventid = "0";
                    $notification ->created_at = Carbon::today()->format('Y-m-d');
                    $notification ->save();
                }
            }
        } catch (\Exception $ex){
            //append exception to errorLog
            $logErr = fopen($errorLog,"a");
            fwrite($logErr, $ex->getMessage());
            fwrite($logErr,"\r\n");
            fclose($logErr);
        }
        echo $resp;
        // return $resp;
    }

    public function c2b_validate_url(Request $request)
    {

        try
        {
            //Set the response content type to application/json
            header("Content-Type:application/json");
            $resp = '{"ResultCode":0,"ResultDesc":"Validation passed successfully"}';
            //read incoming request
            $postData = file_get_contents('php://input');
            $filePath = "messages.log";
            //error log
            $errorLog = "errors.log";
            //Parse payload to json
            $jdata = json_decode($postData,true);
            //open text file for logging messages by appending
            $file = fopen($filePath,"a");
            //log incoming request
            fwrite($file, $postData);
            fwrite($file,"\r\n");
        } catch (\Exception $ex){
            //append exception to file
            $logErr = fopen($errorLog,"a");
            fwrite($logErr, $ex->getMessage());
            fwrite($logErr,"\r\n");
            fclose($logErr);
            //set failure response
            $resp = '{"ResultCode": 1, "ResultDesc":"Validation failure due to internal service error"}';
        }
        //log response and close file
        fwrite($file,$resp);
        fclose($file);
        echo $resp;
       // return $resp;
    }

    public function c2b()
    {
        $url = 'https://api.safaricom.co.ke/mpesa/c2b/v1/simulate';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$this->token)); //setting custom header


        $curl_post_data = array(
            'ShortCode' => '875777',
            'CommandID' => 'CustomerPayBillOnline',
            'Amount' => '20',
            'Msisdn' => '254718933367',
            'BillRefNumber' => '00010'
        );

        $data_string = json_encode($curl_post_data);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        $curl_response = curl_exec($curl);
        print_r($curl_response);

        echo $curl_response;
    }

    function submitRequest($url,$post_string,$headers, $type)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //curl_setopt($ch, CURLOPT_HEADER, true);

        if($type=="POST")
        {
            curl_setopt($ch, CURLOPT_POST,TRUE);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS,  $post_string);
        }

        $data = curl_exec($ch);
        if($data === FALSE)
        {
            $err = 'Curl error: ' . curl_error($ch);
            curl_close($ch);
            echo "Error \n".$err;
        }
        else
        {
            curl_close($ch);
            $body = $data;

        }

        return  $body;
    }
}
