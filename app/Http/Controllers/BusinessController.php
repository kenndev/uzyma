<?php

namespace Uzyma\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Uzyma\User;
use Uzyma\Mail\EmailVerification;
use Uzyma\Mail\EmailVerification2;
use Mail;
use Illuminate\Foundation\Auth\RegistersUsers;
use DateTime;
use DatePeriod;
use DateInterval;

class BusinessController extends Controller
{
    //
    /**
     * Partners
     * referals == 1 walkin clients == 0
     */
    public function home()
    {


        $nowdate = \Carbon\Carbon::now('Africa/Nairobi')->format('Y-m-d');

        $today_customers = \Uzyma\BusinessActivity::where('activity_date', $nowdate)->count();
        $today_amount = \Uzyma\BusinessActivity::where('activity_date', $nowdate)->sum('cost');
        $total_stylist = \Uzyma\Stylist::count();
        $today_expenses = \Uzyma\Expenses::where('date', $nowdate)->sum('amount');


        $title = "Home";
        \Session::flash('filter_message', 'Graph for date ' . $nowdate);
        return View('business.home', compact('today_expenses', 'total_stylist', 'today_amount', 'today_customers', 'title'));
    }


    public function presidentsGraph()
    {
        $title = "Activity";
        $activity = \Uzyma\BusinessActivity::get();
//        $polls = Poll::where('post_id', 1)->get();
        foreach ($activity as $row)
            $presidents = Poll::where('activity_date', $row->activity_date)->count();

        $stocksTable = Lava::DataTable();  // Lava::DataTable() if using Laravel

        $stocksTable->addDateColumn('Day of Month')
            ->addNumberColumn('Projected')
            ->addNumberColumn('Official');

// Random Data For Example
        for ($a = 1; $a < 30; $a++) {
            $stocksTable->addRow([
                '2017-10-' . $a, rand(800, 1000), rand(800, 1000)
            ]);
        }

        return View::make('backend.home', compact('title'));


    }


    protected function validator(array $data)
    {
        return \Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'phone_number' => 'required',
        ]);
    }

    protected function validator2(array $data)
    {
        return \Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'phone_number' => 'required|min:9|max:9',
            'location' => 'required',
        ]);
    }

    /** Create a new partner */
    protected function createPartner(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'account_type' => '101',
            'password' => bcrypt($data['password']),
            'email_token' => str_random(10),
        ]);
    }

    /** Create a new manager */
    protected function managerCreate(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'account_type' => '444',
            'password' => bcrypt($data['password']),
            'email_token' => str_random(10),
        ]);
    }

    public function partnersAdd(Request $request)
    {
        // Laravel validation
        $validator = $this->validator2($request->all());
        if ($validator->fails()) {
            $this->throwValidationException($request, $validator);
        }
        // Using database transactions is useful here because stuff happening is actually a transaction
        // I don't know what I said in the last line! Weird!
        $country = $request->input('country');
        $phone = $request->input('phone_number');
        $finalphone = $country.$phone;

        DB::beginTransaction();

        try {
            $user = $this->createPartner($request->all());
            DB::commit();
            $partner = new \Uzyma\Business;
            $partner->partner_name = $request->input('name');
            $partner->phone_number = $finalphone;
            $partner->slug = str_slug('name', '-');
            $partner->business_name = $request->input('business_name');
            $partner->location = $request->input('location');
            $partner->user_id = $user->id;

            if ($request->hasFile('file')) {
                $port = $request->file('file');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;
                $path = 'packages/uploads/partners';
                $request->file('file')->move($path, $filename);
                $partner->portofolio = url('/') . '/' . $path . '/' . $filename;
            }
            $partner->save();

            // After creating the user send an email with the random token generated in the create method above
            $email = new EmailVerification(new User(['email_token' => $user->email_token]));
            Mail::to($user->email)->send($email);
            \Session::flash('flash_message', 'An email has been sent to ' . $user->email . ' . Please advise the partner to login to their email to activate their account');
            return back();
        } catch (Exception $e) {
            DB::rollback();
            return back();
        }
    }

    public function verify($token)
    {
        // The verified method has been added to the user model and chained here
        // for better readability
        User::where('email_token', $token)->firstOrFail()->verified();
        return redirect('login');
    }

    public function partnersTable()
    {
        $partners = \Uzyma\Partners::join('users', 'partners.user_id', '=', 'users.id')
            ->select('partners.id', 'partners.partner_name', 'partners.phone_number', 'partners.status', 'partners.image_url', 'users.email');
        return \Datatables::of($partners)
            ->addColumn('action', function ($partners) {
                if ($partners->status == 0) {
                    $activateDeactivate = '<li><a href="' . route('partners.activate', ['id' => $partners->id]) . '"><i class="glyphicon glyphicon-remove"></i> activate</a></li>';
                } else {
                    $activateDeactivate = '<li><a href="' . route('partners.deactivate', ['id' => $partners->id]) . '"><i class="glyphicon glyphicon-lock"></i> deactivate</a></li>';
                }

                $endDropdown = '<ul class="icons-list">
                                                <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                                <i class="icon-menu9"></i>
                                                        </a>

                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                                ' . $activateDeactivate . '
                                                                <li><a onclick="return checkDelete();" href="' . route('partners.delete', ['id' => $partners->id]) . '"><i class="icon-trash"></i> Delete</a></li>
                                                        </ul>
                                                </li>
                                            </ul>';


                return $endDropdown;
            })
            ->make(true);
    }

    public function partnersActivate($id)
    {
        $partners = \Uzyma\Partners::findorFail($id);
        $partners->status = 1;
        $partners->save();
        \Session::flash('flash_message', 'Partner activated successfully');
        return back();
    }

    public function partnersDeactivate($id)
    {
        $partners = \Uzyma\Partners::findorFail($id);
        $partners->status = 0;
        $partners->save();
        \Session::flash('flash_message', 'Partner deactivated successfully');
        return back();
    }

    public function partnersDelete($id)
    {
        $partners = \Uzyma\Partners::finsorFail($id);
        $partners->delete();
        \Session::flash('flash_message', 'Partner deleted successfully');
        return back();
    }

    /**Stylist **/
    public function stylist()
    {
        $title = "Stylist";
        return view('business.stylist', compact('title'));
    }

    public function stylistAddEdit(Request $request)
    {

        $inputs = $request->all();
        $rule = array(
            'name' => 'required',
            'phone_number' => 'required',
            'national_id' => 'required',
        );

        $validator = \Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $stylist = \Uzyma\Stylist::findOrFail($request->input('inputId'));
            if ($request->hasFile('imageFile')) {
                \File::delete(public_path() . '/packages/uploads/stylists/' . $stylist->image_url);
                $port = $request->file('imageFile');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;

                $path = 'packages/uploads/stylists';

                $request->file('imageFile')->move($path, $filename);

                $stylist->image_url = url('/') . '/' . $path . '/' . $filename;
            }
        } else {
            $stylist = new \Uzyma\Stylist;
            if ($request->hasFile('imageFile')) {
                \File::delete(public_path() . '/packages/uploads/stylists/' . $stylist->image_url);
                $port = $request->file('imageFile');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;

                $path = 'packages/uploads/stylists';

                $request->file('imageFile')->move($path, $filename);

                $stylist->image_url = url('/') . '/' . $path . '/' . $filename;
            }
        }
        $business_id = \Uzyma\Business::where('user_id', \Auth::user()->id)->first();

        $stylist->name = $request->input('name');
        $stylist->phone_number = $request->input('phone_number');
        $stylist->national_id = $request->input('national_id');
        $stylist->kra_pin = $request->input('kra_pin');
        $stylist->nhif = $request->input('nhif');
        $stylist->nssf = $request->input('nssf');
        $stylist->area_of_expertise = $request->input('area_expertise');
        $stylist->business_id = $business_id->id;
        $stylist->save();
        if (!empty($request->input('inputId'))) {
            \Session::flash('flash_message', 'Changes Saved');

            return back();
        } else {
            \Session::flash('flash_message', 'stylist saved successfully');

            return back();
        }
    }

    public function stylistDetail($id)
    {
        $stylist = \Uzyma\Stylist::findorFail($id);
        return \Response::json($stylist);
    }

    public function my(){
        var_dump("Here");
    }

    public function stylistTable()
    {
        $business_id = \Uzyma\Business::where('user_id', \Auth::user()->id)->first();

        $stylist = \Uzyma\Stylist::where('is_delete',0)->where('business_id', $business_id->id)->select('id', 'name', 'phone_number', 'national_id', 'area_of_expertise', 'image_url');
        return \Datatables::of($stylist)
            ->addColumn('action', function ($stylist) {
                $edit = '<a class="btn btn-xs btn-success" href="javascript: ajaxmodaledit(' . $stylist->id . ')"><i class="glyphicon glyphicon-pencil"></i> edit</a>';
                $certs = ' <a class="btn btn-xs btn-primary" href="' . route('certificates', ['id' => $stylist->id]) . '"><i class="icon-newspaper"></i> cerificates</a>';
                $delete = ' <a onclick="return confirm(\'Are you sure you want to delete this?\')" class="btn btn-xs btn-warning" href="' . route('stylist.delete', ['id' => $stylist->id]) . '"><i class="glyphicon glyphicon-trash"></i> delete</a>';
                return $edit . $certs . $delete;
            })
            ->make(true);
    }

    public function stylistDelete($id)
    {
        $stylist = \Uzyma\Stylist::findorFail($id);
        $stylist->is_delete = 1;
        $stylist->save();
        \Session::flash('flash_message', 'stylist deleted successfully');
        return back();
    }

    /** Customers **/
    public function customers()
    {
        $title = "Customers";
        return view('business.customers', compact('title'));
    }

    public function customersDetail($id)
    {
        $customers = \Uzyma\Customers::findorFail($id);
        return \Response::json($customers);
    }

    public function customersTable()
    {
        $business_id = \Uzyma\Business::where('user_id', \Auth::user()->id)->first();
        $customers = \Uzyma\Customers::where('business_id', $business_id->id)->select('id', 'name', 'phone', 'email')->groupBy('email');
        return \Datatables::of($customers)
            ->addColumn('action', function ($customers) {
                $edit = '<a class="btn btn-xs btn-success" href="javascript: ajaxmodaledit(' . $customers->id . ')"><i class="glyphicon glyphicon-pencil"></i> edit</a>';
                // $delete = ' <a onclick="return confirm(\'Are you sure you want to delete this?\')" class="btn btn-xs btn-warning" href="' . route('stylist.delete', ['id' => $customers->id]) . '"><i class="glyphicon glyphicon-trash"></i> delete</a>';
                return $edit;
            })
            ->make(true);
    }

    public function customersAddEdit(Request $request)
    {
        $inputs = $request->all();
        $rule = array(
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email|max:255|',
        );

        $validator = \Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $customers = \Uzyma\Customers::findOrFail($request->input('inputId'));
        } else {
            $customers = new \Uzyma\Customers;
        }

        $customers->name = $request->input('name');
        $customers->phone = $request->input('phone');
        $customers->email = $request->input('email');
        $customers->save();
        if (!empty($request->input('inputId'))) {
            \Session::flash('flash_message', 'Changes Saved');

            return back();
        } else {
            \Session::flash('flash_message', 'Customers saved successfully');

            return back();
        }
    }

    /** business activity **/
    public function customersActivity()
    {
        $title = "Customer activity";
        return view('business.customers_activity', compact('title'));
    }

    public function customersActivityTable()
    {
        $business_id = \Uzyma\Business::where('user_id', \Auth::user()->id)->first();
        $nowdate = \Carbon\Carbon::now('Africa/Nairobi')->format('Y-m-d');
        $activity = \Uzyma\BusinessActivity::leftJoin('stylist', 'business_activity.stylist_id', '=', 'stylist.id')
            ->leftJoin('customers', 'business_activity.customer_id', '=', 'customers.id')
            ->orderBy('business_activity.id', 'DESC')
            ->where('business_activity.business_id', $business_id->id)
            ->where('business_activity.activity_date', $nowdate)
            ->select('business_activity.id', 'customers.name as customerName', 'customers.id as customerID', 'customers.phone as customerPhone', 'customers.email', 'stylist.name as stylistName', 'stylist.phone_number as stylistPhone', 'business_activity.activity_date', 'business_activity.cost', 'business_activity.style', 'business_activity.time_taken');

        return \Datatables::of($activity)
            ->addColumn('action', function ($activity) {

                $endDropdown = '<ul class="icons-list">
                                                <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                                <i class="icon-menu9"></i>
                                                        </a>

                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                               <li><a href="javascript: ajaxmodalremarket(' . $activity->customerID . ')"><i class="icon-pencil4"></i> Remarket</a></li>
                                                               
                                                        </ul>
                                                </li>
                                            </ul>';


                return $endDropdown;
            })
            ->make(true);
    }

    public function customersActivityTableSearch(Request $request)
    {
        $search_key = $request->input('date');
        $date_final = explode(' - ', $search_key);
        $date1 = $date_final[0];
        $date2 = $date_final[1];

        $final_date1 = date("Y-m-d", strtotime($date1));
        $final_date2 = date("Y-m-d", strtotime($date2));

        $business_id = \Uzyma\Business::where('user_id', \Auth::user()->id)->first();

        $activity = \Uzyma\BusinessActivity::leftJoin('stylist', 'business_activity.stylist_id', '=', 'stylist.id')
            ->leftJoin('customers', 'business_activity.customer_id', '=', 'customers.id')
            ->where('business_activity.business_id', $business_id->id)
            ->whereBetween('business_activity.activity_date', array($final_date1, $final_date2))
            ->orderBy('business_activity.id', 'DESC')
            ->select('business_activity.id', 'customers.name as customerName', 'customers.id as customerID', 'customers.phone as customerPhone', 'customers.email', 'stylist.name as stylistName', 'stylist.phone_number as stylistPhone', 'business_activity.activity_date', 'business_activity.cost', 'business_activity.style', 'business_activity.time_taken');

        return \Datatables::of($activity)
            ->addColumn('action', function ($activity) {

                $endDropdown = '<ul class="icons-list">
                                                <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                                <i class="icon-menu9"></i>
                                                        </a>

                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                               <li><a href="javascript: ajaxmodalremarket(' . $activity->customerID . ')"><i class="icon-pencil4"></i> Remarket</a></li>
                                                               
                                                        </ul>
                                                </li>
                                            </ul>';


                return $endDropdown;
            })
            ->make(true);
    }

    public function remarket(Request $request)
    {
        $inputs = $request->all();
        $rule = array(
            'remarket' => 'required',
            'subject' => 'required',
        );

        $validator = \Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withErrors($validator->messages());
        }

        $customer = \Uzyma\Customers::findorFail($request->input('clientId'));
        $data = array('name' => $customer->name, 'email' => $customer->email, 'subject' => $request->input('subject'), 'message_user' => $request->input('remarket'));

        \Mail::send('emails.remarket', $data, function ($message) use ($data) {
            $message->to($data['email'])->subject($data['subject'])->from(getcong('site_email'));
        });

        \Session::flash('flash_message', 'Remarket message sent successfully');

        return back();

    }

    /** Managers */

    public function manager()
    {
        $title = "Customer activity";
        return view('business.manage_managers', compact('title'));
    }


    public function managerAdd(Request $request)
    {
        // Laravel validation
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $this->throwValidationException($request, $validator);
        }
        // Using database transactions is useful here because stuff happening is actually a transaction
        // I don't know what I said in the last line! Weird!
        DB::beginTransaction();
        $businesses = \Uzyma\Business::where('user_id', \Auth::user()->id)->first();
        try {
            $user = $this->managerCreate($request->all());
            DB::commit();
            $manager = new \Uzyma\Manager;
            $manager->name = $request->input('name');
            $manager->phone_number = $request->input('phone_number');
            $manager->email = $request->input('email');
            $manager->business_id = $businesses->id;
            $manager->users_id = $user->id;

            if ($request->hasFile('file')) {
                $port = $request->file('file');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;
                $path = 'packages/uploads/managers';
                $request->file('file')->move($path, $filename);
                $manager->image_url = url('/') . '/' . $path . '/' . $filename;
            }
            $manager->save();

            $password = $request->input('password');
            // After creating the user send an email with the random token generated in the create method above
            $data = array('name' => $request->input('name'), 'password' => $request->input('password'), 'email' => $request->input('email'), 'email_token' => $user->email_token);

        \Mail::send('emails.managers', $data, function( $message ) use ($data) {
            $message->to($data['email'], getcong('site_name'))->subject('Verification')->from(getcong('site_email'));
        });

            // $email = new EmailVerification2(new User(['email_token' => $user->email_token]), $password);
            // Mail::to($user->email)->send($email);
            \Session::flash('flash_message', 'An email has been sent to ' . $user->email . ' . Please advise the manager to login to their email to activate their account');
            return back();
        } catch (Exception $e) {
            DB::rollback();
            return back();
        }
    }

    public function verify2($token)
    {
        // The verified method has been added to the user model and chained here
        // for better readability
        User::where('email_token', $token)->firstOrFail()->verified();
        return redirect('login');
    }

    public function managerTable()
    {
        $businesses = \Uzyma\Business::where('user_id', \Auth::user()->id)->first();
        $activity = \Uzyma\Manager::leftJoin('businesses', 'managers.business_id', '=', 'businesses.id')
            ->where('business_id', $businesses->id)
            ->select('managers.id', 'managers.name', 'managers.phone_number', 'managers.email', 'managers.image_url as managersImage');
        return \Datatables::of($activity)
            ->addColumn('action', function ($activity) {
                $endDropdown = '<ul class="icons-list">
                                                <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                                <i class="icon-menu9"></i>
                                                        </a>

                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                                
                                                                <li><a onclick="return checkDelete();" href="' . route('manager.delete', ['id' => $activity->id]) . '"><i class="icon-trash"></i> Delete</a></li>
                                                        </ul>
                                                </li>
                                            </ul>';


                return $endDropdown;
            })
            ->make(true);
    }

    public function managerDelete($id)
    {
        $manager = \Uzyma\Manager::findorFail($id);
        $userdel = \Uzyma\User::destroy($manager->users_id);
        $manager->delete();
        \Session::flash('flash_message', 'Manager deleted successfully');
        return back();
    }

    public function managerDetail($id)
    {
        $manager = \Uzyma\Manager::firstOrFail($id);
        return \Response::json($manager);
    }

    /**
     * Expenses functions
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     */

    public function expenses()
    {
        $title = "Expenses";
        $expense_type = \Uzyma\ExpenseTypes::get();
        //var_dump($expense_type);
        return view('business.manage_expenses', compact('title', 'expense_type'));
    }

    public function expensesTable()
    {
        $businesses = \Uzyma\Business::where('user_id', \Auth::user()->id)->first();
        $activity = \Uzyma\Expenses::join('expense_type', 'expenses.expense_type_id', '=', 'expense_type.id')
            ->select('expense_type.expense_type', 'amount', 'expenses.expense_title', 'expenses.date', 'expenses.id as expenseID')
            ->where('expenses.business_id', $businesses->id);
        return \Datatables::of($activity)
            ->addColumn('action', function ($activity) {
                $endDropdown = '<ul class="icons-list">
                                                <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                                <i class="icon-menu9"></i>
                                                        </a>

                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="javascript: ajaxmodaledit(' . $activity->expenseID . ')"><i class="icon-pencil4"></i> Edit</a></li>
                                                                <li><a onclick="return checkDelete();" href="' . route('expenses.delete', ['id' => $activity->expenseID]) . '"><i class="icon-trash"></i> Delete</a></li>
                                                        </ul>
                                                </li>
                                            </ul>';


                return $endDropdown;
            })
            ->make(true);
    }

    public function expensesAdd(Request $request)
    {
        $inputs = $request->all();
        $rule = array(
            'title' => 'required',
            'date' => 'required',
            'amount' => 'required',

        );

        $validator = \Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $expenses = \Uzyma\Expenses::findOrFail($request->input('inputId'));
        } else {
            $expenses = new \Uzyma\Expenses;
        }
        if ($request->input('expense_type') == 1) {
            $expenses->brand = $request->input('brand');
        }
        $expenses->expense_type_id = $request->input('expense_type');
        $businesses = \Uzyma\Business::where('user_id', \Auth::user()->id)->first();
        $expenses->expense_title = $request->input('title');
        $expenses->date = $request->input('date');
        $expenses->business_id = $businesses->id;
        $expenses->amount = $request->input('amount');
        $expenses->description = $request->input('description');
        $expenses->save();
        if (!empty($request->input('inputId'))) {
            \Session::flash('flash_message', 'Changes Saved');

            return back();
        } else {
            \Session::flash('flash_message', 'Customers saved successfully');

            return back();
        }
    }

    public function expensesDetail($id)
    {
        $expenses = \Uzyma\Expenses::findorFail($id);
        return \Response::json($expenses);
    }

    public function expensesDelete($id)
    {
        \Uzyma\Expenses::destroy($id);
        \Session::flash('flash_message', 'Expenses was deleted successfully');
        return back();
    }


    /**
     *
     * Expense Target Funtions
     */
    public function target()
    {
        $title = 'Expenses traget';
        return view('business.manage_expense_target', compact('title'));
    }

    public function targetAdd(Request $request)
    {
        $inputs = $request->all();
        $rule = array(
            'date' => 'required',
            'amount' => 'required',

        );

        $validator = \Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $expenses = \Uzyma\ExpenseTarget::findOrFail($request->input('inputId'));
        } else {
            $expenses = new \Uzyma\ExpenseTarget;
        }
        $businesses = \Uzyma\Business::where('user_id', \Auth::user()->id)->first();
        $expenses->date = $request->input('date');
        $expenses->business_id = $businesses->id;
        $expenses->amount = $request->input('amount');
        $expenses->save();
        if (!empty($request->input('inputId'))) {
            \Session::flash('flash_message', 'Changes Saved');

            return back();
        } else {
            \Session::flash('flash_message', 'Customers saved successfully');

            return back();
        }
    }

    public function targetTable()
    {
        $businesses = \Uzyma\Business::where('user_id', \Auth::user()->id)->first();
        $activity = \Uzyma\ExpenseTarget::select('amount', 'date', 'id as expenseID')
            ->where('business_id', $businesses->id);
        return \Datatables::of($activity)
            ->addColumn('action', function ($activity) {
                $endDropdown = '<ul class="icons-list">
                                                <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                                <i class="icon-menu9"></i>
                                                        </a>

                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="javascript: ajaxmodaledit(' . $activity->expenseID . ')"><i class="icon-pencil4"></i> Edit</a></li>
                                                                <li><a onclick="return checkDelete();" href="' . route('expenses.target.delete', ['id' => $activity->expenseID]) . '"><i class="icon-trash"></i> Delete</a></li>
                                                        </ul>
                                                </li>
                                            </ul>';


                return $endDropdown;
            })
            ->make(true);
    }

    public function targetDetail($id)
    {
        $expenses = \Uzyma\ExpenseTarget::findorFail($id);
        return \Response::json($expenses);
    }

    public function targetDelete($id)
    {
        \Uzyma\ExpenseTarget::destroy($id);
        \Session::flash('flash_message', 'Expenses Target was deleted successfully');
        return back();
    }


    /**
     * Income Target Functions
     *
     */

    public function incomeTarget()
    {
        $title = 'Income traget';
        return view('business.manage_income_target', compact('title'));
    }

    public function incomeTargetAdd(Request $request)
    {
        $inputs = $request->all();
        $rule = array(
            'date' => 'required',
            'amount' => 'required',

        );

        $validator = \Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $income = \Uzyma\IncomeTarget::findOrFail($request->input('inputId'));
        } else {
            $income = new \Uzyma\IncomeTarget;
        }
        $businesses = \Uzyma\Business::where('user_id', \Auth::user()->id)->first();
        $income->date = $request->input('date');
        $income->business_id = $businesses->id;
        $income->amount = $request->input('amount');
        $income->save();
        if (!empty($request->input('inputId'))) {
            \Session::flash('flash_message', 'Changes Saved');

            return back();
        } else {
            \Session::flash('flash_message', 'Customers saved successfully');

            return back();
        }
    }

    public function incomeTargetTable()
    {
        $businesses = \Uzyma\Business::where('user_id', \Auth::user()->id)->first();
        $activity = \Uzyma\IncomeTarget::select('amount', 'date', 'id as incomeID')
            ->where('business_id', $businesses->id);
        return \Datatables::of($activity)
            ->addColumn('action', function ($activity) {
                $endDropdown = '<ul class="icons-list">
                                                <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                                <i class="icon-menu9"></i>
                                                        </a>

                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="javascript: ajaxmodaledit(' . $activity->incomeID . ')"><i class="icon-pencil4"></i> Edit</a></li>
                                                                <li><a onclick="return checkDelete();" href="' . route('income.target.delete', ['id' => $activity->incomeID]) . '"><i class="icon-trash"></i> Delete</a></li>
                                                        </ul>
                                                </li>
                                            </ul>';


                return $endDropdown;
            })
            ->make(true);
    }

    public function incomeTargetDetail($id)
    {
        $expenses = \Uzyma\IncomeTarget::findorFail($id);
        return \Response::json($expenses);
    }

    public function incomeTargetDelete($id)
    {
        \Uzyma\IncomeTarget::destroy($id);
        \Session::flash('flash_message', 'Income Target was deleted successfully');
        return back();
    }

    /**
     *
     * Profit and loss
     */
    public function profitLoss()
    {
        $startDate = date('Y-m-01');
        $endDate = date('Y-m-t');

        $begin = new DateTime($startDate);
        $end = new DateTime($endDate);


        $final_date1 = $begin->format('Y-m-d');
        $final_date2 = $end->format('Y-m-d');

        $pchart = array();
        $pchartexpense = array();
        $business_id = \Uzyma\Business::where('user_id', \Auth::user()->id)->first();
        $detailsA = \Uzyma\BusinessActivity::select(\DB::raw('SUM(cost) as count, activity_date as sName'))
            ->where('business_activity.business_id', $business_id->id)
            ->whereBetween('business_activity.activity_date', array($final_date1, $final_date2))
            ->groupBy('activity_date')
            ->get();
        foreach ($detailsA as $row) {
            $finaldate = \Carbon\Carbon::parse($row->sName)->format('Y-m-d');
            $pchart[$finaldate] = (int)$row->count;

        }
        $expense = \Uzyma\Expenses::select(\DB::raw('SUM(amount) as count, date as sDate'))
            ->where('business_id', $business_id->id)
            ->whereBetween('date', array($final_date1, $final_date2))
            ->groupBy('date')
            ->get();
        foreach ($expense as $row2) {
            $finaldate2 = \Carbon\Carbon::parse($row2->sDate)->format('Y-m-d');
            $pchartexpense[$finaldate2] = (int)$row2->count;

        }


        $interval = new DateInterval('P1D'); // 1 Day
        $dateRange = new DatePeriod($begin, $interval, $end);
        $pchartarray = array();
        foreach ($dateRange as $date) {
            $pchartarray[] = $date->format('Y-m-d');
        }
        $pchartarray[] = $end->format('Y-m-d');


        $dayArrayLength = count($pchartarray);

        $pchartprofitloss = array();

        if (count($pchart) == 0 && count($pchartexpense) == 0) {
            for ($k = 0; $k < $dayArrayLength; $k++) {
                $day_from_array = $pchartarray[$k];
                $income1 = 0;
                $expense1 = 0;
                $profit_or_loss = $income1 - $expense1;
                $pchartprofitloss[$day_from_array] = $profit_or_loss;
            }

        } elseif (count($pchart) > 0 && count($pchartexpense) == 0) {
            for ($k = 0; $k < $dayArrayLength; $k++) {
                $day_from_array = $pchartarray[$k];
                $income1 = isset($pchart[$day_from_array]);
                $expense1 = 0;
                $profit_or_loss = $income1 - $expense1;
                $pchartprofitloss[$day_from_array] = $profit_or_loss;
            }
        } elseif (count($pchart) == 0 && count($pchartexpense) > 0) {

            for ($k = 0; $k < $dayArrayLength; $k++) {
                $day_from_array = $pchartarray[$k];
                $income1 = 0;
                $expense1 = isset($pchartexpense[$day_from_array]);


                $profit_or_loss = $income1 - $expense1;
                $pchartprofitloss[$day_from_array] = $profit_or_loss;
            }
        } else {
            for ($k = 0; $k < $dayArrayLength; $k++) {
                $day_from_array = $pchartarray[$k];
                $income1 = isset($pchart[$day_from_array]);
                $expense1 = isset($pchartexpense[$day_from_array]);
                $profit_or_loss = $income1 - $expense1;
                $pchartprofitloss[$day_from_array] = $profit_or_loss;
            }
        }


        $nowdate = \Carbon\Carbon::now('Africa/Nairobi')->format('Y-m');
        $expenseTarget = \Uzyma\ExpenseTarget::where('business_id', $business_id->id)->where('date', $nowdate)->select('amount')->first();
        $incomeTarget = \Uzyma\IncomeTarget::where('business_id', $business_id->id)->where('date', $nowdate)->select('amount')->first();
        $title = "Profit & Loss";
        \Session::flash('filter_message', 'Graph for dates ' . $final_date1 . ' to ' . $final_date2);
        return view('business.profitloss', compact('pchartprofitloss', 'incomeTarget', 'expenseTarget', 'pchartarray', 'title', 'pchart', 'pchartexpense'));
    }

    public function profitLossFilter(Request $request)
    {

        $search_key = $request->input('date');
        $date_final = explode(' - ', $search_key);
        $date1 = $date_final[0];
        $date2 = $date_final[1];

        $begin = new DateTime($date1);
        $end = new DateTime($date2);


        $final_date1 = $begin->format('Y-m-d');
        $final_date2 = $end->format('Y-m-d');

//        $final_date1 = \Carbon\Carbon::parse($date1)->format('Y-m-d');
//        $final_date2 = \Carbon\Carbon::parse($date2)->format('Y-m-d');

        $pchart = array();
        $pchartexpense = array();

        $business_id = \Uzyma\Business::where('user_id', \Auth::user()->id)->first();
        $detailsA = \Uzyma\BusinessActivity::select(\DB::raw('SUM(cost) as count, activity_date as sName'))
            ->where('business_activity.business_id', $business_id->id)
            ->whereBetween('business_activity.activity_date', array($final_date1, $final_date2))
            ->groupBy('activity_date')
            ->get();
        foreach ($detailsA as $row) {
            $finaldate = \Carbon\Carbon::parse($row->sName)->format('Y-m-d');
            $pchart[$finaldate] = (int)$row->count;

        }
        $expense = \Uzyma\Expenses::select(\DB::raw('SUM(amount) as count, date as sDate'))
            ->where('business_id', $business_id->id)
            ->whereBetween('date', array($final_date1, $final_date2))
            ->groupBy('date')
            ->get();
        foreach ($expense as $row2) {
            $finaldate2 = \Carbon\Carbon::parse($row2->sDate)->format('Y-m-d');
            $pchartexpense[$finaldate2] = (int)$row2->count;

        }


        $interval = new DateInterval('P1D'); // 1 Day
        $dateRange = new DatePeriod($begin, $interval, $end);
        $pchartarray = array();
        foreach ($dateRange as $date) {
            $pchartarray[] = $date->format('Y-m-d');
        }
        $pchartarray[] = $end->format('Y-m-d');


        $dayArrayLength = count($pchartarray);

        $pchartprofitloss = array();

        if (count($pchart) == 0 && count($pchartexpense) == 0) {
            for ($k = 0; $k < $dayArrayLength; $k++) {
                $day_from_array = $pchartarray[$k];
                $income1 = 0;
                $expense1 = 0;
                $profit_or_loss = $income1 - $expense1;
                $pchartprofitloss[$day_from_array] = $profit_or_loss;
            }

        } elseif (count($pchart) > 0 && count($pchartexpense) == 0) {
            for ($k = 0; $k < $dayArrayLength; $k++) {
                $day_from_array = $pchartarray[$k];
                $income1 = isset($pchart[$day_from_array]);
                $expense1 = 0;
                $profit_or_loss = $income1 - $expense1;
                $pchartprofitloss[$day_from_array] = $profit_or_loss;
            }
        } elseif (count($pchart) == 0 && count($pchartexpense) > 0) {

            for ($k = 0; $k < $dayArrayLength; $k++) {
                $day_from_array = $pchartarray[$k];
                $income1 = 0;
                $expense1 = isset($pchartexpense[$day_from_array]);


                $profit_or_loss = $income1 - $expense1;
                $pchartprofitloss[$day_from_array] = $profit_or_loss;
            }
        } else {
            for ($k = 0; $k < $dayArrayLength; $k++) {
                $day_from_array = $pchartarray[$k];
                $income1 = isset($pchart[$day_from_array]);
                $expense1 = isset($pchartexpense[$day_from_array]);
                $profit_or_loss = $income1 - $expense1;
                $pchartprofitloss[$day_from_array] = $profit_or_loss;
            }
        }


        $nowdate = \Carbon\Carbon::now('Africa/Nairobi')->format('Y-m');
        $expenseTarget = \Uzyma\ExpenseTarget::where('business_id', $business_id->id)->where('date', $nowdate)->select('amount')->first();
        $incomeTarget = \Uzyma\IncomeTarget::where('business_id', $business_id->id)->where('date', $nowdate)->select('amount')->first();
        $title = "Profit & Loss";
        \Session::flash('filter_message', 'Graph for dates ' . $final_date1 . ' to ' . $final_date2);
        return view('business.profitloss', compact('pchartprofitloss', 'incomeTarget', 'expenseTarget', 'pchartarray', 'title', 'pchart', 'pchartexpense'));
    }


    public function insights()
    {
        $startDate = date('Y-m-01');
        $endDate = date('Y-m-t');

        $begin = new DateTime($startDate);
        $end = new DateTime($endDate);

        $final_date1 = $begin->format('Y-m-d');
        $final_date2 = $end->format('Y-m-d');

        $pchart = array();
        $pchart2 = array();
        $pchart3 = array();
        $pchart4 = array();

        $pchart5 = array();

        $pchart6 = array();
        $pchart7 = array();
        $business_id = \Uzyma\Business::where('user_id', \Auth::user()->id)->first();
        $detailsA = \Uzyma\BusinessActivity::select(\DB::raw('COUNT(activity_date) as count, activity_date as sName'))
            ->where('business_activity.business_id', $business_id->id)
            ->whereBetween('business_activity.activity_date', array($final_date1, $final_date2))
            ->groupBy('activity_date')
            ->get();
        $dates = null;
        foreach ($detailsA as $row) {
            $dates = $finaldate = \Carbon\Carbon::parse($row->sName)->format('Y-m-d');
            $pchart[$finaldate] = (int)$row->count;


            $month = \Carbon\Carbon::parse($finaldate)->month;
            $pchart5[$month] = (int)$row->count;

        }


        $detailsB = \Uzyma\BusinessActivity::join('customer_type', 'business_activity.type_client', '=', 'customer_type.id')
            ->select(\DB::raw('COUNT(type_client) as count, customer_type.type as sName'))
            ->where('business_activity.business_id', $business_id->id)
            ->whereBetween('business_activity.activity_date', array($final_date1, $final_date2))
            ->groupBy('type_client')
            ->get();

        foreach ($detailsB as $row) {
            $pchart2[$row->sName] = (int)$row->count;
        }
        //Referral
        $detailsC = \Uzyma\BusinessActivity::select(\DB::raw('COUNT(activity_date) as count,activity_date as sNameName'))
            ->where('type_client', 1)
            ->where('business_activity.business_id', $business_id->id)
            ->whereBetween('business_activity.activity_date', array($final_date1, $final_date2))
            ->groupBy('activity_date')
            ->get();
        //Walk in
        $detailsD = \Uzyma\BusinessActivity::select(\DB::raw('COUNT(activity_date) as count,activity_date as sNameName2'))
            ->where('type_client', 2)
            ->where('business_activity.business_id', $business_id->id)
            ->whereBetween('business_activity.activity_date', array($final_date1, $final_date2))
            ->groupBy('activity_date')
            ->get();

        //Referral
        $detailsE = \Uzyma\BusinessActivity::select(\DB::raw('COUNT(activity_date) as count,activity_date as sNameName'))
            ->where('type_client', 1)
            ->where('business_activity.business_id', $business_id->id)
            ->whereBetween('business_activity.activity_date', array($final_date1, $final_date2))
            ->groupBy('activity_date')
            ->get();
        //Walk in
        $detailsF = \Uzyma\BusinessActivity::select(\DB::raw('COUNT(activity_date) as count,activity_date as sNameName2'))
            ->where('type_client', 2)
            ->where('business_activity.business_id', $business_id->id)
            ->whereBetween('business_activity.activity_date', array($final_date1, $final_date2))
            ->groupBy('activity_date')
            ->get();

        foreach ($detailsC as $rowC) {
            $finaldateC = \Carbon\Carbon::parse($rowC->sNameName)->format('Y-m-d');
            $pchart3[$finaldateC] = (int)$rowC->count;

            $monthC = \Carbon\Carbon::parse($finaldateC)->month;
            $pchart6[$monthC] = (int)$rowC->count;

        }

        foreach ($detailsD as $rowD) {
            $finaldateD = \Carbon\Carbon::parse($rowD->sNameName2)->format('Y-m-d');
            $pchart4[$finaldateD] = (int)$rowD->count;

            $monthD = \Carbon\Carbon::parse($finaldateD)->month;
            $pchart6[$monthD] = (int)$rowD->count;
        }

        $title = "Insights";
        \Session::flash('filter_message', 'Graph for dates ' . $final_date1 . ' to ' . $final_date2);
        return View('business.insights', compact('pchart', 'title', 'pchart2', 'pchart3', 'pchart5', 'pchart4', 'pchart6', 'pchart7'));
    }

    public function insightsFilter(Request $request)
    {
        $search_key = $request->input('date');
        $date_final = explode(' - ', $search_key);
        $date1 = $date_final[0];
        $date2 = $date_final[1];

        $final_date1 = \Carbon\Carbon::parse($date1)->format('Y-m-d');
        $final_date2 = \Carbon\Carbon::parse($date2)->format('Y-m-d');
        $pchart = array();
        $pchart2 = array();
        $pchart3 = array();
        $pchart4 = array();

        $pchart5 = array();

        $pchart6 = array();
        $pchart7 = array();

        $business_id = \Uzyma\Business::where('user_id', \Auth::user()->id)->first();
        $detailsA = \Uzyma\BusinessActivity::select(\DB::raw('COUNT(activity_date) as count, activity_date as sName'))
            ->where('business_activity.business_id', $business_id->id)
            ->whereBetween('business_activity.activity_date', array($final_date1, $final_date2))
            ->groupBy('activity_date')
            ->get();
        $dates = null;
        foreach ($detailsA as $row) {
            $dates = $finaldate = \Carbon\Carbon::parse($row->sName)->format('Y-m-d');
            $pchart[$finaldate] = (int)$row->count;


            $month = \Carbon\Carbon::parse($finaldate)->month;
            $pchart5[$month] = (int)$row->count;

        }


        $detailsB = \Uzyma\BusinessActivity::join('customer_type', 'business_activity.type_client', '=', 'customer_type.id')
            ->select(\DB::raw('COUNT(type_client) as count, customer_type.type as sName'))
            ->where('business_activity.business_id', $business_id->id)
            ->whereBetween('business_activity.activity_date', array($final_date1, $final_date2))
            ->groupBy('type_client')
            ->get();

        foreach ($detailsB as $row) {
            $pchart2[$row->sName] = (int)$row->count;
        }
        //Referral
        $detailsC = \Uzyma\BusinessActivity::select(\DB::raw('COUNT(activity_date) as count,activity_date as sNameName'))
            ->where('type_client', 1)
            ->where('business_activity.business_id', $business_id->id)
            ->whereBetween('business_activity.activity_date', array($final_date1, $final_date2))
            ->groupBy('activity_date')
            ->get();
        //Walk in
        $detailsD = \Uzyma\BusinessActivity::select(\DB::raw('COUNT(activity_date) as count,activity_date as sNameName2'))
            ->where('type_client', 2)
            ->where('business_activity.business_id', $business_id->id)
            ->whereBetween('business_activity.activity_date', array($final_date1, $final_date2))
            ->groupBy('activity_date')
            ->get();

        //Referral
        $detailsE = \Uzyma\BusinessActivity::select(\DB::raw('COUNT(activity_date) as count,activity_date as sNameName'))
            ->where('type_client', 1)
            ->whereBetween('business_activity.activity_date', array($final_date1, $final_date2))
            ->groupBy('activity_date')
            ->get();
        //Walk in
        $detailsF = \Uzyma\BusinessActivity::select(\DB::raw('COUNT(activity_date) as count,activity_date as sNameName2'))
            ->where('type_client', 2)
            ->where('business_activity.business_id', $business_id->id)
            ->whereBetween('business_activity.activity_date', array($final_date1, $final_date2))
            ->groupBy('activity_date')
            ->get();

        foreach ($detailsC as $rowC) {
            $finaldateC = \Carbon\Carbon::parse($rowC->sNameName)->format('Y-m-d');
            $pchart3[$finaldateC] = (int)$rowC->count;

            $monthC = \Carbon\Carbon::parse($finaldateC)->month;
            $pchart6[$monthC] = (int)$rowC->count;

        }

        foreach ($detailsD as $rowD) {
            $finaldateD = \Carbon\Carbon::parse($rowD->sNameName2)->format('Y-m-d');
            $pchart4[$finaldateD] = (int)$rowD->count;

            $monthD = \Carbon\Carbon::parse($finaldateD)->month;
            $pchart6[$monthD] = (int)$rowD->count;
        }

        $title = "Insights";
        \Session::flash('filter_message', 'Graph for dates ' . $final_date1 . ' to ' . $final_date2);
        return View('business.insights', compact('pchart', 'title', 'pchart2', 'pchart3', 'pchart5', 'pchart4', 'pchart6', 'pchart7'));
    }

    /**
     *
     * Dashboard filter
     */
    public function dashboardFilter(Request $request)
    {
        $inputs = $request->all();
        $rule = array(
            'date' => 'required'

        );

        $validator = \Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withErrors($validator->messages());
        }

        $nowdate = $request->input('date');

        $business_id = \Uzyma\Business::where('user_id', \Auth::user()->id)->first();

        $today_customers = \Uzyma\BusinessActivity::where('business_id', $business_id->id)->where('activity_date', $nowdate)->count();
        $today_amount = \Uzyma\BusinessActivity::where('business_id', $business_id->id)->where('activity_date', $nowdate)->sum('cost');
        $total_stylist = \Uzyma\Stylist::where('business_id', $business_id->id)->count();
        $today_expenses = \Uzyma\Expenses::where('business_id', $business_id->id)->where('date', $nowdate)->sum('amount');

        $title = "Home";
        \Session::flash('filter_message', 'Graph for date ' . $nowdate);
        return View('business.home', compact('today_expenses', 'total_stylist', 'today_amount', 'today_customers', 'title'));
    }

    /**
     * Income graph
     */
    public function graphIncome()
    {
        $startDate = date('Y-m-01');
        $endDate = date('Y-m-t');

        $begin = new DateTime($startDate);
        $end = new DateTime($endDate);

        $final_date1 = $begin->format('Y-m-d');
        $final_date2 = $end->format('Y-m-d');

        $pchart = array();

        $business_id = \Uzyma\Business::where('user_id', \Auth::user()->id)->first();
        $detailsA = \Uzyma\BusinessActivity::select(\DB::raw('SUM(cost) as count, activity_date as sName'))
            ->where('business_activity.business_id', $business_id->id)
            ->whereBetween('business_activity.activity_date', array($final_date1, $final_date2))
            ->groupBy('activity_date')
            ->get();
        foreach ($detailsA as $row) {
            $finaldate = \Carbon\Carbon::parse($row->sName)->format('Y-m-d');
            $pchart[$finaldate] = (int)$row->count;
        }

        $title = 'Graph on income';
        \Session::flash('filter_message', 'Graph for date ' . $final_date1 . ' to ' . $final_date2);
        return view('business.income_graph', compact('title', 'pchart'));
    }

    public function graphIncomeFilter(Request $request)
    {
        $search_key = $request->input('date');
        $date_final = explode(' - ', $search_key);
        $date1 = $date_final[0];
        $date2 = $date_final[1];

        $final_date1 = \Carbon\Carbon::parse($date1)->format('Y-m-d');
        $final_date2 = \Carbon\Carbon::parse($date2)->format('Y-m-d');
        $pchart = array();
        $business_id = \Uzyma\Business::where('user_id', \Auth::user()->id)->first();
        $detailsA = \Uzyma\BusinessActivity::select(\DB::raw('SUM(cost) as count, activity_date as sName'))
            ->where('business_activity.business_id', $business_id->id)
            ->whereBetween('business_activity.activity_date', array($final_date1, $final_date2))
            ->groupBy('activity_date')
            ->get();
        foreach ($detailsA as $row) {
            $finaldate = \Carbon\Carbon::parse($row->sName)->format('Y-m-d');
            $pchart[$finaldate] = (int)$row->count;

        }

        $title = 'Graph on income';
        \Session::flash('filter_message', 'Graph for date ' . $final_date1 . ' to ' . $final_date2);
        return view('business.income_graph', compact('title', 'pchart'));
    }

    /**
     * Expense graph
     */
    public function graphExpense()
    {
        $startDate = date('Y-m-01');
        $endDate = date('Y-m-t');

        $begin = new DateTime($startDate);
        $end = new DateTime($endDate);

        $final_date1 = $begin->format('Y-m-d');
        $final_date2 = $end->format('Y-m-d');

        $pchart = array();
        $business_id = \Uzyma\Business::where('user_id', \Auth::user()->id)->first();
        $detailsA = \Uzyma\Expenses::select(\DB::raw('SUM(amount) as count, date as sName'))
            ->where('business_id', $business_id->id)
            ->whereBetween('date', array($final_date1, $final_date2))
            ->groupBy('date')
            ->get();
        foreach ($detailsA as $row) {
            $finaldate = \Carbon\Carbon::parse($row->sName)->format('Y-m-d');
            $pchart[$finaldate] = (int)$row->count;
        }

        $title = 'Graph on expense';
        \Session::flash('filter_message', 'Graph for date ' . $final_date1 . ' to ' . $final_date2);
        return view('business.expense_graph', compact('title', 'pchart'));
    }

    public function graphExpenseFilter(Request $request)
    {
        $search_key = $request->input('date');
        $date_final = explode(' - ', $search_key);
        $date1 = $date_final[0];
        $date2 = $date_final[1];

        $final_date1 = \Carbon\Carbon::parse($date1)->format('Y-m-d');
        $final_date2 = \Carbon\Carbon::parse($date2)->format('Y-m-d');
        $pchart = array();
        $business_id = \Uzyma\Business::where('user_id', \Auth::user()->id)->first();
        $detailsA = \Uzyma\Expenses::select(\DB::raw('SUM(amount) as count, date as sName'))
            ->where('business_id', $business_id->id)
            ->whereBetween('date', array($final_date1, $final_date2))
            ->groupBy('date')
            ->get();
        foreach ($detailsA as $row) {
            $finaldate = \Carbon\Carbon::parse($row->sName)->format('Y-m-d');
            $pchart[$finaldate] = (int)$row->count;

        }

        $title = 'Graph on Expenses';
        \Session::flash('filter_message', 'Graph for date ' . $final_date1 . ' to ' . $final_date2);
        return view('business.expense_graph', compact('title', 'pchart'));
    }

    /**
     * Customers graph
     */
    public function graphCustomers()
    {
        $startDate = date('Y-m-01');
        $endDate = date('Y-m-t');

        $begin = new DateTime($startDate);
        $end = new DateTime($endDate);

        $final_date1 = $begin->format('Y-m-d');
        $final_date2 = $end->format('Y-m-d');

        $pchart = array();
        $business_id = \Uzyma\Business::where('user_id', \Auth::user()->id)->first();
        $detailsA = \Uzyma\BusinessActivity::select(\DB::raw('COUNT(activity_date) as count, activity_date as sName'))
            ->where('business_activity.business_id', $business_id->id)
            ->whereBetween('activity_date', array($final_date1, $final_date2))
            ->groupBy('activity_date')
            ->get();
        foreach ($detailsA as $row) {
            $finaldate = \Carbon\Carbon::parse($row->sName)->format('Y-m-d');
            $pchart[$finaldate] = (int)$row->count;
        }

        $title = 'Graph on expense';
        \Session::flash('filter_message', 'Graph for date ' . $final_date1 . ' to ' . $final_date2);
        return view('business.customers_graph', compact('title', 'pchart'));
    }

    public function graphCustomersFilter(Request $request)
    {
        $search_key = $request->input('date');
        $date_final = explode(' - ', $search_key);
        $date1 = $date_final[0];
        $date2 = $date_final[1];

        $final_date1 = \Carbon\Carbon::parse($date1)->format('Y-m-d');
        $final_date2 = \Carbon\Carbon::parse($date2)->format('Y-m-d');
        $pchart = array();
        $business_id = \Uzyma\Business::where('user_id', \Auth::user()->id)->first();
        $detailsA = \Uzyma\BusinessActivity::select(\DB::raw('COUNT(activity_date) as count, activity_date as sName'))
            ->where('business_activity.business_id', $business_id->id)
            ->whereBetween('activity_date', array($final_date1, $final_date2))
            ->groupBy('activity_date')
            ->get();
        foreach ($detailsA as $row) {
            $finaldate = \Carbon\Carbon::parse($row->sName)->format('Y-m-d');
            $pchart[$finaldate] = (int)$row->count;

        }

        $title = 'Graph on Expenses';
        \Session::flash('filter_message', 'Graph for date ' . $final_date1 . ' to ' . $final_date2);
        return view('business.customers_graph', compact('title', 'pchart'));
    }


    /**
     *
     * Upload stylist certificates
     */

    public function certificates(Request $request)
    {
        $inputs = $request->all();
        $rule = array(
            'imageFile' => 'required',
        );
        $validator = \Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $row = \Uzyma\StylistCertificates::findOrFail($request->input('inputId'));

            if ($request->hasFile('imageFile')) {
                \File::delete(public_path() . '/packages/uploads/certs/' . $row->image_url);
                $port = $request->file('imageFile');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;

                $path = 'packages/uploads/certs';

                $request->file('imageFile')->move($path, $filename);

                $row->file_url = url('/') . '/' . $path . '/' . $filename;
            }
        } else {
            $row = new \Uzyma\StylistCertificates;

            if ($request->hasFile('imageFile')) {
                \File::delete(public_path() . '/packages/uploads/certs/' . $row->image_url);
                $port = $request->file('imageFile');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;

                $path = 'packages/uploads/certs';

                $request->file('imageFile')->move($path, $filename);

                $row->file_url = url('/') . '/' . $path . '/' . $filename;
            }
        }
        $row->stylist_id = $request->input('stylist_id');
        $row->save();
        \Session::flash('flash_message', 'Certificate saved successfully');
        return back();
    }

    public function certificatesLoad($id)
    {
        $title = "Certificates";
        $stylist = \Uzyma\Stylist::findorFail($id);
        return view('business.manage_certificates', compact('title', 'id', 'stylist'));
    }

    public function certificatesTable($id)
    {
        $activity = \Uzyma\StylistCertificates::select('id', 'file_url')->where('stylist_id', $id);
        return \Datatables::of($activity)
            ->addColumn('action', function ($activity) {
                $rend = '<a onclick="return checkDelete();" href="' . route('certificates.delete', ['id' => $activity->id]) . '" class="btn btn-xs btn-danger"  ><i class="glyphicon glyphicon-trash"></i> Delete</a>';
                return $rend;
            })
            ->make(true);
    }

    public function certificatesDelete($id)
    {
        \Uzyma\StylistCertificates::destroy($id);
        \Session::flash('flash_message', 'Certificate deleted successfully');
        return back();
    }


}
