<?php

namespace Uzyma\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Collection;
use Uzyma\User;
use Uzyma\Mail\EmailVerification;
use Mail;
use Illuminate\Foundation\Auth\RegistersUsers;

class AdminController extends Controller
{
    /*
     * Home functions
     */

    public function index()
    {
        $title = 'Home';
        $partners = \Uzyma\Partners::count();
        $activities = \Uzyma\Activities::count();
        $users = \Uzyma\AppUser::count();
        $bookings = \Uzyma\Checkout::count();
        return view('backend.home', compact('title', 'partners', 'activities', 'users', 'bookings'));
    }

    /*
     * Settings Functions
     */

    public function settings()
    {
        $title = "Settings";
        $settings = \Uzyma\Settings::first();
        return view('backend.manage_settings', compact('title', 'settings'));
    }

    public function settingsAdd(Request $request)
    {
        $inputs = $request->all();
        $rule = array(
            'site_name' => 'required',
            'site_email' => 'required',
            'site_phone' => 'required',
            'site_address' => 'required',
        );

        $validator = \Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $settings = \Uzyma\Settings::findOrFail($request->input('inputId'));
        } else {
            $settings = new \Uzyma\Settings;
        }

        $settings->site_name = $request->input('site_name');
        $settings->site_email = $request->input('site_email');
        $settings->site_phone = $request->input('site_phone');
        $settings->address = $request->input('site_address');
        $settings->save();
        if (!empty($request->input('inputId'))) {
            \Session::flash('flash_message', 'Changes Saved');

            return back();
        } else {
            \Session::flash('flash_message', 'Settings saved successfully');

            return back();
        }
    }

    public function helpCentre()
    {
        $title = "Help";
        $help = \Uzyma\HelpCentre::get();
        $help2 = \Uzyma\HelpCentreSet::get();

        return view('backend.manage_help', compact('title', 'help', 'help2'));
    }

    public function helpcentreAdd(Request $request)
    {
        $inputs = $request->all();
        $rule = array(
            'title' => 'required',
            'description' => 'required',
        );

        $validator = \Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $help = \Uzyma\HelpCentreSet::findOrFail($request->input('inputId'));
        } else {
            $help = new \Uzyma\HelpCentreSet;
        }

        $help->title = $request->input('title');
        $help->description = $request->input('description');
        $help->save();
        if (!empty($request->input('inputId'))) {
            \Session::flash('flash_message', 'Changes Saved');

            return back();
        } else {
            \Session::flash('flash_message', 'Record saved successfully');

            return back();
        }
    }

    //Business Types
    public function businessTypes()
    {
        $title = "Business Types";
        return view('backend.manage_business_types', compact('title'));
    }

    public function businessTypesAdd(Request $request)
    {
        $inputs = $request->all();
        $rule = array(
            'business_type' => 'required',
        );

        $validator = \Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $businesstype = \Uzyma\BusinessType::findOrFail($request->input('inputId'));
        } else {
            $businesstype = new \Uzyma\BusinessType;
        }

        $businesstype->name = $request->input('business_type');
        $businesstype->save();
        if (!empty($request->input('inputId'))) {
            \Session::flash('flash_message', 'Changes Saved');

            return back();
        } else {
            \Session::flash('flash_message', 'Business type saved successfully');

            return back();
        }
    }

    public function businessTypesTable()
    {
        $businesstype = \Uzyma\BusinessType::select('id', 'name');
        return \Datatables::of($businesstype)
            ->addColumn('action', function ($businesstype) {
                $edit = '<a class="btn btn-xs btn-success" href="javascript: ajaxmodaledit(' . $businesstype->id . ')"><i class="glyphicon glyphicon-pencil"></i> edit</a>';
                $delete = ' <a onclick="return confirm(\'Are you sure you want to delete this?\')" class="btn btn-xs btn-warning" href="' . route('business.types.delete', ['id' => $businesstype->id]) . '"><i class="glyphicon glyphicon-trash"></i> delete</a>';
                return $edit . $delete;
            })
            ->make(true);
    }

    public function businessTypesDelete($id)
    {
        $businesstype = \Uzyma\BusinessType::findorFail($id);
        $businesstype->delete();
        \Session::flash('flash_message', 'Business type deleted successfully');
        return back();
    }

    public function businessTypesDetails($id)
    {
        $businesstype = \Uzyma\BusinessType::findorFail($id);
        return \Response::json($businesstype);
    }

    /**
     * Partners
     */
    public function partners()
    {
        $title = "Partners";
        return view('backend.manage_partners', compact('title'));
    }

    protected function validator(array $data)
    {
        return \Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'phone_number' => 'required',
        ]);
    }

    protected function createPartner(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'account_type' => 101,
            'password' => bcrypt($data['password']),
            'email_token' => str_random(10),
        ]);
    }

    public function partnersAdd(Request $request)
    {
        // Laravel validation
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $this->throwValidationException($request, $validator);
        }
        // Using database transactions is useful here because stuff happening is actually a transaction
        // I don't know what I said in the last line! Weird!
        DB::beginTransaction();
        try {
            $user = $this->createPartner($request->all());
            DB::commit();
            $partner = new \Uzyma\Partners;
            $partner->partner_name = $request->input('name');
            $partner->phone_number = $request->input('phone_number');
            $partner->slug = str_slug('name', '-');
            $partner->user_id = $user->id;

            if ($request->hasFile('file')) {
                $port = $request->file('file');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;
                $path = 'packages/uploads/partners';
                $request->file('file')->move($path, $filename);
                $partner->portofolio = url('/') . '/' . $path . '/' . $filename;
            }
            $partner->save();

            // After creating the user send an email with the random token generated in the create method above
            $email = new EmailVerification(new User(['email_token' => $user->email_token]));
            Mail::to($user->email)->send($email);
            \Session::flash('flash_message', 'An email has been sent to ' . $user->email . ' . Please advise the partner to login to their email to activate their account');
            return back();
        } catch (Exception $e) {
            DB::rollback();
            return back();
        }
    }

    public function verify($token)
    {
        // The verified method has been added to the user model and chained here
        // for better readability
        User::where('email_token', $token)->firstOrFail()->verified();
        return redirect('login');
    }

    public function partnersTable()
    {
        $partners = \Uzyma\Partners::join('users', 'partners.user_id', '=', 'users.id')
            ->select('partners.id', 'partners.partner_name', 'partners.phone_number', 'partners.status', 'partners.image_url', 'users.email');
        return \Datatables::of($partners)
            ->addColumn('action', function ($partners) {
                if ($partners->status == 0) {
                    $activateDeactivate = '<li><a href="' . route('partners.activate', ['id' => $partners->id]) . '"><i class="glyphicon glyphicon-remove"></i> activate</a></li>';
                } else {
                    $activateDeactivate = '<li><a href="' . route('partners.deactivate', ['id' => $partners->id]) . '"><i class="glyphicon glyphicon-lock"></i> deactivate</a></li>';
                }

                $endDropdown = '<ul class="icons-list">
                                                <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                                <i class="icon-menu9"></i>
                                                        </a>

                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                                ' . $activateDeactivate . '
                                                                <li><a onclick="return checkDelete();" href="' . route('partners.delete', ['id' => $partners->id]) . '"><i class="icon-trash"></i> Delete</a></li>
                                                        </ul>
                                                </li>
                                            </ul>';


                return $endDropdown;
            })
            ->make(true);
    }

    public function partnersActivate($id)
    {
        $partners = \Uzyma\Partners::findorFail($id);
        $partners->status = 1;
        $partners->save();
        \Session::flash('flash_message', 'Partner activated successfully');
        return back();
    }

    public function partnersDeactivate($id)
    {
        $partners = \Uzyma\Partners::findorFail($id);
        $partners->status = 0;
        $partners->save();
        \Session::flash('flash_message', 'Partner deactivated successfully');
        return back();
    }

    public function partnersDelete($id)
    {
        $partners = \Uzyma\Partners::finsorFail($id);
        $partners->delete();
        \Session::flash('flash_message', 'Partner deleted successfully');
        return back();
    }

    /**
     *
     * Activity functions
     */
    public function activity()
    {
        $title = 'Activity';
        return view('backend.activity', compact('title'));
    }

    public function activityTable()
    {
        $activity = \Uzyma\Activities::leftJoin('business_type', 'activitys.service_id', '=', 'business_type.id')
            ->leftJoin('businesses', 'activitys.user_id', '=', 'businesses.user_id')
            ->select('businesses.partner_name', 'activitys.image_url', 'activitys.slug', 'activitys.id', 'business_type.name as service', 'activitys.status', 'activitys.activity_name', 'activitys.date', 'activitys.time', 'activitys.address', 'activitys.fitness_or_beauty');
        return \Datatables::of($activity)
            ->addColumn('action', function ($activity) {
                if ($activity->status == 0) {
                    $activateDeactivate = '<li><a href="' . route('activity.activate', ['id' => $activity->slug]) . '"><i class="glyphicon glyphicon-remove"></i> activate</a></li>';
                } else {
                    $activateDeactivate = '<li><a href="' . route('activity.deactivate', ['id' => $activity->slug]) . '"><i class="glyphicon glyphicon-lock"></i> deactivate</a></li>';
                }
                $images = '<li><a href="' . route('productImages', ['id' => $activity->id]) . '" role="menuitem"><i class="glyphicon glyphicon-cogs" aria-hidden="true"></i> Add images</a></li>';

                $endDropdown = '<ul class="icons-list">
                                                <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                                <i class="icon-menu9"></i>
                                                        </a>

                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                                ' . $activateDeactivate . '
                                                                    ' . $images . '
                                                                
                                                                <li><a onclick="return checkDelete();" href="' . route('activity.delete', ['id' => $activity->slug]) . '"><i class="icon-trash"></i> Delete</a></li>
                                                                    <li><a  href="javascript: ajaxmodaldetails(\'' . $activity->slug . '\')"><i class="glyphicon glyphicon-star"></i> Details</a></li>
                                                                        <li><a  href="' . route('comments', ['id' => $activity->id]) . '"><i class="glyphicon glyphicon-star"></i> Comments</a></li>
                                                        </ul>
                                                </li>
                                            </ul>';


                return $endDropdown;
            })
            ->make(true);
    }

    public function activityActivate($id)
    {
        $activity = \Uzyma\Activities::where('slug', $id)->first();
        $activity->status = 1;
        $activity->save();
        \Session::flash('flash_message', 'Activity activated successfully');
        return back();
    }

    public function activityDeactivate($id)
    {
        $activity = \Uzyma\Activities::where('slug', $id)->first();
        $activity->status = 0;
        $activity->save();
        \Session::flash('flash_message', 'Activity deactivated successfully');
        return back();
    }

    public function activityDelete($id)
    {
        $activity = \Uzyma\Activities::where('slug', $id)->first();
        $activity->delete();
        \Session::flash('flash_message', 'Activity deleted successfully');
        return back();
    }

    public function activityNew()
    {
        $title = 'Activity';
        $services = \Uzyma\BusinessType::get();
        $activity = "";
        return view('partners.activity_new', compact('title', 'services', 'activity'));
    }

    //Difficult Levels
    public function difficultyLevel()
    {
        $title = "Difficult Levels";
        return view('backend.manage_difficult_level', compact('title'));
    }

    public function difficultyLevelAdd(Request $request)
    {
        $inputs = $request->all();
        $rule = array(
            'difficult_level' => 'required',
        );

        $validator = \Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $difficulty = \Uzyma\Difficulty::findOrFail($request->input('inputId'));
        } else {
            $difficulty = new \Uzyma\Difficulty;
        }

        $difficulty->level_name = $request->input('difficult_level');
        $difficulty->save();
        if (!empty($request->input('inputId'))) {
            \Session::flash('flash_message', 'Changes Saved');

            return back();
        } else {
            \Session::flash('flash_message', 'Level saved successfully');

            return back();
        }
    }

    public function difficultyLevelTable()
    {
        $difficulty = \Uzyma\Difficulty::select('id', 'level_name');
        return \Datatables::of($difficulty)
            ->addColumn('action', function ($difficulty) {
                $edit = '<a class="btn btn-xs btn-success" href="javascript: ajaxmodaledit(' . $difficulty->id . ')"><i class="glyphicon glyphicon-pencil"></i> edit</a>';
                $delete = ' <a onclick="return confirm(\'Are you sure you want to delete this?\')" class="btn btn-xs btn-warning" href="' . route('difficultyLevel.delete', ['id' => $difficulty->id]) . '"><i class="glyphicon glyphicon-trash"></i> delete</a>';
                return $edit . $delete;
            })
            ->make(true);
    }

    public function difficultyLevelDelete($id)
    {
        $difficulty = \Uzyma\Difficulty::findorFail($id);
        $difficulty->delete();
        \Session::flash('flash_message', 'Level deleted successfully');
        return back();
    }

    public function difficultyLevelDetails($id)
    {
        $difficulty = \Uzyma\Difficulty::findorFail($id);
        return \Response::json($difficulty);
    }

    /**
     *
     * Subscribers
     */
    public function subscribers()
    {
        $title = "Subscribers";
        return view('backend.manage_subscribers', compact('title'));
    }

    public function subscribersTable()
    {
        $sub = \Uzyma\Subscription::select('email', 'id');
        return \Datatables::of($sub)
            ->make(true);
    }

    /*     * *
     * Comments
     */

    public function comments($activityID)
    {
        $title = "Comments";
        return view('backend.manage_comments', compact('title', 'activityID'));
    }

    public function commentsTable($activityID)
    {
        $comments = \Uzyma\ActivityComments::where('activity_id', $activityID)
            ->select('id', 'activity_id', 'message');
        return \Datatables::of($comments)
            ->make(true);
    }

    /**
     * Bookings
     */
    public function bookings()
    {
        $title = "Bookings";
        return view('backend.manage_bookings', compact('title'));
    }

    public function bookingsTable()
    {
        $bookings = \Uzyma\Checkout::leftJoin('app_users', 'checkout.app_user_id', '=', 'app_users.id')
            ->leftJoin('activitys', 'checkout.activity_id', '=', 'activitys.id')
            ->leftJoin('users', 'activitys.user_id', '=', 'users.id')
            ->select('activitys.activity_name', 'app_users.name', 'app_users.email', 'app_users.contact', 'checkout.id as checkoutID', 'app_users.id as appuserID', 'activitys.id as activityID', 'users.name as ownerName', 'checkout.check_out_date');
        return \Datatables::of($bookings)
            ->addColumn('action', function ($bookings) {
                $edit = '<a class="btn btn-xs btn-success" href="javascript: ajaxmodaledit(' . $bookings->checkoutID . ')"><i class="glyphicon glyphicon-pencil"></i> Details</a>';
                return $edit;
            })
            ->make(true);
    }

    /**
     * Users
     */
    public function users()
    {
        $title = "Users";
        return view('backend.manage_users', compact('title'));
    }

    public function usersTable()
    {
        $users = \Uzyma\AppUser::orderBy('id', 'DESC')
            ->select('id', 'name', 'email', 'contact', 'birth_date', 'gender', 'created_at');
        return \Datatables::of($users)
//                        ->addColumn('action', function ($users) {
//                            $edit = '<a class="btn btn-xs btn-success" href="javascript: ajaxmodaledit(' . $users->id . ')"><i class="glyphicon glyphicon-pencil"></i> Details</a>';
//                            return $edit;
//                        })
            ->make(true);
    }


    /**
     * Slider
     */
    public function slider()
    {
        $title = "Slider";

        return view('backend.manage_slider', compact('title'));
    }

    public function sliderTable()
    {
        $row = \Uzyma\Slider::select('id', 'image_url', 'slider_name');
        return \Datatables::of($row)
            ->addColumn('action', function ($row) {
                $edit = ' <a  href="javascript: ajaxmodaledit(' . $row->id . ')" class="btn btn-xs btn-primary"  ><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
                $rend = '<a onclick="return checkDelete();" href="' . route('slider.delete', ['id' => $row->id]) . '" class="btn btn-xs btn-danger" ><i class="glyphicon glyphicon-trash"></i> Delete</a>';
                return $rend . $edit;
            })
            ->make(true);
    }

    public function sliderAddEdit(Request $request)
    {
        $inputs = $request->all();
        $rule = array(

            'imageFile' => 'required',
        );
        $validator = \Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $row = \Uzyma\Slider::findOrFail($request->input('inputId'));

            if ($request->hasFile('imageFile')) {
                \File::delete(public_path() . '/packages/uploads/slider/' . $row->image_url);
                $port = $request->file('imageFile');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;

                $path = 'packages/uploads/slider';

                $request->file('imageFile')->move($path, $filename);

                $row->image_url = url('/') . '/' . $path . '/' . $filename;
            }
        } else {
            $row = new \Uzyma\Slider;

            if ($request->hasFile('imageFile')) {
                \File::delete(public_path() . '/packages/uploads/slider/' . $row->image_url);
                $port = $request->file('imageFile');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;

                $path = 'packages/uploads/slider';

                $request->file('imageFile')->move($path, $filename);

                $row->image_url = url('/') . '/' . $path . '/' . $filename;
            }
        }

        $row->slider_name = $request->input('slider_name');
        $row->save();
        \Session::flash('flash_message', 'Changes Saved successfully');
        return back();
    }

    public function sliderDetails($id)
    {
        $slider = \Uzyma\Slider::findorFail($id);
        return \Response::json($slider);
    }

    public function sliderDelete($id)
    {
        $slider = \Uzyma\Slider::findorFail($id);
        $slider->delete();
        \Session::flash('flash_message', 'slider deleted successfully');
        return back();
    }

    /**
     * Product Images
     */
    public function productImages($id)
    {
        $title = "Product Images";
        $products = \Uzyma\Activities::findorFail($id);
        return View('backend.manage_product_images', compact('title', 'products'));
    }

    public function productImagesTable($id)
    {
        $productsImages = \Uzyma\ProductsImages::select('id', 'image_url')->where('activity_id', $id);
        return \Datatables::of($productsImages)
            ->addColumn('action', function ($productsImages) {
                $rend = '<a onclick="return checkDelete();" href="' . route('productImagesDelete', ['id' => $productsImages->id]) . '" class="btn btn-xs btn-danger"  ><i class="glyphicon glyphicon-trash"></i> Delete</a>';
                return $rend;
            })
            ->make(true);
    }

    public function productImagesDelete($id)
    {
        $productsimages = \Uzyma\ProductsImages::findorFail($id);
        \File::delete(public_path() . '/packages/uploads/productsimages/' . $productsimages->image_url);
        $productsimages->delete($id);
        \Session::flash('flash_message', 'Products images deleted successfully');
        return back();
    }

    public function productImagesAdd(Request $request)
    {
        $inputs = $request->all();
        $rule = array(
            'imageFile' => 'required',
        );
        $validator = \Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $row = \Uzyma\ProductsImages::findOrFail($request->input('inputId'));

            if ($request->hasFile('imageFile')) {
                \File::delete(public_path() . '/packages/uploads/productsimages/' . $row->image_url);
                $port = $request->file('imageFile');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;

                $path = 'packages/uploads/productsimages';

                $request->file('imageFile')->move($path, $filename);

                $row->image_url = url('/') . '/' . $path . '/' . $filename;
            }
        } else {
            $row = new \Uzyma\ProductsImages;

            if ($request->hasFile('imageFile')) {
                \File::delete(public_path() . '/packages/uploads/productsimages/' . $row->image_url);
                $port = $request->file('imageFile');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;

                $path = 'packages/uploads/productsimages';

                $request->file('imageFile')->move($path, $filename);

                $row->image_url = url('/') . '/' . $path . '/' . $filename;
            }
        }
        $row->activity_id = $request->input('activity_id');
        $row->save();
        \Session::flash('flash_message', 'Changes Saved successfully');
        return back();
    }

    /**
     * Products for sale
     */
    public function addProductsSale()
    {
        $title = "Products";
        return view('backend.manage_products', compact('title'));
    }

    public function addProductsNew()
    {
        $title = "Products";
        return view('backend.add_products', compact('title'));
    }

    public function addProductsSaleTable()
    {
        $products = \Uzyma\Products::orderBY('id', 'DESC');
        return \Datatables::of($products)
            ->addColumn('action', function ($products) {
                $rend = '<li><a onclick="return checkDelete();" href="' . route('product.sell.delete', ['id' => $products->id]) . '" > Delete</a></li>';
                $images = '<li><a href="' . route('product.sell.images', ['id' => $products->id]) . '" role="menuitem"> Add images</a></li>';
                if ($products->activated == 0) {
                    $active = '<li><a href="' . route('product.sell.activate', ['id' => $products->id]) . '" role="menuitem"> Activate</a></li>';
                } else {
                    $active = '<li><a href="' . route('product.sell.deactivate', ['id' => $products->id]) . '" role="menuitem"> Deactivate</a></li>';
                }
                $endDropdown = '<ul class="icons-list">
                                                <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                                <i class="icon-menu9"></i>
                                                        </a>

                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href=" ' . route('product.sell.edit', ['id' => $products->id]) . ' " role="menuitem"> Edit</a></li>
                                                        ' . $images . '
                               
                                                        ' . $active . '
                                                        ' . $rend . '
                                                        </ul>
                                                </li>
                                            </ul>';

                return $endDropdown;
            })
            ->make(true);
    }

    public function addProductsDelete($id)
    {
        \Uzyma\Products::destroy($id);
        \Session::flash('flash_message', 'Product deleted successfully');
        return back();
    }


    public function addProductsSaleAddEdit(Request $request)
    {
        $inputs = $request->all();
        $rule = array(
            'name' => 'required',
            'price' => 'required',
            'description' => 'required',
            'quantity' => 'required',
        );

        $validator = \Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $activity = \Uzyma\Products::findOrFail($request->input('inputId'));
            if ($request->hasFile('file')) {
                \File::delete(public_path() . '/packages/uploads/products/' . $activity->image);
                $port = $request->file('file');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;
                $path = 'packages/uploads/products';
                $request->file('file')->move($path, $filename);
                $activity->image = url('/') . '/' . $path . '/' . $filename;
            }
            $activity->slug = $this->createSlug($request->input('name'), $activity->id);
        } else {
            $activity = new \Uzyma\Products;
            if ($request->hasFile('file')) {
                $port = $request->file('file');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;
                $path = 'packages/uploads/products';
                $request->file('file')->move($path, $filename);
                $activity->image = url('/') . '/' . $path . '/' . $filename;
            }
            $activity->slug = $this->createSlug($request->input('name'));
        }
        $activity->name = $request->input('name');
        $activity->description = $request->input('description');
        $activity->price = $request->input('price');
        $activity->quantity = $request->input('quantity');


        $activity->save();
        if (!empty($request->input('inputId'))) {
            \Session::flash('flash_message', 'Changes Saved successfully');
            return redirect()->route('product.sell.view');
        } else {
            \Session::flash('flash_message', 'Product saved successfully');
            return redirect()->route('product.sell.view');
        }
    }

    public function createSlug($title, $id = 0)
    {
        // Normalize the title
        $slug = str_slug($title);
        // Get any that could possibly be related.
        // This cuts the queries down by doing it once.
        $allSlugs = $this->getRelatedSlugsProducts($slug, $id);
        // If we haven't used it before then we are all good.
        if (!$allSlugs->contains('slug', $slug)) {
            return $slug;
        }
        // Just append numbers like a savage until we find not used.
        for ($i = 1; $i <= 10; $i++) {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                return $newSlug;
            }
        }
        throw new \Exception('Can not create a unique slug');
    }

    protected function getRelatedSlugsProducts($slug, $id = 0)
    {
        return \Uzyma\Products::select('slug')->where('slug', 'like', $slug . '%')
            ->where('id', '<>', $id)
            ->get();
    }

    public function addProductsEdit($id)
    {
        $activity = \Uzyma\Products::findorFail($id);
        $title = "Edit Products";
        return view('backend.add_products', compact('title', 'activity'));
    }

    public function addProductsActivate($id)
    {
        $product = \Uzyma\Products::findorFail($id);
        $product->activated = 1;
        $product->save();
        \Session::flash('flash_message', 'Product activated successfully');
        return back();
    }

    public function addProductsDeactivate($id)
    {
        $product = \Uzyma\Products::findorFail($id);
        $product->activated = 0;
        $product->save();
        \Session::flash('flash_message', 'Product deactivated successfully');
        return back();
    }

    public function addProductsImages($id)
    {
        $title = "Product Images";
        $product = \Uzyma\Products::findorFail($id);
        return view('backend.manage_product_true_images', compact('title', 'id', 'product'));
    }

    public function productSellImagesTable($id)
    {
        $productsImages = \Uzyma\ProductsImagesReloaded::select('id', 'image_url')->where('product_id', $id);
        return \Datatables::of($productsImages)
            ->addColumn('action', function ($productsImages) {
                $rend = '<a onclick="return checkDelete();" href="' . route('product.sell.ImagesDelete', ['id' => $productsImages->id]) . '" class="btn btn-xs btn-danger"  ><i class="glyphicon glyphicon-trash"></i> Delete</a>';
                return $rend;
            })
            ->make(true);
    }

    public function productSellImagesDelete($id)
    {
        $productsimages = \Uzyma\ProductsImagesReloaded::findorFail($id);
        \File::delete(public_path() . '/packages/uploads/productsimages/' . $productsimages->image_url);
        $productsimages->delete($id);
        \Session::flash('flash_message', 'Products images deleted successfully');
        return back();
    }

    public function productSellImagesAdd(Request $request)
    {
        $inputs = $request->all();
        $rule = array(
            'imageFile' => 'required',
        );
        $validator = \Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $row = \Uzyma\ProductsImagesReloaded::findOrFail($request->input('inputId'));

            if ($request->hasFile('imageFile')) {
                \File::delete(public_path() . '/packages/uploads/products/' . $row->image_url);
                $port = $request->file('imageFile');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;

                $path = 'packages/uploads/products';

                $request->file('imageFile')->move($path, $filename);

                $row->image_url = url('/') . '/' . $path . '/' . $filename;
            }
        } else {
            $row = new \Uzyma\ProductsImagesReloaded;

            if ($request->hasFile('imageFile')) {
                \File::delete(public_path() . '/packages/uploads/products/' . $row->image_url);
                $port = $request->file('imageFile');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;

                $path = 'packages/uploads/products';

                $request->file('imageFile')->move($path, $filename);

                $row->image_url = url('/') . '/' . $path . '/' . $filename;
            }
        }
        $row->product_id = $request->input('product_id');
        $row->save();
        \Session::flash('flash_message', 'Changes Saved successfully');
        return back();
    }

    /****
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     *
     *
     * Trivia codes here
     */
    //Questions
    public function questions()
    {
        $title = "Questions";
        return view('backend.manage_questions', compact('title'));
    }

    public function questionsAddView()
    {
        $title = "Add questions";
        $activity = new Collection();
        return view('backend.add_questions', compact('title', 'activity'));
    }

    public function questionsDate($id)
    {
        $title = "Questions";

        $questions = \Uzyma\Questions::where('date',$id)->get();
        //$answers = \Uzyma\Answers::where('question_id', $questions->id)->get();
        return view('backend.manage_questions_answers', compact('questions', 'title'));
    }

    public function questionsEditView($id)
    {
        $title = "Add questions";
        $activity = new Collection();
        return view('backend.add_questions', compact('title', 'activity'));
    }

    public function questionsTable()
    {
        $questions = \Uzyma\Questions::orderBY('id', 'DESC')
            ->groupBy('date')
            ->where('is_deleted', 0);
        return \Datatables::of($questions)
            ->addColumn('action', function ($questions) {
                $delete = '<li><a onclick="return checkDelete();" href="' . route('questions.delete', ['id' => $questions->id]) . '" > Delete</a></li>';


                $edit = '<li><a href="' . route('questions.edit', ['id' => $questions->id]) . '" role="menuitem"> Edit</a></li>';

                $questions = '<li><a href="' . route('questions.date', ['id' => $questions->date]) . '" role="menuitem"> Questions</a></li>';

                $endDropdown = '<ul class="icons-list">
                                                <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                                <i class="icon-menu9"></i>
                                                        </a>

                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                      ' . $edit . '
                                                        ' . $questions . '
                                                        </ul>
                                                </li>
                                            </ul>';

                return $endDropdown;
            })
            ->make(true);
    }

    public function questionsDelete($id)
    {
        $delete = \Uzyma\Questions::findOrFail($id);
        $delete->is_deleted = 1;
        $delete->save();
        \Session::flash('flash_message', 'Operation was successful');
        return back();
    }


    public function questionsAdd(Request $request)
    {
        $inputs = $request->all();
        $rule = array(
            'title' => 'required',
            'date' => 'required',
            'period' => 'required',
            'correct' => 'required',
        );
        $validator = \Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $row = \Uzyma\Questions::findOrFail($request->input('inputId'));
        } else {
            $row = new \Uzyma\Questions;
        }

        $correct = $request->input('correct');
        $answer = $request->input('answer');

        DB::beginTransaction();
        try {
            $row->title = $request->input('title');
            $row->date = $request->input('date');
            $row->period = $request->input('period');
            $row->save();
            for ($k = 0; $k < count($answer); $k++) {
                $row2 = new \Uzyma\Answers;
                if ($correct == $k) {
                    $row2->correct = 1;
                }
                $row2->answer = $answer[$k];
                $row2->question_id = $row->id;
                $row2->save();
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        \Session::flash('flash_message', 'Changes Saved successfully');
        return back();
    }

    public function questionDelete($id)
    {
        $questions = \Uzyma\Questions::findOrFail($id);
        $questions->is_deleted = 1;
        $questions->save();
        \Session::flash('flash_message', 'Operation was successful');
        return back();
    }

    //Answers function
    public function answers()
    {
        $title = "Answers";
        return view('backend.manage_answers', compact('title'));
    }

    public function answersAdd(Request $request)
    {
        $inputs = $request->all();
        $rule = array(
            'answer' => 'required',
        );
        $validator = \Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $row = \Uzyma\Answers::findOrFail($request->input('inputId'));
        } else {
            $row = new \Uzyma\Answers;
        }
        $checkbox = $request->input('correct');
        $answer = $request->input('answer');
        $question_id = $request->input('question_id');
        for ($i = 0; $i < count($checkbox); $i++) {
            $id = $checkbox[$i];
            if ($id == 1) {
                $correct_or_not = 1;
            } else {
                $correct_or_not = 0;
            }
            $row->answer = $answer[$i];
            $row->question_id = $question_id;
            $row->correct = $correct_or_not;
            $row->save();
        }

        \Session::flash('flash_message', 'Changes Saved successfully');
        return back();
    }


}
