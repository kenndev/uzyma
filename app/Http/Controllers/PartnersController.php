<?php

namespace Uzyma\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Uzyma\User;
use Uzyma\Mail\EmailVerification;
use Mail;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Collection;

class PartnersController extends Controller
{

    /**
     * Begining of partners module
     * service = 1 , product = 2
     */
    public function partnersindex()
    {
        $title = 'Partners Home';
        $bookings = \Uzyma\Checkout::where('owner_id', \Auth::user()->id)->count();
        $activities = \Uzyma\Activities::where('user_id', \Auth::user()->id)->count();
        return view('partners.home', compact('title', 'bookings', 'activities'));
    }

    public function activity()
    {
        $title = 'Activity';
        $services = \Uzyma\BusinessType::get();
        $levels = \Uzyma\Difficulty::get();

        return view('partners.activity', compact('title', 'services', 'levels'));
    }

    public function getOther()
    {
        $other = \Uzyma\Other::get();
        return $other;
    }

    public function other($id)
    {
        $title = "Tags";
        return view('partners.other', compact('title', 'id'));
    }

    public function otherTable($id)
    {
        $other = \Uzyma\Other::where('activity_id', $id)->select('id', 'name', 'image_url');
        return \Datatables::of($other)
            ->addColumn('action', function ($other) {
                $rend = '<a onclick="return checkDelete();" href="' . route('other.delete', ['id' => $other->id]) . '" class="btn btn-xs btn-danger"  ><i class="glyphicon glyphicon-trash"></i> Delete</a>';
                return $rend;
            })
            ->make(true);
    }

    public function getAmenities()
    {
        $other = \Uzyma\Amenities::get();
        return $other;
    }

    public function otherDelete($id)
    {
        $delete = \Uzyma\Other::findOrFail($id);
        $delete->delete();
        \Session::flash('flash_message', 'Deleted successfully');
        return back();
    }

    public function amenities()
    {
        $title = "Tags";
        return view('partners.amenities', compact('title'));
    }

    public function amenitiesTable()
    {
        $other = \Uzyma\Amenities::select('name');
        return \Datatables::of($other)
            ->make(true);
    }

    public function otherAdd(Request $request)
    {

        $inputs = $request->all();
        $rule = array(
            'name' => 'required',
            'imageFile' => 'required',
        );
        $validator = \Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $row = \Uzyma\Other::findOrFail($request->input('inputId'));

            if ($request->hasFile('imageFile')) {
                \File::delete(public_path() . '/packages/uploads/shades/' . $row->image_url);
                $port = $request->file('imageFile');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;

                $path = 'packages/uploads/shades';

                $request->file('imageFile')->move($path, $filename);

                $row->image_url = url('/') . '/' . $path . '/' . $filename;
            }
        } else {
            $row = new \Uzyma\Other();

            if ($request->hasFile('imageFile')) {
                \File::delete(public_path() . '/packages/uploads/shades/' . $row->image_url);
                $port = $request->file('imageFile');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;

                $path = 'packages/uploads/shades';

                $request->file('imageFile')->move($path, $filename);

                $row->image_url = url('/') . '/' . $path . '/' . $filename;
            }
        }
        $row->name = $request->input('name');
        $row->activity_id = $request->input('activity_id');
        $row->save();
        \Session::flash('flash_message', 'Changes Saved successfully');
        return back();
    }

    public function amenitiesAdd(Request $request)
    {
        $inputs = $request->all();
        $rule = array(
            'name' => 'required',
        );
        $validator = \Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $row = \Uzyma\Amenities::findOrFail($request->input('inputId'));

        } else {
            $row = new \Uzyma\Amenities;

        }
        $row->name = $request->input('name');
        $row->save();
        \Session::flash('flash_message', 'Saved successfully');
        return back();
    }

    public function activityAdd(Request $request)
    {
        $inputs = $request->all();
        $rule = array(
            'activity_name' => 'required',
            'price' => 'required',
            'description' => 'required',
            'fitness_or_beauty' => 'required'
        );

        $validator = \Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $activity = \Uzyma\Activities::findOrFail($request->input('inputId'));
            if ($request->hasFile('file')) {
                \File::delete(public_path() . '/packages/uploads/activity/' . $activity->image_url);
                $port = $request->file('file');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;
                $path = 'packages/uploads/activity';
                $request->file('file')->move($path, $filename);
                $activity->image_url = url('/') . '/' . $path . '/' . $filename;
            }
            $activity->slug = $this->createSlug($request->input('activity_name'), $activity->id);
        } else {
            $activity = new \Uzyma\Activities;
            if ($request->hasFile('file')) {
                $port = $request->file('file');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;
                $path = 'packages/uploads/activity';
                $request->file('file')->move($path, $filename);
                $activity->image_url = url('/') . '/' . $path . '/' . $filename;
            }
            $activity->slug = $this->createSlug($request->input('activity_name'));
        }
        $tag_entries = array();
        $size_item = $request->input('tags');
        $number_of_size_entries = sizeof($size_item);
        for ($i = 0; $i < $number_of_size_entries; $i++) {
            if ($size_item[$i] != "") {
                $new_entry = array($size_item[$i]);
                array_push($tag_entries, $new_entry);
            }
        }

        $activity->tags = json_encode($tag_entries);

        $tag_entries2 = array();
        $size_item2 = $request->input('amenities2');


        $number_of_size_entries2 = sizeof($size_item2);
        for ($k = 0; $k < $number_of_size_entries2; $k++) {
            if ($size_item2[$k] != "") {

                $new_entry2 = array($size_item2[$k]);


                array_push($tag_entries2, $new_entry2);
            }
        }


        $activity->activity_amen = json_encode($tag_entries2);

        $activity->activity_name = $request->input('activity_name');
        $activity->activity_description = $request->input('description');
        $activity->service_id = $request->input('select_service');
        $activity->date = $request->input('date');
        $time_now = date("H:i", strtotime($request->input('time')));
        $activity->time = $time_now;

        $activity->latitude = $request->input('latitude');
        $activity->longitude = $request->input('longitude');
        if ($request->input('address')) {
            $activity->address = $request->input('address');
        } else {
            $activity->address = $request->input('homevisit');
        }

        $activity->service_type = $request->input('service_type');

        $activity->duration = $request->input('duration');
        $activity->stylist = $request->input('stylist');

        $activity->rating = $request->input('rating');
        $activity->fitness_or_beauty = $request->input('fitness_or_beauty');
        $activity->user_id = \Auth::user()->id;

        $activity->cancelationdate = $request->input('cancelationdate');
        $activity->price = $request->input('price');
        $activity->difficult_level = $request->input('levels');

        $activity->save();
        if (!empty($request->input('inputId'))) {
            \Session::flash('flash_message', 'Changes Saved successfully');
            return redirect()->route('activity');
        } else {
            \Session::flash('flash_message', 'Activity saved successfully');
            return redirect()->route('activity');
        }
    }

    public function createSlug($title, $id = 0)
    {
        // Normalize the title
        $slug = str_slug($title);
        // Get any that could possibly be related.
        // This cuts the queries down by doing it once.
        $allSlugs = $this->getRelatedSlugsActivity($slug, $id);
        // If we haven't used it before then we are all good.
        if (!$allSlugs->contains('slug', $slug)) {
            return $slug;
        }
        // Just append numbers like a savage until we find not used.
        for ($i = 1; $i <= 10; $i++) {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                return $newSlug;
            }
        }
        throw new \Exception('Can not create a unique slug');
    }

    protected function getRelatedSlugsActivity($slug, $id = 0)
    {
        return \Uzyma\Activities::select('slug')->where('slug', 'like', $slug . '%')
            ->where('id', '<>', $id)
            ->get();
    }

    public function activityTable()
    {
        $activity = \Uzyma\Activities::leftJoin('business_type', 'activitys.service_id', '=', 'business_type.id')
            ->where('user_id', \Auth::user()->id)
            ->select('activitys.image_url', 'activitys.slug', 'activitys.id', 'business_type.name as service', 'activitys.status', 'activitys.activity_name', 'activitys.date', 'activitys.time', 'activitys.address', 'activitys.fitness_or_beauty');
        return \Datatables::of($activity)
            ->addColumn('action', function ($activity) {
                if ($activity->status == 0) {
                    $activateDeactivate = '<li><a href="' . route('activity.activate', ['id' => $activity->slug]) . '"><i class="glyphicon glyphicon-remove"></i> activate</a></li>';
                } else {
                    $activateDeactivate = '<li><a href="' . route('activity.deactivate', ['id' => $activity->slug]) . '"><i class="glyphicon glyphicon-lock"></i> deactivate</a></li>';
                }
                $images = '<li><a href="' . route('productImages.partner', ['id' => $activity->id]) . '" role="menuitem"><i class="glyphicon glyphicon-cogs" aria-hidden="true"></i> Add images</a></li>';
                $shades = '<li><a href="' . route('other', ['id' => $activity->id]) . '" role="menuitem"><i class="glyphicon glyphicon-arrow-right" aria-hidden="true"></i>Shades</a></li>';

                $endDropdown = '<ul class="icons-list">
                                                <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                                <i class="icon-menu9"></i>
                                                        </a>

                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                                ' . $activateDeactivate . '
                                                                    ' . $images . '
                                                                    ' . $shades . '
                                                                <li><a href="' . route('activity.edit', ['id' => $activity->id]) . '"><i class="glyphicon glyphicon-pencil"></i> Edit</a></li>
                                                                <li><a onclick="return checkDelete();" href="' . route('activity.delete', ['id' => $activity->id]) . '"><i class="icon-trash"></i> Delete</a></li>
                                                                <li><a  href="javascript: ajaxmodaldetails(\'' . $activity->id . '\')"><i class="glyphicon glyphicon-star"></i> Details</a></li>
                                                                    <li><a  href="' . route('comments', ['id' => $activity->id]) . '"><i class="glyphicon glyphicon-star"></i> Comments</a></li>
                                                        </ul>
                                                </li>
                                            </ul>';


                return $endDropdown;
            })
            ->make(true);
    }

    public function activityActivate($id)
    {
        $activity = \Uzyma\Activities::where('slug', $id)->first();
        $activity->status = 1;
        $activity->save();
        \Session::flash('flash_message', 'Activity activated successfully');
        return back();
    }

    public function activityDeactivate($id)
    {
        $activity = \Uzyma\Activities::where('slug', $id)->first();
        $activity->status = 0;
        $activity->save();
        \Session::flash('flash_message', 'Activity deactivated successfully');
        return back();
    }

    public function activityDelete($id)
    {
        $activity = \Uzyma\Activities::findorFail($id);
        $activity->delete();
        \Session::flash('flash_message', 'Activity deleted successfully');
        return back();
    }

    public function activityNew()
    {
        $title = 'Activity';
        $services = \Uzyma\BusinessType::get();
        $others = $this->getOther();
        $amenities = $this->getAmenities();
        $activity = new Collection();
        $levels = \Uzyma\Difficulty::get();
        $business_id = \Uzyma\Business::where('user_id', \Auth::user()->id)->first();
        $stylist = \Uzyma\Stylist::where('business_id', $business_id->id)->get();

        return view('partners.activity_new', compact('stylist','title', 'services', 'activity', 'levels', 'others', 'amenities'));
    }

    public function activityEdit($id)
    {
        $title = 'Activity Edit';
        $activity = \Uzyma\Activities::findorFail($id);
        $services = \Uzyma\BusinessType::get();
        $levels = \Uzyma\Difficulty::get();
        $others = $this->getOther();
        $amenities = $this->getAmenities();
        $business_id = \Uzyma\Business::where('user_id', \Auth::user()->id)->first();
        $stylist = \Uzyma\Stylist::where('business_id', $business_id->id)->get();
        return view('partners.activity_new', compact('stylist','title', 'services', 'activity', 'levels', 'others', 'amenities'));
    }

    public function activityDetails($id)
    {
        $activityres = \Uzyma\Activities::leftJoin('business_type', 'activitys.service_id', '=', 'business_type.id')
            ->leftJoin('difficulty', 'activitys.difficult_level', '=', 'difficulty.id')
            ->where('activitys.id', $id)
            ->select('activitys.activity_description', 'activitys.cancelationdate as cancel', 'difficulty.level_name', 'activitys.image_url', 'activitys.slug', 'activitys.id', 'business_type.name as service', 'activitys.status', 'activitys.activity_name', 'activitys.date', 'activitys.time', 'activitys.address')
            ->first();


        return \Response::json($activityres);
    }

    /**
     *
     * @return type
     * Bookings
     */
    public function bookings()
    {
        $title = "Bookings";
        return view('partners.manage_bookings', compact('title'));
    }

    public function bookingsTable()
    {
        $bookings = \Uzyma\Checkout::where('owner_id', \Auth::user()->id)
            ->join('app_users', 'checkout.app_user_id', '=', 'app_users.id')
            ->join('activitys', 'checkout.activity_id', '=', 'activitys.id')
            ->select('checkout.total', 'checkout.check_out_date', 'activitys.activity_name', 'app_users.name', 'app_users.email', 'app_users.contact', 'checkout.id as checkoutID', 'app_users.id as appuserID', 'activitys.id as activityID');
        return \Datatables::of($bookings)
            ->addColumn('action', function ($bookings) {
                $edit = '<a class="btn btn-xs btn-success" href="javascript: ajaxmodaledit(' . $bookings->checkoutID . ')"><i class="glyphicon glyphicon-pencil"></i> Details</a>';
                return $edit;
            })
            ->make(true);
    }

    public function bookingSearch(Request $request)
    {
        $bookings = \Uzyma\Checkout::where('owner_id', \Auth::user()->id)
            ->whereBetween('check_out_date', array(new \DateTime($request->input('start_date')), new \DateTime($request->input('end_date'))))
            ->join('app_users', 'checkout.app_user_id', '=', 'app_users.id')
            ->join('activitys', 'checkout.activity_id', '=', 'activitys.id')
            ->select('checkout.total', 'checkout.check_out_date', 'activitys.activity_name', 'app_users.name', 'app_users.email', 'app_users.contact', 'checkout.id as checkoutID', 'app_users.id as appuserID', 'activitys.id as activityID');
        return \Datatables::of($bookings)
            ->addColumn('action', function ($bookings) {
                $edit = '<a class="btn btn-xs btn-success" href="javascript: ajaxmodaledit(' . $bookings->checkoutID . ')"><i class="glyphicon glyphicon-pencil"></i> Details</a>';
                return $edit;
            })
            ->make(true);
    }


    /**
     * Product Images
     */
    public function productImages($id)
    {
        $title = "Product Images";
        $products = \Uzyma\Activities::findorFail($id);
        return View('partners.manage_product_images2', compact('title', 'products'));
    }

    public function productImagesTable($id)
    {
        $productsImages = \Uzyma\ProductsImages::select('id', 'image_url')->where('activity_id', $id);
        return \Datatables::of($productsImages)
            ->addColumn('action', function ($productsImages) {
                $rend = '<a onclick="return checkDelete();" href="' . route('productImagesDelete.partner', ['id' => $productsImages->id]) . '" class="btn btn-xs btn-danger"  ><i class="glyphicon glyphicon-trash"></i> Delete</a>';
                return $rend;
            })
            ->make(true);
    }

    public function productImagesDelete($id)
    {
        $productsimages = \Uzyma\ProductsImages::findorFail($id);
        \File::delete(public_path() . '/packages/uploads/productsimages/' . $productsimages->image_url);
        $productsimages->delete($id);
        \Session::flash('flash_message', 'Products images deleted successfully');
        return back();
    }

    public function productImagesAdd(Request $request)
    {
        $inputs = $request->all();
        $rule = array(
            'imageFile' => 'required',
        );
        $validator = \Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $row = \Uzyma\ProductsImages::findOrFail($request->input('inputId'));

            if ($request->hasFile('imageFile')) {
                \File::delete(public_path() . '/packages/uploads/productsimages/' . $row->image_url);
                $port = $request->file('imageFile');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;

                $path = 'packages/uploads/productsimages';

                $request->file('imageFile')->move($path, $filename);

                $row->image_url = url('/') . '/' . $path . '/' . $filename;
            }
        } else {
            $row = new \Uzyma\ProductsImages;

            if ($request->hasFile('imageFile')) {
                \File::delete(public_path() . '/packages/uploads/productsimages/' . $row->image_url);
                $port = $request->file('imageFile');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;

                $path = 'packages/uploads/productsimages';

                $request->file('imageFile')->move($path, $filename);

                $row->image_url = url('/') . '/' . $path . '/' . $filename;
            }
        }
        $row->activity_id = $request->input('activity_id');
        $row->save();
        \Session::flash('flash_message', 'Changes Saved successfully');
        return back();
    }






    public function stylistTable()
    {
        $business_id = \Uzyma\Business::where('user_id', \Auth::user()->id)->first();
        var_dump($business_id);
        $stylist = \Uzyma\Stylist::where('is_delete',0)->where('business_id', $business_id->id)->select('id', 'name', 'phone_number', 'national_id', 'area_of_expertise', 'image_url');
        return \Datatables::of($stylist)
            ->addColumn('action', function ($stylist) {
                $edit = '<a class="btn btn-xs btn-success" href="javascript: ajaxmodaledit(' . $stylist->id . ')"><i class="glyphicon glyphicon-pencil"></i> edit</a>';
                $certs = ' <a class="btn btn-xs btn-primary" href="' . route('certificates', ['id' => $stylist->id]) . '"><i class="icon-newspaper"></i> cerificates</a>';
                $delete = ' <a onclick="return confirm(\'Are you sure you want to delete this?\')" class="btn btn-xs btn-warning" href="' . route('stylist.delete', ['id' => $stylist->id]) . '"><i class="glyphicon glyphicon-trash"></i> delete</a>';
                return $edit . $certs . $delete;
            })
            ->make(true);
    }

}
