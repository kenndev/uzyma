<?php

namespace Uzyma\Http\Controllers;

use Illuminate\Http\Request;
use Uzyma\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Uzyma\Rate;
use Uzyma\Transactions;
use Uzyma\User;
use Lang;
use Uzyma\CustomerType;
use Illuminate\Support\Facades\Auth;

class MobileController extends Controller
{
    /*
     * Save app user to database
     */

    //sign in
    public function signIn(Request $request)
    {
        $firebase_id = $request->input('firebase_id');
        $name = $request->input('name');
        $email = $request->input('email');
        $terms = $request->input('terms', false);
        $image = $request->input('image');
        $contact = $request->input('contact');
        $emailExists = \Uzyma\AppUser::where('email', $request->input('email'))
            ->first();

        if ($emailExists) {
            $response["result"] = "success";
            $response["message"] = "User Data has synced successfully";
            $response["name"] = $emailExists->name;
            $response["email"] = $emailExists->email;
            $response["firebase_id"] = $emailExists->firebase_id;
            $response["gender"] = $emailExists->gender;
            $response["contact"] = $emailExists->contact;
            $response["image"] = $emailExists->image;
            $response["terms"] = $emailExists->terms;
            $response["birth_date"] = $emailExists->birth_date;
            $response["latitude"] = $emailExists->latitude;
            $response["longitude"] = $emailExists->longitude;
            $response["created_at"] = $emailExists->created_at;
            $response["location"] = $emailExists->location;
            $response["national_id"] = $emailExists->national_id;
            echo json_encode($response);
        } else {
            $myuser = new \Uzyma\AppUser();
            $myuser->name = $name;
            $myuser->email = $email;
            $myuser->terms = $terms;
            $myuser->image = $image;
            $myuser->contact = $contact;
            $myuser->firebase_id = $firebase_id;
            $saved = $myuser->save();

            $wallet = new \Uzyma\Wallet();
            $wallet->userid = $myuser->id;
            $wallet->amount = 0;
            $wallet->save();

            if ($saved) { //updating
                $response["result"] = "success";
                $response["message"] = "User Data has synced successfully";
                $response["name"] = $myuser->name;
                $response["email"] = $myuser->email;
                $response["firebase_id"] = $myuser->firebase_id;
                $response["gender"] = $myuser->gender;
                $response["contact"] = $myuser->contact;
                $response["image"] = $myuser->image;
                $response["terms"] = $myuser->terms;
                $response["latitude"] = $myuser->latitude;
                $response["longitude"] = $myuser->longitude;
                $response["created_at"] = $myuser->created_at;
                $response["birth_date"] = $myuser->birth_date;
                $response["location"] = $myuser->location;
                $response["national_id"] = $myuser->national_id;

                echo json_encode($response);
            } else {
                $response["result"] = "failure";
                $response["message"] = "Error saving data";
                echo json_encode($response);
            }
        }
    }

    //sign up
    public function signUp(Request $request)
    {
        $firebase_id = $request->input('firebase_id');
        $name = $request->input('name');
        $email = $request->input('email');
        $terms = $request->input('terms');
        $gender = $request->input('gender');
        $image = $request->input('image');
        $contact = $request->input('contact');
        $emailExists = \Uzyma\AppUser::where('email', $request->input('email'))
            ->first();

        if ($emailExists) {
            $response["result"] = "success";
            $response["message"] = "User Data has synced successfully";
            $response["name"] = $emailExists->name;
            $response["email"] = $emailExists->email;
            $response["firebase_id"] = $emailExists->firebase_id;
            $response["gender"] = $emailExists->gender;
            $response["contact"] = $emailExists->contact;
            $response["image"] = $emailExists->image;
            $response["terms"] = $emailExists->terms;
            $response["birth_date"] = $emailExists->birth_date;
            $response["latitude"] = $emailExists->latitude;
            $response["longitude"] = $emailExists->longitude;
            $response["created_at"] = $emailExists->created_at;
            $response["location"] = $emailExists->location;
            $response["national_id"] = $emailExists->national_id;
            echo json_encode($response);
        } else {
            $myuser = new \Uzyma\AppUser();
            $myuser->name = $name;
            $myuser->email = $email;
            $myuser->terms = $terms;
            $myuser->gender = $gender;
            $myuser->image = $image;
            $myuser->contact = $contact;
            $myuser->firebase_id = $firebase_id;
            $saved = $myuser->save();

            $wallet = new \Uzyma\Wallet();
            $wallet->userid = $myuser->id;
            $wallet->amount = 0;
            $wallet->save();
            if ($saved) { //updating
                $response["result"] = "success";
                $response["message"] = "User Data has synced successfully";
                $response["name"] = $myuser->name;
                $response["email"] = $myuser->email;
                $response["firebase_id"] = $myuser->firebase_id;
                $response["gender"] = $myuser->gender;
                $response["contact"] = $myuser->contact;
                $response["image"] = $myuser->image;
                $response["terms"] = $myuser->terms;
                $response["latitude"] = $myuser->latitude;
                $response["longitude"] = $myuser->longitude;
                $response["location"] = $myuser->location;
                $response["created_at"] = $myuser->created_at;
                $response["birth_date"] = $myuser->birth_date;
                $response["national_id"] = $myuser->national_id;
                echo json_encode($response);
            } else {
                $response["result"] = "failure";
                $response["message"] = "Error saving data";
                echo json_encode($response);
            }
        }
    }

    public function updateUser(Request $request)
    {
        $gender = $request->input('gender');
        $name = $request->input('name');
        $terms = $request->input('terms');
        $firebase_id = $request->input('firebase_id');
        $contact = $request->input('contact');
        $birth_date = $request->input('birth_date');
        $location = $request->input('location');
        $latitude = $request->input('latitude');
        $longitude = $request->input('longitude');
        $nationalId = $request->input('national_id');
        $appuser = \Uzyma\AppUser::where('firebase_id', $firebase_id)->first();
        $appuser->gender = $gender;
        $appuser->name = $name;
        $appuser->terms = $terms;
        $appuser->contact = $contact;
        $appuser->birth_date = $birth_date;
        $appuser->location = $location;
        $appuser->latitude = $latitude;
        $appuser->longitude = $longitude;
        $appuser->national_id = $nationalId;
        $appuser->save();
        if ($appuser->id) {
            $response["result"] = "success";
            $response["message"] = "Profile Updated Successfully";
            $response["name"] = $appuser->name;
            $response["email"] = $appuser->email;
            $response["firebase_id"] = $appuser->firebase_id;
            $response["gender"] = $appuser->gender;
            $response["contact"] = $appuser->contact;
            $response["image"] = $appuser->image;
            $response["terms"] = $appuser->terms;
            $response["location"] = $appuser->location;
            $response["latitude"] = $appuser->latitude;
            $response["longitude"] = $appuser->longitude;
            $response["created_at"] = $appuser->created_at;
            $response["birth_date"] = $appuser->birth_date;
            $response["national_id"] = $appuser->national_id;
            echo json_encode($response);
        }
    }

    public function updateProfilePic(Request $request)
    {
        $firebase_id = $request->input('firebase_id');
        $appuser = \Uzyma\AppUser::where('firebase_id', $firebase_id)->first();
        \File::delete('"' . $appuser->image . '"');
        $destinationPath = 'packages/uploads/profile/' . \Carbon\Carbon::now()->year . '/' . \Carbon\Carbon::now()->month;
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }

        $filename = \Carbon\Carbon::now()->year . '_' . \Carbon\Carbon::now()->month . '_' . \Carbon\Carbon::now()->day . '_' . time() . '.png';
        $base64 = $request->input('file');

        // This saves the base64encoded destinationPath
        $upload_success = file_put_contents($destinationPath . '/' . $filename, base64_decode($base64));
        if ($upload_success) {
            $appuser->image = url('/') . '/' . $destinationPath . '/' . $filename;
            $appuser->save();
            $response["result"] = "success";
            $response["message"] = "Profile Picture Updated Successfully";
            $response["name"] = $appuser->name;
            $response["email"] = $appuser->email;
            $response["firebase_id"] = $appuser->firebase_id;
            $response["gender"] = $appuser->gender;
            $response["contact"] = $appuser->contact;
            $response["image"] = $appuser->image;
            $response["terms"] = $appuser->terms;
            $response["birth_date"] = $appuser->birth_date;
            $response["latitude"] = $appuser->latitude;
            $response["longitude"] = $appuser->longitude;
            $response["created_at"] = $appuser->created_at;
            $response["national_id"] = $appuser->national_id;
            echo json_encode($response);
        } else {
            $response["result"] = "failure";
            $response["message"] = "Error saving Image";
            echo json_encode($response);
        }
    }

    public function getServices()
    {
        $services = \Uzyma\BusinessType::select('id as serviceId', 'name')->get();
        $response["result"] = "success";
        $response["message"] = "Services retrived successfully";
        $response["services"] = $services;
        echo json_encode($response);
    }

    public function getActivities()
    {
        $activities = \Uzyma\Activities::where('activitys.status', 1)
            ->leftJoin('partners', 'activitys.user_id', '=', 'partners.user_id')
            ->leftJoin('business_type', 'activitys.service_id', '=', 'business_type.id')
            ->leftJoin('difficulty', 'activitys.difficult_level', '=', 'difficulty.id')
            ->select('activitys.servi/ho    ce_id','activitys.service_type','activitys.fitness_or_beauty', 'activitys.tags', 'activitys.rating', 'activitys.activity_amen as amenities', 'activitys.stylist', 'activitys.duration', 'activitys.id as activityID', 'activitys.price', 'activitys.cancelationdate as cancelationDate', 'difficulty.level_name as difficulty_level', 'activitys.activity_name', 'activitys.activity_description', 'partners.partner_name as partnerName', 'business_type.name as serviceName', 'activitys.image_url', 'activitys.date', 'activitys.time', 'activitys.latitude', 'activitys.longitude', 'activitys.address')
            ->orderBy('activitys.id','DESC')
            ->get();
        $response["result"] = "success";
        $response["message"] = "Activities retrieved successfully";
        $response["activities"] = $activities;
        return response()->json($response);
        //echo json_encode($response);
    }

    public function getStylistClient(Request $request){
        $stylist_id = $request->input('stylist_id');
        $stylist = \Uzyma\Stylist::leftJoin('businesses','stylist.business_id','=','businesses.id')
            ->where('stylist.id',$stylist_id)
            ->select('businesses.business_name','stylist.name','stylist.image_url','stylist.phone_number')
            ->first();
        $response["result"] = "success";
        $response["message"] = "Stylist information retrieved successfully";
        $response["stylist"] = $stylist;
        return response()->json($response);
    }

    public function saveNationalId(Request $request)
    {
        $national_id = $request->input('national_id');
        $app_user_id = $request->input('user_id');
        $appuser = \Uzyma\AppUser::where('firebase_id', $app_user_id)->first();
        $appuser->national_id = $national_id;
        $appuser->save();
        $response["result"] = "success";
        $response["message"] = "Profile Updated Successfully";
        $response["name"] = $appuser->name;
        $response["email"] = $appuser->email;
        $response["firebase_id"] = $appuser->firebase_id;
        $response["gender"] = $appuser->gender;
        $response["contact"] = $appuser->contact;
        $response["image"] = $appuser->image;
        $response["terms"] = $appuser->terms;
        $response["location"] = $appuser->location;
        $response["latitude"] = $appuser->latitude;
        $response["longitude"] = $appuser->longitude;
        $response["created_at"] = $appuser->created_at;
        $response["birth_date"] = $appuser->birth_date;
        $response["national_id"] = $appuser->national_id;
        return response()->json($response);
    }

    public function getNotifications(Request $request)
    {
        $firebase_id = $request->input('firebase_id');

        $appuser = \Uzyma\AppUser::where('firebase_id', $firebase_id)
            ->orderBy('id','DESC')
            ->first();


        $notifications = \Uzyma\Notifications::where('notifieeid', $appuser->id)->get();
        $response["result"] = "success";
        $response["message"] = "Notifications pulled Successfully";
        $response['notifications'] = $notifications;
        return response()->json($response);
    }

    public function checkOut(Request $request)
    {
        $app_user_id = $request->input('firebase_id');
        $activity_id = $request->input('activity_id');
        $activity_id = $request->input('service_or_product');
        $activity_id = $request->input('date');
        $activity_id = $request->input('start_time');
        $activity_id = $request->input('end_time');


        $user_id = \Uzyma\AppUser::where('firebase_id', $app_user_id)->first();
        $date_now = \Carbon\Carbon::now('Africa/Nairobi');

        $ownerId = \Uzyma\Activities::findorFail($activity_id);

        $check_out = new \Uzyma\Checkout();
        $check_out->app_user_id = $user_id->id;
        $check_out->activity_id = $activity_id;
        $check_out->check_out_date = $date_now;
        $check_out->owner_id = $ownerId->user_id;
        $check_out->service_or_product = $request->input('service_or_product');
        $check_out->total = $ownerId->price;
        $check_out->save();
        $response["result"] = "success";
        $response["message"] = "You have checked out successfully";
        echo json_encode($response);
    }

    public function checkout2(Request $request)
    {
        $amount = $request->input('final_amount');
        $firebase_id = $request->input('firebase_id');
        $details4 = $request->input('details');


        $user_id = \Uzyma\AppUser::where('firebase_id', $firebase_id)->first();
        $date_now = \Carbon\Carbon::now('Africa/Nairobi');
        $wallet = \Uzyma\Wallet::where('userid', $user_id->id)->first();
        $current_amount = $wallet->amount;
//        echo "Current amount " .$current_amount;
//        echo "Amount" . $amount;


        if ($current_amount >= $amount) {
//        $gg = array(array('activity_id' => '2',
//            'service_or_product' => '2',
//            'date' => '2',
//            'start_time' => '2',
//            'end_time' => '2',
//            'total' => '2',
//            'quantity' => '2'));
//
//        $details3 = json_encode($gg);
//
//        $details = json_decode($details3, true);
//        print_r($details);

            $details = json_decode($details4, true);


            for ($i = 0; $i < count($details); $i++) {

                $ownerId = \Uzyma\Activities::findorFail($details[$i]['activity_id']);
                $check_out = new \Uzyma\Checkout();
                $check_out->app_user_id = $user_id->id;
                $check_out->activity_id = $details[$i]['activity_id'];
                $check_out->owner_id = $ownerId;
                $check_out->service_or_product = $details[$i]['service_or_product'];
                $check_out->paid = 1;
                $check_out->check_out_date = $date_now;
                $check_out->date = $details[$i]['date'];
                $check_out->start_time = $details[$i]['start_time'];
                $check_out->end_time = $details[$i]['end_time'];
                $check_out->total = $details[$i]['total'];
                $check_out->final_amount = $amount;
                $check_out->quantity = $details[$i]['Quantity'];
                $check_out->save();
            }

            $wallet_amount = $wallet->amount - $amount;
            $wallet->amount = $wallet_amount;
            $wallet->save();

            $notification = new \Uzyma\Notifications;
            $notification->title = "Check out!";
            $notification->message = "Paid for services on Uzyma worth " . $amount;
            $notification->notifierid = "0";
            $notification->notifieeid = $user_id->id;
            $notification->created_at = \Carbon\Carbon::today()->format('Y-m-d');
            $notification->save();

            $transaction = new Transactions();
            $transaction->userid = $user_id->id;
            $transaction->transaction = "Account debited";
            $transaction->amount = $amount;
            $transaction->created_at = $date_now;
            $transaction->save();
            $response["result"] = "success";
            $response["message"] = "You have checked out successfully";
        } else {
            $response["result"] = "fail";
            $response["message"] = "Woops! insufficients funds";
        }

        echo json_encode($response);

    }

    public function checkout3(Request $request)
    {
        $amount = $request->input('final_amount');
        $firebase_id = $request->input('firebase_id');
        $details4 = $request->input('details');
        $mpesa_id = $request->input('checkout');


        $user_id = \Uzyma\AppUser::where('firebase_id', $firebase_id)->first();
        $date_now = \Carbon\Carbon::now('Africa/Nairobi');
        $wallet = \Uzyma\Wallet::where('userid', $user_id->id)->first();
        $current_amount = $wallet->amount;

        if ($current_amount >= $amount) {

            $details = json_decode($details4, true);


            for ($i = 0; $i < count($details); $i++) {

                $ownerId = \Uzyma\Activities::findorFail($details[$i]['activity_id']);
                $check_out = new \Uzyma\Checkout();
                $check_out->app_user_id = $user_id->id;
                $check_out->activity_id = $details[$i]['activity_id'];
                $check_out->owner_id = $ownerId;
                $check_out->service_or_product = $details[$i]['service_or_product'];
                $check_out->paid = 1;
                $check_out->check_out_date = $date_now;
                $check_out->date = $details[$i]['date'];
                $check_out->start_time = $details[$i]['start_time'];
                $check_out->end_time = $details[$i]['end_time'];
                $check_out->total = $details[$i]['total'];
                $check_out->final_amount = $amount;
                $check_out->quantity = $details[$i]['Quantity'];
                $check_out->payment_id = $mpesa_id;
                $check_out->save();
            }

            $notification = new \Uzyma\Notifications();
            $notification->title = "Check out!";
            $notification->message = "Paid for services on Uzyma worth " . $amount;
            $notification->notifierid = "0";
            $notification->notifieeid = $user_id->id;
            $notification->created_at = \Carbon\Carbon::today()->format('Y-m-d');
            $notification->save();

            $transaction = new Transactions();
            $transaction->userid = $user_id->id;
            $transaction->transaction = "Account debited";
            $transaction->amount = $amount;
            $transaction->created_at = $date_now;
            $transaction->save();
            $response["result"] = "success";
            $response["message"] = "You have checked out successfully";
        } else {
            $response["result"] = "fail";
            $response["message"] = "Woops! insufficients funds";
        }

        echo json_encode($response);

    }


    public function getActivityUser(Request $request)
    {
        $app_user_id = $request->input('firebase_id');
        $date = $request->input('date');
        $user_id = \Uzyma\AppUser::where('firebase_id', $app_user_id)->first();
        $activities = \Uzyma\Checkout::leftJoin('activitys', 'checkout.activity_id', '=', 'activitys.id')
            ->leftJoin('partners', 'activitys.user_id', '=', 'partners.user_id')
            ->leftJoin('business_type', 'activitys.service_id', '=', 'business_type.id')
            ->leftJoin('difficulty', 'activitys.difficult_level', '=', 'difficulty.id')
            ->select('activitys.id as activityID','activitys.service_id', 'activitys.tags', 'activitys.activity_amen as amenities', 'activitys.price', 'activitys.cancelationdate as cancelationDate', 'difficulty.level_name as difficulty_level', 'activitys.activity_name', 'activitys.activity_description', 'partners.partner_name as partnerName', 'business_type.name as serviceName', 'activitys.image_url', 'activitys.date', 'activitys.time', 'activitys.latitude', 'activitys.longitude', 'activitys.address')
            ->where('checkout.app_user_id', $user_id->id)
            ->where('checkout.date', $date)
            ->get();
        $response["result"] = "success";
        $response["message"] = "Activities retrieved successfully";
        $response["activities"] = $activities;
        echo json_encode($response);
    }

    public function getActivityUserddd(Request $request)
    {
        echo "Bruh";
        exit();
    }

    public function filter(Request $request)
    {
        $maxprice = $request->input('maxprice');
        $minprice = $request->input('minprice');

        $activities = \Uzyma\Activities::where('activitys.status', 1)
            ->leftJoin('partners', 'activitys.user_id', '=', 'partners.user_id')
            ->leftJoin('business_type', 'activitys.service_id', '=', 'business_type.id')
            ->leftJoin('difficulty', 'activitys.difficult_level', '=', 'difficulty.id')
            ->select('activitys.id as activityID', 'activitys.price', 'activitys.tags', 'activitys.activity_amen as amenities', 'activitys.cancelationdate as cancelationDate', 'difficulty.level_name as difficulty_level', 'activitys.activity_name', 'activitys.activity_description', 'partners.partner_name as partnerName', 'business_type.name as serviceName', 'activitys.image_url', 'activitys.date', 'activitys.time', 'activitys.latitude', 'activitys.longitude', 'activitys.address')
            ->whereBetween('activitys.price', [$minprice, $maxprice])->get();
        $response["result"] = "success";
        $response["activities"] = $activities;
        echo json_encode($response);
    }

    public function filterTime(Request $request)
    {
        $maxtime = $request->input('maxtime');
        $mintime = $request->input('mintime');
        $time_max = date("H:i", strtotime($maxtime));
        $time_min = date("H:i", strtotime($mintime));

        $activities = \Uzyma\Activities::where('activitys.status', 1)
            ->leftJoin('partners', 'activitys.user_id', '=', 'partners.user_id')
            ->leftJoin('business_type', 'activitys.service_id', '=', 'business_type.id')
            ->leftJoin('difficulty', 'activitys.difficult_level', '=', 'difficulty.id')
            ->select('activitys.id as activityID', 'activitys.price', 'activitys.tags', 'activitys.activity_amen as amenities', 'activitys.cancelationdate as cancelationDate', 'difficulty.level_name as difficulty_level', 'activitys.activity_name', 'activitys.activity_description', 'partners.partner_name as partnerName', 'business_type.name as serviceName', 'activitys.image_url', 'activitys.date', 'activitys.time', 'activitys.latitude', 'activitys.longitude', 'activitys.address')
            ->whereBetween('activitys.time', [$time_min, $time_max])->get();
        $response["result"] = "success";
        $response["activities"] = $activities;
        echo json_encode($response);
    }

    public function filterSchedule(Request $request)
    {
        $firebaseId = $request->input('firebase_id');
        $date = $request->input('date');
        $user_id = \Uzyma\AppUser::where('firebase_id', $firebaseId)->first();
        $checkSchedule = \Uzyma\Checkout::leftJoin('activitys', 'checkout.activity_id', '=', 'activitys.id')
            ->leftJoin('partners', 'activitys.user_id', '=', 'partners.user_id')
            ->leftJoin('business_type', 'activitys.service_id', '=', 'business_type.id')
            ->leftJoin('difficulty', 'activitys.difficult_level', '=', 'difficulty.id')
            ->select('activitys.id as activityID', 'activitys.tags', 'activitys.activity_amen as amenities', 'activitys.price', 'activitys.cancelationdate as cancelationDate', 'difficulty.level_name as difficulty_level', 'activitys.activity_name', 'activitys.activity_description', 'partners.partner_name as partnerName', 'business_type.name as serviceName', 'activitys.image_url', 'activitys.date', 'activitys.time', 'activitys.latitude', 'activitys.longitude', 'activitys.address')
            ->where('activitys.date', $date)
            ->where('checkout.app_user_id', $user_id->id)
            ->get();
        $response["result"] = "success";
        $response["message"] = "Activities retrieved successfully";
        $response["activities"] = $checkSchedule;
        echo json_encode($response);
    }

    public function slider()
    {
        $slider = \Uzyma\Slider::select('slider_name', 'image_url', 'id')->get();
        $response["result"] = "success";
        $response["slider"] = $slider;
        echo json_encode($response);
    }

    //Get product images
    public function getProductImages(Request $request)
    {
        $product_id = $request->input('id');
        $productimages = \Uzyma\ProductsImages::select('image_url')->where('activity_id', $product_id)->get();
        if (count($productimages)) {
            $response["images"] = $productimages;
        } else {
            $productsimage2 = \Uzyma\Activities::select('image_url')->where('id', $product_id)->get();
            $response["images"] = $productsimage2;
        }
        $response["result"] = "success";

        echo json_encode($response);
    }

    public function getProducts()
    {
        $products = \Uzyma\Products::get();
        $response["result"] = "success";
        $response["products"] = $products;
        echo json_encode($response);
    }

    public function cart(Request $request)
    {

    }

    public function helpCentre(Request $request)
    {
        $help = new HelpCentre();
        $help->title = $request->input('title');
        $help->description = $request->input('description');
        $help->save();
        $response["result"] = "success";
        echo json_encode($response);
    }

    public function helpCentre2()
    {
        $help = \Uzyma\HelpCentreSet::get();
        $response["result"] = "success";
        $response["help"] = $help;
        echo json_encode($response);
    }

    public function transactions(Request $request)
    {
        $app_user_id = $request->input('firebase_id');
        $user_id = \Uzyma\AppUser::where('firebase_id', $app_user_id)->first();
        $activity = \Uzyma\Transactions::where('userid', $user_id->id)->get();
        $bal_activity = \Uzyma\Wallet::where('userid', $user_id->id)->first();
        $response["result"] = "success";
        $response["wallet"] = $activity;
        $response["balance"] = $bal_activity->amount;
        echo json_encode($response);
    }


    public function savePayment(Request $request)
    {
        $transactionId = $request->input("transactionId");
        $transactionPhoneNumber = $request->input("transactionPhoneNumber");
        $transactionAmount = $request->input("transactionAmount");
        $transactionMpesaId = $request->input("transactionMpesaId");
        $transactionStatus = $request->input("transactionStatus");

        $mp = new mobilepayments;
        $mp->transactionID = $transactionId;
        $mp->transactionPhoneNumber = $transactionPhoneNumber;
        $mp->transactionAmount = $transactionAmount;
        $mp->transactionDate = Carbon::today()->format('Y-m-d');
        $mp->transactionMpesaID = $transactionMpesaId;
        $mp->transactionStatus = $transactionStatus;
        $mp->created_at = Carbon::today()->format('Y-m-d');
        $mp->save();

        $mpexists = mobilepayments::where('transactionID', $transactionId)->count();
        if ($mpexists == 1) {
            echo false;
        } else {
            echo true;
        }
    }

    public function getShades(Request $request){
        $shades = \Uzyma\Other::where('activity_id',$request->input('activity_id'))->get();
        $response['result'] = "success";
        $response['message'] = "Shades retrieved successfully";
        $response['shades'] = $shades;
        return response()->json($response);
    }

    public function Rate(Request $request){
        $firebase_id = $request->input('firebase_id');
        $rat_e = $request->input('rate');
        $activity_id = $request->input('activity_id');
        $user = \Uzyma\AppUser::where('firebase_id',$firebase_id)->first();
        $rate = new \Uzyma\Rate;
        $rate->rate = $rat_e;
        $rate->activity_id = $activity_id;
        $rate->firebase_id = $user->id;
        $rate->save();
        $response['result'] = "success";
        $response['message'] = "Rate retrieved successfully";
        $response['rate'] = $rate;
        return response()->json($response);
    }

    public function getRate(Request $request){
        $activity_id = $request->input('activity_id');
        $rate_value = \Uzyma\Rate::where('activity_id', $activity_id)->get();
        $final_rate = 0;
        $size_of_array = sizeof($rate_value);
        foreach ($rate_value as $row){
            $final_rate = $final_rate + $row->rate;
        }
        $totalRate = $final_rate / $size_of_array;
        $response['result'] = "success";
        $response['message'] = "Rate retrieved successfully";
        $response['total_rate'] = $totalRate;
        return response()->json($response);
    }


    /********************************** Business *************************************************/

    public function signUpStylist(Request $request)
    {
        $firebase_id = $request->input('firebase_id');
        $name = $request->input('name');
        $email = $request->input('email');
        $terms = $request->input('terms');
        $phone = $request->input('phone');
        $image = $request->input('image');
        $emailExists = \Uzyma\Stylist::where('email', $request->input('email'))
            ->first();

        if ($emailExists) {
            $response["result"] = "success";
            $response["message"] = "User Data has synced successfully";
            $response["name"] = $emailExists->name;
            $response["email"] = $emailExists->email;
            $response["firebase_id"] = $emailExists->firebase_id;
            $response["terms"] = $emailExists->terms;
            $response["phone"] = $emailExists->phone_number;
            $response["image"] = $emailExists->image;
            $response["created_at"] = $emailExists->created_at;
            echo json_encode($response);
        } else {
            $myuser = new \Uzyma\Stylist();
            $myuser->name = $name;
            $myuser->email = $email;
            $myuser->terms = $terms;
            $myuser->image = $image;
            $myuser->phone_number = $phone;
            $myuser->firebase_id = $firebase_id;
            $saved = $myuser->save();
            if ($saved) { //updating
                $response["result"] = "success";
                $response["message"] = "User Data has synced successfully";
                $response["name"] = $myuser->name;
                $response["email"] = $myuser->email;
                $response["firebase_id"] = $myuser->firebase_id;
                $response["phone"] = $myuser->phone_number;
                $response["image"] = $myuser->image;
                $response["terms"] = $myuser->terms;
                $response["created_at"] = $myuser->created_at;
                echo json_encode($response);
            } else {
                $response["result"] = "failure";
                $response["message"] = "Error saving data";
                echo json_encode($response);
            }
        }
    }


    public function updateUserStylist(Request $request)
    {
        $name = $request->input('name');
        $email = $request->input('email');
        $terms = $request->input('terms');
        $firebase_id = $request->input('firebase_id');
        $phone = $request->input('phone');
        $appuser = \Uzyma\Stylist::where('firebase_id', $firebase_id)->first();
        $appuser->email = $email;
        $appuser->name = $name;
        $appuser->terms = $terms;
        $appuser->phone_number = $phone;
        $appuser->save();
        if ($appuser->id) {
            $response["result"] = "success";
            $response["message"] = "Profile Updated Successfully";
            $response["name"] = $appuser->name;
            $response["email"] = $appuser->email;
            $response["firebase_id"] = $appuser->firebase_id;
            $response["image"] = $appuser->image;
            $response["terms"] = $appuser->terms;
            $response["created_at"] = $appuser->created_at;
            $response["phone"] = $appuser->phone_number;
            echo json_encode($response);
        }
    }


    public function updateProfilePicStylist(Request $request)
    {
        $firebase_id = $request->input('firebase_id');
        $appuser = \Uzyma\Stylist::where('firebase_id', $firebase_id)->first();
        \File::delete('"' . $appuser->image . '"');
        $destinationPath = 'packages/uploads/stylist/' . \Carbon\Carbon::now()->year . '/' . \Carbon\Carbon::now()->month;
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }

        $filename = \Carbon\Carbon::now()->year . '_' . \Carbon\Carbon::now()->month . '_' . \Carbon\Carbon::now()->day . '_' . time() . '.png';
        $base64 = $request->input('file');

        // This saves the base64encoded destinationPath
        $upload_success = file_put_contents($destinationPath . '/' . $filename, base64_decode($base64));
        if ($upload_success) {
            $appuser->image = url('/') . '/' . $destinationPath . '/' . $filename;
            $appuser->save();
            $response["result"] = "success";
            $response["message"] = "Profile Picture Updated Successfully";
            $response["name"] = $appuser->name;
            $response["email"] = $appuser->email;
            $response["firebase_id"] = $appuser->firebase_id;
            $response["phone"] = $appuser->phone_number;
            $response["image"] = $appuser->image;
            $response["terms"] = $appuser->terms;
            $response["created_at"] = $appuser->created_at;
            echo json_encode($response);
        } else {
            $response["result"] = "failure";
            $response["message"] = "Error saving Image";
            echo json_encode($response);
        }
    }


    public function signInStylist(Request $request)
    {

        $email = $request->input('email');
        $password = $request->input('password');
        // $emailExists = User::where('email', $email)
        // // ->where('password', bcrypt($password))
        // ->first();

        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            if (Auth::user()->account_type === 444) {
                $response["result"] = "success";
                $response["message"] = "User Data has synced successfully";
                $response["name"] = Auth::user()->name;
                $response["email"] = Auth::user()->email;
                $response["manager_id"] = Auth::user()->id;
                echo json_encode($response);
            } else {
                $response["result"] = "success";
                $response["message"] = "User is not a manager";
                echo json_encode($response);
            }

        } else {

            $response["result"] = "failure";
            $response["message"] = "Error: email and password do not match";
            echo json_encode($response);
        }
    }


    public function customersAddEdit(Request $request)
    {
        $manager_id = $request->input('manager_id');
        // $user = $this->getStylist($firebase_id);
        $business = \Uzyma\Manager::where('users_id', $manager_id)->first();

        $cutomers_check = \Uzyma\Customers::where('phone')->first();
        if (count($cutomers_check)) {
            $customersID = $cutomers_check->id;
        } else {
            $customers = new \Uzyma\Customers;
            $customers->name = $request->input('client_name');
            $customers->phone = $request->input('client_phone');
            $customers->email = $request->input('client_email');
            $customers->business_id = $business->business_id;
            $customers->save();
            $customersID = $customers->id;
        }
        $datepost = \Carbon\Carbon::now('Africa/Nairobi')->format('Y-m-d');

        $business_activity = new \Uzyma\BusinessActivity;
        $business_activity->customer_id = $customersID;
        $business_activity->business_id = $business->business_id;
        $business_activity->activity_date = $datepost;
        $business_activity->style = $request->input('style');
        $business_activity->cost = $request->input('cost');
        $business_activity->stylist_id = $request->input('stylist_id');
        $business_activity->type_client = $request->input('type_client');
        $business_activity->save();

        $data = array('name' => $request->input('client_name'), 'client_email' => $request->input('client_email'));


        \Mail::send('emails.uzymamail', $data, function ($message) use ($data) {
            $message->to($data['client_email'])->subject('Service Receipt')->from(getcong('site_email'));
        });

        if ($business_activity) {
            $response["result"] = "success";
            $response["message"] = "Client saved successfully";

        } else {
            $response["result"] = "failure";
            $response["message"] = "Error saving Client data";

        }
        echo json_encode($response);
    }

    public function getStylist(Request $request)
    {
        $manager_id = $request->input('manager_id');
        $business = \Uzyma\Manager::where('users_id', $manager_id)->first();
        $stylist = \Uzyma\Stylist::where('business_id', $business->business_id)->select('name', 'id')->get();
        $response["result"] = "success";
        $response["stylist"] = $stylist;
        $response["message"] = "data synced successfully";
        echo json_encode($response);
    }

    public function stylistActivity(Request $request)
    {
        $manager_id = $request->input('manager_id');
        $business = \Uzyma\Manager::where('users_id', $manager_id)->first();
        //$user = $this->getStylist($firebase_id);
        $stylist_data = \Uzyma\BusinessActivity::leftJoin('customers', 'business_activity.customer_id', '=', 'customers.id')
            ->leftJoin('stylist', 'business_activity.stylist_id', '=', 'stylist.id')
            ->leftJoin('customer_type', 'business_activity.type_client', '=', 'customer_type.id')
            ->where('business_activity.business_id', $business->business_id)
            ->select('customers.email', 'customers.phone', 'customers.name as customerName', 'customers.id as customerId', 'business_activity.style', 'business_activity.cost', 'business_activity.activity_date', 'stylist.name', 'customer_type.type')
            ->get();

        $cash = \Uzyma\BusinessActivity::where('business_id', $business->business_id)->sum('cost');
        $response["cash_all"] = $cash;

        $datepost = \Carbon\Carbon::now('Africa/Nairobi')->format('Y-m-d');
        $cash = \Uzyma\BusinessActivity::where('activity_date', $datepost)->where('business_id', $business->business_id)->sum('cost');
        $response["cash_today"] = $cash;

        $referals = \Uzyma\BusinessActivity::where('business_id', $business->business_id)->where('type_client', 1)->count();
        $response["referals"] = $referals;
        $notreferals = \Uzyma\BusinessActivity::where('business_id', $business->business_id)->where('type_client', 0)->count();
        $response["notreferals"] = $notreferals;

        if ($stylist_data) {
            $response["result"] = "success";
            $response["activity"] = $stylist_data;
            $response["message"] = "data synced successfully";
        } else {
            $response["result"] = "failure";
            $response["message"] = "failure while syncing data";
        }
        echo json_encode($response);
    }

    public function getMoney(Request $request)
    {
        $manager_id = $request->input('manager_id');
        $business = \Uzyma\Manager::where('users_id', $manager_id)->first();
        $cash = \Uzyma\BusinessActivity::where('business_id', $business->business_id)->sum('cost');
        $response["result"] = "success";
        $response["cash_all"] = $cash;
        $response["message"] = "data synced successfully";
        echo json_encode($response);
    }

    public function getMoneyToday(Request $request)
    {
        $manager_id = $request->input('manager_id');
        $business = \Uzyma\Manager::where('users_id', $manager_id)->first();
        $datepost = \Carbon\Carbon::now('Africa/Nairobi')->format('Y-m-d');
        $cash = \Uzyma\BusinessActivity::where('activity_date', $datepost)->where('business_id', $business->business_id)->sum('cost');
        $response["result"] = "success";
        $response["cash_today"] = $cash;
        $response["message"] = "data synced successfully";
        echo json_encode($response);
    }

    public function customerTypes()
    {
        $customer_type = CustomerType::get();
        $response["result"] = "success";
        $response["customer_type"] = $customer_type;
        $response["message"] = "data synced successfully";
        echo json_encode($response);
    }

    public function searchCustomer(Request $request)
    {
//        $business = \Uzyma\Manager::where('users_id', $manager_id)->first();
        $value = $request->input('name');
        $customer = \Uzyma\Customers::where('name', 'LIKE', '%' . $value . '%')->get();
        $response["result"] = "success";
        $response["customers"] = $customer;
        $response["message"] = "data synced successfully";
        echo json_encode($response);
    }


}
