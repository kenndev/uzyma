<?php

namespace Uzyma\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin {

    public function handle($request, Closure $next, $guard = null) {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        } else if (Auth::guard($guard)->user()->account_type != 221) {
            \Session::flash('flash_message_error', 'Permission Denied. You are not authorised to view this page');
            return back();
        }

        return $next($request);
    }

}
