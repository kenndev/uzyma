<?php

namespace Uzyma;

use Illuminate\Database\Eloquent\Model;

class HelpCentreSet extends Model
{
    protected $fillable = [];
    protected $table = 'help_centre_set';
}
