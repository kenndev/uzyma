<?php

namespace Uzyma;

use Illuminate\Database\Eloquent\Model;

class ProductsImagesReloaded extends Model
{
    protected $fillable = [];
    protected $table = 'product_images';
}
