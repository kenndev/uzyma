<?php

namespace Uzyma;

use Illuminate\Database\Eloquent\Model;

class MobilePayments extends Model
{
    protected $table = 'mobile_payments';

    protected $fillable = ['id', 'transactionID', 'transactionPhoneNumber', 'transactionAmount', 'transactionDate', 'transactionMpesaID', 'transactionStatus', 'confirmStatus', 'created_at',];

	public $timestamps = false;
}
