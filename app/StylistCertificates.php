<?php

namespace Uzyma;

use Illuminate\Database\Eloquent\Model;

class StylistCertificates extends Model
{
    protected $fillable = [];
    protected $table = 'stylist_certificates';
}
