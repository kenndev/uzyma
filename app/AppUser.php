<?php

namespace Uzyma;

use Illuminate\Database\Eloquent\Model;

class AppUser extends Model {

    protected $fillable = [];
    protected $table = 'app_users';

}
