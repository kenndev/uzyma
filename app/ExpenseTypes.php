<?php

namespace Uzyma;

use Illuminate\Database\Eloquent\Model;

class ExpenseTypes extends Model
{
    protected $fillable = [];
    protected $table = 'expense_type';
}
