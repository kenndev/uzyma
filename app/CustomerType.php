<?php

namespace Uzyma;

use Illuminate\Database\Eloquent\Model;

class CustomerType extends Model
{
    protected $fillable = [];
    protected $table = 'customer_type';
}
