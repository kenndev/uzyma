<?php

namespace Uzyma;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $fillable = [];
    protected $table = 'comments';
}
