<?php

namespace Uzyma;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable = [];
    protected $table = 'cart';
}
