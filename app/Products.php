<?php

namespace Uzyma;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $fillable = [];
    protected $table = 'products';
}
