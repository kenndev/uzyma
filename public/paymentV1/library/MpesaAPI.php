<?php

//This files handles all the SOAP Request and Responses
ini_set("soap.wsdl_cache_enabled", "0");

class MpesaApi
{

	public function processCheckOutRequest($password,$MERCHANT_ID,$MERCHANT_TRANSACTION_ID,$REFERENCE_ID,$AMOUNT,$MSISDN,$CALL_BACK_URL){
		$TIMESTAMP=new DateTime();
		$datetime=$TIMESTAMP->format('YmdHis');
		
		$post_string='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tns="tns:ns">
		<soapenv:Header>
		  <tns:CheckOutHeader>
			<MERCHANT_ID>'.$MERCHANT_ID.'</MERCHANT_ID>
			<PASSWORD>'.$password.'</PASSWORD>
			<TIMESTAMP>'.$datetime.'</TIMESTAMP>
		  </tns:CheckOutHeader>
		</soapenv:Header>
		<soapenv:Body>
		  <tns:processCheckOutRequest>
			<MERCHANT_TRANSACTION_ID>'.$MERCHANT_TRANSACTION_ID.'</MERCHANT_TRANSACTION_ID>
			<REFERENCE_ID>'.$REFERENCE_ID.'</REFERENCE_ID>
			<AMOUNT>'.$AMOUNT.'</AMOUNT>
			<MSISDN>'.$MSISDN.'</MSISDN>
			<!--Optional parameters-->
			<CALL_BACK_URL>'.$CALL_BACK_URL.'</CALL_BACK_URL>
			<CALL_BACK_METHOD>POST</CALL_BACK_METHOD>
			<TIMESTAMP>'.$datetime.'</TIMESTAMP>
		  </tns:processCheckOutRequest>
		</soapenv:Body>
		</soapenv:Envelope>';
		/*
		Headers
		 */
		$headers = array(  
		"Content-type: text/xml",
		"Content-length: ".strlen($post_string),
		"Content-transfer-encoding: text",
		"SOAPAction: \"processCheckOutRequest\"",
		);
		/*
		To get the feedback from the process request system
		For debug purposes only
		 */
		$response=$this->submitRequest(URL,$post_string,$headers);
		//echo $response;
		/*
		To get the feedback from the process transaction system
		For debug purposes only
		 */
		$this->confirmTransaction($response,$datetime,$password,$MERCHANT_ID);
	}

	/*
	The Merchant makes a SOAP call to the SAG to confirm an online checkout transaction
	 */
	public function confirmTransaction($checkoutResponse,$datetime,$password,$MERCHANT_ID){
		$xml = simplexml_load_string($checkoutResponse);
		$ns = $xml->getNamespaces(true);
		$soap = $xml->children($ns['SOAP-ENV']);
		$sbody = $soap->Body;
		$mpesa_response = $sbody->children($ns['ns1']);
		$rstatus = $mpesa_response->processCheckOutResponse;
		$status = $rstatus->children();		
		$s_returncode = $status->RETURN_CODE;
		$s_description = $status->DESCRIPTION;
		$s_transactionid = $status->TRX_ID;
		$s_enryptionparams = $status->ENC_PARAMS;
		$s_customer_message = $status->CUST_MSG;
		if($s_returncode==42){

			return json_encode("Authentication Failed",401);
		}
		$confirmTransactionResponse='
			<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tns="tns:ns">
		   <soapenv:Header>
		      <tns:CheckOutHeader>
		         <MERCHANT_ID>'.$MERCHANT_ID.'</MERCHANT_ID>
			<PASSWORD>'.$password.'</PASSWORD>
			<TIMESTAMP>'.$datetime.'</TIMESTAMP>
		      </tns:CheckOutHeader>
		   </soapenv:Header>
		   <soapenv:Body>
		      <tns:transactionConfirmRequest>
		         <!--Optional:-->
		         <TRX_ID>'.$s_transactionid.'</TRX_ID>
		         <!--Optional:-->
		         
		      </tns:transactionConfirmRequest>
		   </soapenv:Body>
		</soapenv:Envelope>';

		$headers = array(  
		"Content-type: text/xml",
		"Content-length: ".strlen($confirmTransactionResponse),
		"Content-transfer-encoding: text",
		"SOAPAction: \"transactionConfirmRequest\"",
		);

		//Do whatever you want with the data. You can as well pass it as Xml data
		$result= $this->submitRequest(URL,$confirmTransactionResponse,$headers);
		
        echo $result.":To complete this transaction, enter your Service PIN on the Pop-Up Message that shows shortly on the Phone Number provided. If you don't have a Service PIN, press 0, Enter your ID Number and follow the instructions.";
    }

	public function statusRequest($Password,$MERCHANT_ID,$TXID,$MERCHANT_TRANSACTION_ID){
		$TIMESTAMP=new DateTime();
		$datetime=$TIMESTAMP->format('YmdHis');

		$post_string='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tns="tns:ns">
					   	<soapenv:Header>
					      	<tns:CheckOutHeader>
					           	<MERCHANT_ID>'.$MERCHANT_ID.'</MERCHANT_ID>
								<PASSWORD>'.$Password.'</PASSWORD>
								<TIMESTAMP>'.$datetime.'</TIMESTAMP>
					      	</tns:CheckOutHeader>
					   	</soapenv:Header>
					   	<soapenv:Body>
					      	<tns:transactionStatusRequest>
					         	<!--Optional:-->
					         		<TRX_ID>'.$TXID.'</TRX_ID>
					         	<!--Optional:-->
					         	<MERCHANT_TRANSACTION_ID>'.$MERCHANT_TRANSACTION_ID.'</MERCHANT_TRANSACTION_ID>
					      	</tns:transactionStatusRequest>
					   	</soapenv:Body>
					</soapenv:Envelope>';

		$headers = array(  
		"Content-type: text/xml",
		"Content-length: ".strlen($post_string),
		"Content-transfer-encoding: text",
		"SOAPAction: \"transactionStatusRequest\"",
		);

		echo $this->submitRequest(URL,$post_string,$headers);
	}

	function submitRequest($url,$post_string,$headers){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_POST,TRUE); 
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS,  $post_string); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		
		$data = curl_exec($ch);
		if($data === FALSE)
		{
			$err = 'Curl error: ' . curl_error($ch);
			curl_close($ch);
			echo "Error \n".$err;
		}
		else
		{
			curl_close($ch);
			$body = $data;
			
		}
		return $body;
	}

	//Mpesa Library
  //   public function processCheckOutRequest($password,$MERCHANT_ID,$MERCHANT_TRANSACTION_ID,$REFERENCE_ID,$AMOUNT,$MSISDN,$CALL_BACK_URL){
  //       $TIMESTAMP=new DateTime();
		// $datetime=$TIMESTAMP->format('YmdHis');
        
  //       $post_string='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tns="tns:ns">
  //       <soapenv:Header>
  //         <tns:CheckOutHeader>
  //           <MERCHANT_ID>'.$MERCHANT_ID.'</MERCHANT_ID>
  //           <PASSWORD>'.$password.'</PASSWORD>
  //           <TIMESTAMP>'.$TIMESTAMP.'</TIMESTAMP>
  //         </tns:CheckOutHeader>
  //       </soapenv:Header>
  //       <soapenv:Body>
  //         <tns:processCheckOutRequest>
  //           <MERCHANT_TRANSACTION_ID>'.$MERCHANT_TRANSACTION_ID.'</MERCHANT_TRANSACTION_ID>
  //           <REFERENCE_ID>LipaTap Services </REFERENCE_ID>
  //           <AMOUNT>'.$AMOUNT.'</AMOUNT>
  //           <MSISDN>'.$MSISDN.'</MSISDN>
  //           <!--Optional parameters-->
  //           <CALL_BACK_URL>'.$CALL_BACK_URL.'</CALL_BACK_URL>
  //           <CALL_BACK_METHOD>xml</CALL_BACK_METHOD>
  //           <TIMESTAMP>'.$TIMESTAMP.'</TIMESTAMP>
  //         </tns:processCheckOutRequest>
  //       </soapenv:Body>
  //       </soapenv:Envelope>';
  //       /*
  //       Headers
  //        */
  //       $headers = array(  
  //       "Content-type: text/xml",
  //       "Content-length: ".strlen($post_string),
  //       "Content-transfer-encoding: text",
  //       "SOAPAction: \"processCheckOutRequest\"",
  //       );
  //       /*
  //       To get the feedback from the process request system
  //       For debug purposes only
  //        */
  //       $output=$this->submitRequest(URL,$post_string,$headers);
  //       dd($output);
  //       //Call method to extract response from safaricom
  //       $requestCheckout_response = $this->extract_requestCheckout_response($output);
  //       $response_data=array();
        
  //       if($requestCheckout_response['return_code']=='00'){
  //           $response="";
  //           $raw_response2 = $this->confirmTransaction($output,$TIMESTAMP,$password,$MERCHANT_ID);
  //           $response2 = $this->extract_processCheckout_response($raw_response2);
  //           $response_data['trx_id']=$response2['trx_id'];
  //           $response_data['message'] = "";
  //           //print_r($response2);
  //       }

  //       //ERROR OCCURED WHILE PROCESSING THE REQUEST 
  //       else{
  //           $response = "Error While processing your request \n Description : ".$requestCheckout_response['description'] ;
            
  //           $response_data['trx_id']="";
  //           $response_data['message'] = $response;
  //       }
  //       return $response_data;
  //   }
    /*
    The Merchant makes a SOAP call to the SAG to confirm an online checkout transaction
     */
//     public function confirmTransaction($checkoutResponse,$datetime,$password,$MERCHANT_ID){     
//         $xml = simplexml_load_string($checkoutResponse);
//         $ns = $xml->getNamespaces(true);
//         $soap = $xml->children($ns['SOAP-ENV']);
//         $sbody = $soap->Body;
//         $mpesa_response = $sbody->children($ns['ns1']);
//         $rstatus = $mpesa_response->processCheckOutResponse;
//         $status = $rstatus->children();     
//         $s_returncode = $status->RETURN_CODE;
//         $s_description = $status->DESCRIPTION;
//         $s_transactionid = $status->TRX_ID;
//         $s_enryptionparams = $status->ENC_PARAMS;
//         $s_customer_message = $status->CUST_MSG;
//         if($s_returncode==42){
//             return json_encode("Authentication Failed",401);
//         }
//         $confirmTransactionResponse='
//             <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tns="tns:ns">
//            <soapenv:Header>
//               <tns:CheckOutHeader>
//                  <MERCHANT_ID>'.$MERCHANT_ID.'</MERCHANT_ID>
//             <PASSWORD>'.$password.'</PASSWORD>
//             <TIMESTAMP>'.$TIMESTAMP.'</TIMESTAMP>
//               </tns:CheckOutHeader>
//            </soapenv:Header>
//            <soapenv:Body>
//               <tns:transactionConfirmRequest>
//                  <!--Optional:-->
//                  <TRX_ID>'.$s_transactionid.'</TRX_ID>
//                  <!--Optional:-->
                 
//               </tns:transactionConfirmRequest>
//            </soapenv:Body>
//         </soapenv:Envelope>';

//         $headers = array(  
//         "Content-type: text/xml",
//         "Content-length: ".strlen($confirmTransactionResponse),
//         "Content-transfer-encoding: text",
//         "SOAPAction: \"transactionConfirmRequest\"",
//         );

//         //Do whatever you want with the data. You can as well pass it as Xml data
//         return $this->submitRequest(URL,$confirmTransactionResponse,$headers);
                
        
//     }

//     public function statusRequest($Password,$MERCHANT_ID,$TXID,$MERCHANT_TRANSACTION_ID){
//         $TIMESTAMP=new DateTime();
//         $datetime=TIMESTAMP;

//         $post_string='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tns="tns:ns">
//                        <soapenv:Header>
//                           <tns:CheckOutHeader>
//                                <MERCHANT_ID>'.$MERCHANT_ID.'</MERCHANT_ID>
//                         <PASSWORD>'.$Password.'</PASSWORD>
//                         <TIMESTAMP>'.$TIMESTAMP.'</TIMESTAMP>
//                           </tns:CheckOutHeader>
//                        </soapenv:Header>
//                        <soapenv:Body>
//                           <tns:transactionStatusRequest>
//                              <!--Optional:-->
//                              <TRX_ID>'.$TXID.'</TRX_ID>
//                              <!--Optional:-->
//                              <MERCHANT_TRANSACTION_ID>'.$MERCHANT_TRANSACTION_ID.'</MERCHANT_TRANSACTION_ID>
//                           </tns:transactionStatusRequest>
//                        </soapenv:Body>
//                     </soapenv:Envelope>';

//         $headers = array(  
//         "Content-type: text/xml",
//         "Content-length: ".strlen($post_string),
//         "Content-transfer-encoding: text",
//         "SOAPAction: \"transactionStatusRequest\"",
//         );
                
//         $checkoutResponse = $this->submitRequest(URL,$post_string,$headers);
//         dd($checkoutResponse);
//         //return $this->extract_status_request($checkoutResponse);
// //                return $checkoutResponse;
//     }

//     function submitRequest($url,$post_string,$headers){
//         $ch = curl_init();
//         curl_setopt($ch, CURLOPT_URL,$url);
//         curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
//         curl_setopt($ch, CURLOPT_TIMEOUT, 10);
//         curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
//         curl_setopt($ch, CURLOPT_POST,TRUE); 
//         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
//         curl_setopt($ch, CURLOPT_POSTFIELDS,  $post_string); 
//         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//         curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//                 curl_setopt($ch, CURLOPT_TIMEOUT, 500000); // the timeout in seconds
        
//         $data = curl_exec($ch);
//         $body = "";
//         if($data === FALSE)
//         {
//             $err = 'Curl error: ' . curl_error($ch);
//             curl_close($ch);
//             echo "Error \n".$err;
//         }
//         else
//         {
//             curl_close($ch);
//             $body = $data;
            
//         }
//         return $body;
// }
// public function getPasword(){
//     $datetime = Carbon::now()->toDateTimeString();
//     //$datetime=$TIMESTAMP->format('YmdHis');
//     $Password=base64_encode(hash("sha256", MERCHANT_ID.PASSKEY.$datetime));
//     return $Password;
// }

// public function generateRandomString() {

//         $length = 10;

//         $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

//         $charactersLength = strlen($characters);

//         $randomString = '';

//         for ($i = 0; $i < $length; $i++) {

//             $randomString .= $characters[rand(0, $charactersLength - 1)];

//         }

//         return $randomString;

//     }
    
//     //METHOD TO EXTRACT RESPONSE ON MPESA CHECKOUT REQUEST. MEANT TO ENSURE THE AMOUNT IS EQUAL OR GRETTER THAN MINIMUM SET
//     public function extract_requestCheckout_response($output)
//     {
//         $xml = simplexml_load_string($output);
//         $ns = $xml->getNamespaces(true);
//         $soap = $xml->children($ns['SOAP-ENV']);
//         $sbody = $soap->Body;
//         $details = $sbody->children($ns['ns1'])->processCheckOutResponse;
//         $details = $details->children();
//         $RETURN_CODE = $details->RETURN_CODE;
//         $return_code = $RETURN_CODE[0];
//         $DESCRIPTION = $details->DESCRIPTION;
//         $description = $DESCRIPTION[0];
//         $TRX_ID = $details->TRX_ID;
//         $trx_id = $TRX_ID[0];

//         $response=array();
//         $response['return_code']=$return_code;
//         $response['description'] = $description;
//         $response['trx_id']=$trx_id;
        
//         return $response;
//     }
    
//         //FUNCTION TO PROCESS CHECKOUT. THE RESPONSE IS RETURNED BEFORE A USSD MPESA POP UP ON CUSTOMER SCREEN
//     function extract_processCheckout_response($output){

//         $xml = simplexml_load_string($output);
//         $ns = $xml->getNamespaces(true);
//         $soap = $xml->children($ns['SOAP-ENV']);
//         $sbody = $soap->Body;

//         $details = $sbody->children($ns['ns1'])->transactionConfirmResponse;
//         $details = $details->children();

//         $RETURN_CODE = $details->RETURN_CODE;
//         $return_code = $RETURN_CODE[0];

//         $DESCRIPTION = $details->DESCRIPTION;
//         $description = $DESCRIPTION[0];

//         $MERCHANT_TRANSACTION_ID = $details->MERCHANT_TRANSACTION_ID;
//         $merchant_transaction_id = $MERCHANT_TRANSACTION_ID[0];

//         $TRX_ID = $details->TRX_ID;
//         $trx_id = $TRX_ID[0];

//         $response=array(
//          'return_code'=>"".$return_code,
//             'description'=>"".$description,
//             'merchant_transaction_id'=>"".$merchant_transaction_id,
//             'trx_id'=>"".$trx_id
//         );
        
//         return $response;    
//     }
    
//     public function extract_status_request($checkoutResponse){
// //        print_r($checkoutResponse);
//         $xml = simplexml_load_string($checkoutResponse);
//         $ns = $xml->getNamespaces(true);
//         $soap = $xml->children($ns['SOAP-ENV']);
//         $sbody = $soap->Body;
//         $mpesa_response = $sbody->children($ns['ns1']);
//         $rstatus = $mpesa_response->transactionStatusResponse;
//         $status = $rstatus->children(); 
//         $s_msisdn = $status->MSISDN;
//                 $s_amount = $status->AMOUNT;
//         $s_date = $status->MPESA_TRX_DATE;
//         $s_transactionid = $status->MPESA_TRX_ID;
//         $s_status = $status->TRX_STATUS;
//         $s_returncode = $status->RETURN_CODE;
//         $s_description = $status->DESCRIPTION;
//         $s_merchant_transaction_id = $status->MERCHANT_TRANSACTION_ID;
//         $s_encparams = $status->ENC_PARAMS;
//         $s_txID = $status->TRX_ID;

//                 $res_array=array();
//                 $res_array['msisdn'] = $s_msisdn;
//                 $res_array['amount'] = $s_amount;
//                 $res_array['mpesa_trx_date'] = $s_date;
//                 $res_array['mpesa_trx_id'] = $s_transactionid;
//                 $res_array['trx_status'] = $s_status;
//                 $res_array['return_code'] = $s_returncode;
//                 $res_array['description'] = $s_description;
//                 $res_array['random_number'] = $s_merchant_transaction_id;
//                 $res_array['enc_params'] = $s_encparams;
//                 $res_array['trx_id'] = $s_txID;
                
//         return $res_array;
//     }

}
?>
