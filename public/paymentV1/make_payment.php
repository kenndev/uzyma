<?php

/*Simplified MPESA API for PHP with lots and lots of comments with Explanations
*Incase you Need Extra Help feel free to tweet me '@jm_reed'
*Or drop me an email 'jamesrandom47@gmail.com'
*/

require_once('library/PayBillDetails.php');
require_once('library/MpesaAPI.php');

if (isset($_GET['transactionDescription']) && isset($_GET['transactionAmount']) && isset($_GET['transactionPhoneNumber'])) {

	$password = Constant::generateHash();
	$mClient = new MpesaAPI;

	//Function to Generate Your Own Random Unique Transaction ID. This is just your own Transaction ID
	$length = 10;
    $keyspace = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $str = '';
    $keyspaceLength = strlen($keyspace);

    for ($i = 0; $i < $length; $i++) {
        $str .= $keyspace[rand(0, $keyspaceLength - 1)];
    }

	//Function to Generate Your Own Random Unique Transaction ID. This is just your own Transaction ID and is NOT the MPESA Transaction ID. The MPESA Transaction ID will be provided by Safaricom to the CallBack url you specified
	$transactionId = $str;

	//Put the transaction details 

	$callBackUrl = "https://www.uzyma.com/paymentV1/receive_payment.php"; //Put the URL where you will Host the Receive File whihc is what Safaricom will callback once payment reaches your PayBill/Goes Through E.g http://your_domain_name/mpesa_api/receive_payment.php

	$transactionDescription = $_GET['transactionDescription']; //A short description that will show on the USSD Prompt i.e "Testing REED API"

	$transactionAmount = $_GET['transactionAmount'];  //Put The Amount you want the user to pay. Minimum is 10Ksh.

	$transactionPhoneNumber = $_GET['transactionPhoneNumber'];// Put The phone number you want payment to be made from  and it is in the format 254XXXXXXXXX i.e 254700000000

	//Process The USSD Prompt on the provided Phone Number
	$mClient->processCheckOutRequest($password,MERCHANT_ID,$transactionId,$transactionDescription,$transactionAmount,$transactionPhoneNumber,$callBackUrl);
}else{
	echo "No values";
}
?>
