//Payment Section
    public function savePayment(Request $request){
        $transactionId = input::get("transactionId");
        $transactionPhoneNumber = input::get("transactionPhoneNumber");
        $transactionAmount = input::get("transactionAmount");
        $transactionMpesaId = input::get("transactionMpesaId");
        $transactionStatus = input::get("transactionStatus");

        $mp = new mobilepayments;
        $mp->transactionID = input::get("transactionId", false);
        $mp->transactionPhoneNumber = input::get("transactionPhoneNumber", false);
        $mp->transactionAmount = input::get("transactionAmount", false);
        $mp->transactionDate = Carbon::today()->format('Y-m-d');
        $mp->transactionMpesaID = input::get("transactionMpesaId", false);
        $mp->transactionStatus = input::get("transactionStatus", false);
        $mp->created_at = Carbon::today()->format('Y-m-d');
        $mp->save();

        $mpexists = mobilepayments::where('transactionID', $transactionId)->count();
        if($mpexists == 1){
            echo false;
        }else{
            echo true;
        }
    }
