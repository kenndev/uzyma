<?php

/*Simplified MPESA API for PHP with lots and lots of comments with Explanations
*Incase you Need Extra Help feel free to tweet me '@jm_reed'
*Or drop me an email 'jamesrandom47@gmail.com'
*/

$data = $_POST;

//Get POST Contents that have been sent by Safaricom once the payment got to the PayBill
$transactionId                = $data["TRX_ID"];
$transactionPhoneNumber       = $data["MSISDN"];
$transactionAmount            = $data["AMOUNT"];
$transactionStatus            = $data["TRX_STATUS"];
$transactionDescription       = $data["DESCRIPTION"];
$transactionReturnCode        = $data["RETURN_CODE"];
$transactionDate              = $data["MPESA_TRX_DATE"];
$transactionMpesaId           = $data["MPESA_TRX_ID"];

//Save the returned data into the database or use it to finish certain operation i.e Send SMS notification or Activate App or Service
if($transactionStatus == "Success")
{
	$url = "https://www.uzyma.com/api/v1/savePayment?transactionId=".$transactionId."&transactionPhoneNumber=".$transactionPhoneNumber."&transactionAmount=".$transactionAmount."&transactionDate=".$transactionDate."&transactionMpesaId=".$transactionMpesaId."&transactionStatus=".$transactionStatus;
	//Use file_get_contents to GET the URL in question.
	$contents = file_get_contents($url);

	//If $contents is not a boolean FALSE value.
	if($contents == false){
	    //Print out the contents.
	    //Data was successfully inserted to your database. Do something here like SEND User an SMS or Activate a Service
	    echo $contents;
	}else{
		//Data was not inserted to your database.
    	//Probably a MYSQL Error. Check your database connections details
    	echo $contents;
	}
}
else
{
	$url = "https://www.uzyma.com/api/v1/savePayment?transactionId=".$transactionId."&transactionPhoneNumber=".$transactionPhoneNumber."&transactionAmount=".$transactionAmount."&transactionDate=".$transactionDate."&transactionMpesaId=".$transactionMpesaId."&transactionStatus=".$transactionStatus;
	//Use file_get_contents to GET the URL in question.
	$contents = file_get_contents($url);

	//If $contents is not a boolean FALSE value.
	if($contents == false){
	    //Print out the contents.
	    //Data was successfully inserted to your database. Do something here like SEND User an SMS or Activate a Service
	    echo $contents;
	}else{
		//Data was not inserted to your database.
    	//Probably a MYSQL Error. Check your database connections details
    	echo $contents;
	}

}

?>