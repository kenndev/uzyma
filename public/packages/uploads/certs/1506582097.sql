-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 19, 2017 at 09:08 AM
-- Server version: 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uzyma`
--

-- --------------------------------------------------------

--
-- Table structure for table `activitys`
--

CREATE TABLE `activitys` (
  `id` int(11) NOT NULL,
  `activity_name` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `activity_description` text,
  `service_id` int(11) NOT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `time` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `address` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `slug` text NOT NULL,
  `cancelationdate` varchar(255) DEFAULT NULL,
  `difficult_level` int(11) NOT NULL DEFAULT '0',
  `price` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activitys`
--

INSERT INTO `activitys` (`id`, `activity_name`, `user_id`, `activity_description`, `service_id`, `image_url`, `date`, `time`, `latitude`, `longitude`, `address`, `status`, `slug`, `cancelationdate`, `difficult_level`, `price`, `created_at`, `updated_at`) VALUES
(1, 'Hair styling', 6, 'Adventure activity', 2, 'http://162.243.72.219/packages/uploads/activity/1499326732.jpg', '2017-05-08', '08:30', '-1.285304', '36.82312300000001', 'Vedic House Building, Nairobi, Kenya', 1, 'hair-styling', '2017-05-12', 2, 2000, '2017-05-07 11:47:54', '2017-07-06 07:38:52'),
(2, 'make up design', 6, 'make up design', 1, 'http://162.243.72.219/packages/uploads/activity/1499326893.jpg', '2017-05-07', '14:00', '-1.1700055', '36.82714480000004', 'Cleanshelf Supermarket, Kiambu, Kenya', 1, 'make-up-design', '2017-05-13', 2, 4000, '2017-05-07 12:13:46', '2017-07-06 07:41:33'),
(3, 'Afro hair styling', 6, 'Afro hair styling', 2, 'http://162.243.72.219/packages/uploads/activity/1499327252.jpg', '2017-05-25', '13:00', '-1.0387569', '37.08337529999994', 'Thika, Kiambu County, Kenya', 1, 'afro-hair-styling', '2017-05-19', 2, 1000, '2017-05-08 15:54:54', '2017-07-06 07:47:32'),
(4, 'Hair and make up stlying', 6, 'Hair and make up stlying', 1, 'http://162.243.72.219/packages/uploads/activity/1499327404.jpg', '2017-05-10', '08:00', '-1.3363068', '36.77572380000004', 'Langata Road, Nairobi, Kenya', 1, 'hair-and-make-up-stlying', '2017-05-18', 1, 5000, '2017-05-08 15:58:33', '2017-07-06 07:50:04'),
(5, 'Martial arts', 6, 'Martial arts', 2, 'http://162.243.72.219/packages/uploads/activity/1498218482.jpg', '2017-06-08', '15:30', '-1.3082986', '36.732905500000015', 'Ngong Road, Nairobi, Kenya', 1, 'martial-arts', '2017-05-30', 1, 3000, '2017-05-30 08:18:49', '2017-06-23 11:48:02'),
(6, 'Sports', 6, 'Sports', 1, 'http://162.243.72.219/packages/uploads/activity/1498218530.jpeg', '2017-06-01', '08:00', '-1.2279931', '36.89064380000002', 'Moi International Sports Center, Nairobi, Kenya', 1, 'sports', '2017-05-30', 1, 500, '2017-05-30 08:19:35', '2017-06-23 11:48:50'),
(7, 'Yoga', 6, 'Yoga', 1, 'http://162.243.72.219/packages/uploads/activity/1498218591.jpeg', '2017-06-10', '14:30', '-1.2611605', '36.80197769999995', 'Sarit Centre, Karuna Road, Nairobi, Kenya', 1, 'yoga', '2017-05-30', 2, 2500, '2017-05-30 08:21:00', '2017-06-23 11:49:51');

-- --------------------------------------------------------

--
-- Table structure for table `activity_images`
--

CREATE TABLE `activity_images` (
  `id` int(11) NOT NULL,
  `image_url` varchar(255) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity_images`
--

INSERT INTO `activity_images` (`id`, `image_url`, `activity_id`, `created_at`, `updated_at`) VALUES
(5, 'http://uzyma.com/packages/uploads/productsimages/1499332624.jpg', 1, '2017-07-06 09:17:04', '2017-07-06 09:17:04'),
(6, 'http://uzyma.com/packages/uploads/productsimages/1499332633.jpg', 1, '2017-07-06 09:17:13', '2017-07-06 09:17:13'),
(7, 'http://uzyma.com/packages/uploads/productsimages/1499332645.jpg', 1, '2017-07-06 09:17:25', '2017-07-06 09:17:25'),
(8, 'http://uzyma.com/packages/uploads/productsimages/1499332747.jpg', 1, '2017-07-06 09:19:07', '2017-07-06 09:19:07'),
(9, 'http://uzyma.com/packages/uploads/productsimages/1499332983.jpg', 2, '2017-07-06 09:23:03', '2017-07-06 09:23:03'),
(10, 'http://uzyma.com/packages/uploads/productsimages/1499332991.jpg', 2, '2017-07-06 09:23:11', '2017-07-06 09:23:11'),
(11, 'http://uzyma.com/packages/uploads/productsimages/1499333006.jpg', 2, '2017-07-06 09:23:26', '2017-07-06 09:23:26'),
(12, 'http://uzyma.com/packages/uploads/productsimages/1499333014.jpg', 2, '2017-07-06 09:23:34', '2017-07-06 09:23:34'),
(13, 'http://uzyma.com/packages/uploads/productsimages/1499333037.jpg', 2, '2017-07-06 09:23:57', '2017-07-06 09:23:57'),
(14, 'http://uzyma.com/packages/uploads/productsimages/1499333157.jpg', 3, '2017-07-06 09:25:57', '2017-07-06 09:25:57'),
(15, 'http://uzyma.com/packages/uploads/productsimages/1499333165.jpg', 3, '2017-07-06 09:26:05', '2017-07-06 09:26:05'),
(16, 'http://uzyma.com/packages/uploads/productsimages/1499333178.jpg', 3, '2017-07-06 09:26:18', '2017-07-06 09:26:18'),
(18, 'http://uzyma.com/packages/uploads/productsimages/1499333202.jpg', 3, '2017-07-06 09:26:42', '2017-07-06 09:26:42'),
(19, 'http://uzyma.com/packages/uploads/productsimages/1499333356.jpg', 4, '2017-07-06 09:29:16', '2017-07-06 09:29:16'),
(20, 'http://uzyma.com/packages/uploads/productsimages/1499333364.jpg', 4, '2017-07-06 09:29:24', '2017-07-06 09:29:24'),
(21, 'http://uzyma.com/packages/uploads/productsimages/1499333374.jpg', 4, '2017-07-06 09:29:34', '2017-07-06 09:29:34'),
(22, 'http://uzyma.com/packages/uploads/productsimages/1499333385.jpg', 4, '2017-07-06 09:29:45', '2017-07-06 09:29:45'),
(23, 'http://uzyma.com/packages/uploads/productsimages/1499333398.jpg', 4, '2017-07-06 09:29:58', '2017-07-06 09:29:58');

-- --------------------------------------------------------

--
-- Table structure for table `app_users`
--

CREATE TABLE `app_users` (
  `id` int(11) NOT NULL,
  `firebase_id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `birth_date` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `terms` int(11) NOT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_users`
--

INSERT INTO `app_users` (`id`, `firebase_id`, `name`, `email`, `gender`, `birth_date`, `contact`, `image`, `terms`, `latitude`, `longitude`, `created_at`, `updated_at`) VALUES
(1, 'XbVsuQ33F7fuyuThI26gxSWv36E2', 'oluaa', 'olua@ggmail.com', 'Male', NULL, NULL, 'null', 1, NULL, NULL, '2017-05-08 14:16:15', '2017-05-08 14:16:15'),
(2, 'pCAioxyFKXUj8Exo0lHFqLhPhAl1', 'oluaa', 'olua@ghmail.com', 'Male', NULL, NULL, 'null', 1, NULL, NULL, '2017-05-08 14:16:53', '2017-05-08 14:16:53'),
(3, 'ss8VNB6jBlgTU5p21HV15I4HDUb2', 'oluaa', 'olua@gimail.com', 'Male', NULL, NULL, 'null', 1, NULL, NULL, '2017-05-08 14:17:14', '2017-05-08 14:17:14'),
(4, 'ZcmQuaI2u0XBT0lg2cb9teLWyF72', 'olaaa', 'olaaa@gmail.com', 'Male', NULL, NULL, 'null', 1, NULL, NULL, '2017-05-08 14:18:58', '2017-05-08 14:18:58'),
(5, 'Rv2KHOs774YDGo2f9Mew0UC6es72', 'ol', 'ol@gmail.com', 'Male', NULL, NULL, 'null', 1, NULL, NULL, '2017-05-08 14:26:45', '2017-05-08 14:26:45'),
(6, 'NJfgD5jTXMfnAqOH2fDXohoIWi43', 'Kelvin Kiok', 'kelvinmkioko@gmail.com', 'Male', '2017-05-26', '+254 700 000 000', 'http://uzyma.com/packages/uploads/profile/2017/5/2017_5_27_1495881288.png', 1, NULL, NULL, '2017-05-08 14:43:35', '2017-07-17 15:25:56'),
(7, 'euj12RUR4jaoVfn5sDUgEwleer62', 'Victor Mochengo', 'victormochengo@gmail.com', 'Male', '00-00-0000', '+254 723649624', 'http://uzyma.com/packages/uploads/profile/2017/6/2017_6_18_1497781636.png', 1, NULL, NULL, '2017-05-08 18:33:05', '2017-06-18 10:27:16'),
(8, '10209481290498118', 'Kelvin Kioko', 'kiokokelvin@gmail.com', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2017-05-09 18:37:04', '2017-05-09 18:37:04'),
(9, 'goLQLi78iAQ9L59f1ILJzW3FEhF2', 'Kennedy Mwangi', 'kenkiggz@gmail.com', NULL, NULL, NULL, 'https://lh5.googleusercontent.com/-lDVEqfjxotM/AAAAAAAAAAI/AAAAAAAAAAA/AHalGhql7sI9u4Za-d2_7FnC8kY1I3rJ5Q/s96-c/photo.jpg', 1, NULL, NULL, '2017-05-10 14:20:32', '2017-05-10 14:20:32'),
(10, '4CStPIaCevVKzswhqWTkwUlym4z1', 'Michael Chege', 'chegemike4@gmail.com', NULL, NULL, NULL, 'https://lh6.googleusercontent.com/-JjB2hdFeMaE/AAAAAAAAAAI/AAAAAAAAAAA/AHalGhoX33scD735IBVXEUJXBYe_cUFjhA/s96-c/photo.jpg', 1, NULL, NULL, '2017-05-12 12:03:43', '2017-05-12 12:03:43'),
(11, '1483501861670264', 'Kenn Murimi', 'murimi.kenn@gmail.com', 'Male', '1990-08-08', '+254 700 000 000', 'http://uzyma.com/packages/uploads/profile/2017/5/2017_5_26_1495814458.png', 1, NULL, NULL, '2017-05-26 15:59:01', '2017-05-26 16:00:58'),
(12, 'YydS23K4DVZXAU6RrusrRyj1Uxx2', 'Kangai', 'kangai.mwiti@gmail.com', 'Female', NULL, NULL, 'null', 1, NULL, NULL, '2017-07-11 16:31:12', '2017-07-11 16:31:12'),
(13, 'GzTczBMWkQge9Lh5MzkmlV32lKb2', 'kamwari', 'kamwarikinya@gmail.com', 'Female', NULL, NULL, 'null', 1, NULL, NULL, '2017-08-03 11:26:23', '2017-08-03 11:26:23');

-- --------------------------------------------------------

--
-- Table structure for table `businesses`
--

CREATE TABLE `businesses` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `partner_name` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `business_name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `businesses`
--

INSERT INTO `businesses` (`id`, `user_id`, `partner_name`, `phone_number`, `status`, `slug`, `image_url`, `business_name`, `created_at`, `updated_at`) VALUES
(6, 8, 'Kenned Murimi', '0704103356', 0, 'name', NULL, 'Fire salloon', '2017-08-03 08:47:14', '2017-08-03 08:47:14'),
(7, 12, 'Victor Mochengo', '0723649624', 0, 'name', NULL, 'Vic', '2017-08-30 16:54:13', '2017-08-30 16:54:13'),
(8, 14, 'Victor Mochengo', '0723649624', 0, 'name', NULL, 'Vic 2', '2017-08-31 12:43:30', '2017-08-31 12:43:30'),
(9, 15, 'Karen Rollier Kinya', '0735271322', 0, 'name', NULL, 'mirror mirror salon barber and spa', '2017-09-13 08:01:31', '2017-09-13 08:01:31');

-- --------------------------------------------------------

--
-- Table structure for table `business_activity`
--

CREATE TABLE `business_activity` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `business_id` int(11) DEFAULT NULL,
  `stylist_id` int(11) DEFAULT NULL,
  `activity_date` date DEFAULT NULL,
  `style` text,
  `cost` varchar(255) DEFAULT NULL,
  `type_client` int(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `business_activity`
--

INSERT INTO `business_activity` (`id`, `customer_id`, `business_id`, `stylist_id`, `activity_date`, `style`, `cost`, `type_client`, `created_at`, `updated_at`) VALUES
(1, 5, 8, 2, '2017-08-04', 'manywele', '2000', 1, '2017-08-04 13:51:06', '2017-08-04 13:51:06'),
(2, 6, 8, 2, '2017-08-04', 'Afro', '1000', 1, '2017-08-04 13:51:17', '2017-08-04 13:51:17'),
(3, 7, 6, 2, '2017-08-25', 'Braids', '4000', 1, '2017-08-25 14:57:02', '2017-08-25 14:57:02'),
(4, 8, 6, 2, '2017-08-25', 'Kinyozi', '4000', 1, '2017-08-25 18:39:39', '2017-08-25 18:39:39'),
(5, 9, 6, 2, '2017-08-26', 'Styling hair', '4000', 2, '2017-08-26 08:00:57', '2017-08-26 08:00:57'),
(6, 10, 6, 2, '2017-08-26', 'Braids', '1200', 1, '2017-08-26 08:03:32', '2017-08-26 08:03:32'),
(7, 11, 6, 2, '2017-08-27', 'Braids (Locs)', '2000', 1, '2017-08-27 18:06:32', '2017-08-27 18:06:32'),
(8, 12, 6, 2, '2017-08-27', 'Dreadlocks', '5400', 1, '2017-08-27 18:08:45', '2017-08-27 18:08:45'),
(9, 13, 6, 2, '2017-08-27', 'Barber', '3500', 2, '2017-08-27 18:10:55', '2017-08-27 18:10:55');

-- --------------------------------------------------------

--
-- Table structure for table `business_type`
--

CREATE TABLE `business_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `business_type`
--

INSERT INTO `business_type` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'gym', '2017-05-04 04:58:08', '2017-05-04 04:58:08'),
(2, 'test', '2017-05-04 04:58:22', '2017-05-04 05:01:32');

-- --------------------------------------------------------

--
-- Table structure for table `checkout`
--

CREATE TABLE `checkout` (
  `id` int(11) NOT NULL,
  `app_user_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `paid` int(11) NOT NULL DEFAULT '0',
  `check_out_date` varchar(255) NOT NULL,
  `total` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `checkout`
--

INSERT INTO `checkout` (`id`, `app_user_id`, `activity_id`, `owner_id`, `paid`, `check_out_date`, `total`, `created_at`, `updated_at`) VALUES
(2, 6, 2, 6, 0, '2017-05-26 18:05:12', '4000', '2017-05-26 15:05:12', '2017-05-26 15:05:12'),
(3, 11, 3, 6, 0, '2017-05-26 19:02:47', '1000', '2017-05-26 16:02:47', '2017-05-26 16:02:47'),
(4, 6, 1, 6, 0, '2017-05-26 19:55:43', '2000', '2017-05-26 16:55:43', '2017-05-26 16:55:43'),
(5, 11, 2, 6, 0, '2017-05-28 11:56:41', '4000', '2017-05-28 08:56:41', '2017-05-28 08:56:41'),
(6, 11, 4, 6, 0, '2017-05-28 11:56:42', '5000', '2017-05-28 08:56:42', '2017-05-28 08:56:42'),
(7, 11, 1, 6, 0, '2017-05-30 11:22:52', '2000', '2017-05-30 08:22:52', '2017-05-30 08:22:52'),
(8, 7, 1, 6, 0, '2017-06-04 11:10:21', '2000', '2017-06-04 08:10:21', '2017-06-04 08:10:21'),
(9, 7, 2, 6, 0, '2017-06-05 22:06:27', '4000', '2017-06-05 19:06:27', '2017-06-05 19:06:27'),
(10, 7, 7, 6, 0, '2017-06-05 22:06:27', '2500', '2017-06-05 19:06:27', '2017-06-05 19:06:27'),
(11, 7, 1, 6, 0, '2017-06-06 11:47:42', '2000', '2017-06-06 08:47:42', '2017-06-06 08:47:42'),
(12, 7, 1, 6, 0, '2017-06-10 18:00:21', '2000', '2017-06-10 15:00:21', '2017-06-10 15:00:21'),
(13, 7, 3, 6, 0, '2017-06-10 18:00:57', '1000', '2017-06-10 15:00:57', '2017-06-10 15:00:57'),
(14, 7, 5, 6, 0, '2017-06-10 18:00:58', '3000', '2017-06-10 15:00:58', '2017-06-10 15:00:58'),
(15, 7, 2, 6, 0, '2017-06-10 18:00:58', '4000', '2017-06-10 15:00:58', '2017-06-10 15:00:58'),
(16, 7, 1, 6, 0, '2017-06-13 11:00:55', '2000', '2017-06-13 08:00:55', '2017-06-13 08:00:55'),
(17, 7, 1, 6, 0, '2017-06-14 21:14:33', '2000', '2017-06-14 18:14:33', '2017-06-14 18:14:33'),
(18, 7, 1, 6, 0, '2017-06-16 12:22:15', '2000', '2017-06-16 09:22:15', '2017-06-16 09:22:15'),
(19, 7, 3, 6, 0, '2017-06-16 12:22:16', '1000', '2017-06-16 09:22:16', '2017-06-16 09:22:16'),
(20, 6, 1, 6, 0, '2017-06-18 12:40:47', '2000', '2017-06-18 09:40:47', '2017-06-18 09:40:47'),
(21, 6, 1, 6, 0, '2017-06-20 15:14:31', '2000', '2017-06-20 12:14:31', '2017-06-20 12:14:31'),
(22, 7, 1, 6, 0, '2017-06-20 19:15:49', '2000', '2017-06-20 16:15:49', '2017-06-20 16:15:49'),
(23, 7, 1, 6, 0, '2017-06-20 19:20:10', '2000', '2017-06-20 16:20:10', '2017-06-20 16:20:10'),
(24, 7, 1, 6, 0, '2017-06-21 12:29:49', '2000', '2017-06-21 09:29:49', '2017-06-21 09:29:49'),
(25, 7, 6, 6, 0, '2017-06-25 11:51:38', '500', '2017-06-25 08:51:38', '2017-06-25 08:51:38'),
(26, 7, 1, 6, 0, '2017-06-29 20:47:15', '2000', '2017-06-29 17:47:15', '2017-06-29 17:47:15'),
(27, 7, 1, 6, 0, '2017-07-04 20:36:21', '2000', '2017-07-04 17:36:21', '2017-07-04 17:36:21'),
(28, 7, 1, 6, 0, '2017-07-05 13:00:54', '2000', '2017-07-05 10:00:54', '2017-07-05 10:00:54'),
(29, 7, 1, 6, 0, '2017-07-05 13:01:00', '2000', '2017-07-05 10:01:00', '2017-07-05 10:01:00'),
(30, 7, 1, 6, 0, '2017-07-06 10:25:49', '2000', '2017-07-06 07:25:49', '2017-07-06 07:25:49'),
(31, 7, 2, 6, 0, '2017-07-06 14:27:44', '4000', '2017-07-06 11:27:44', '2017-07-06 11:27:44'),
(32, 7, 1, 6, 0, '2017-07-13 12:19:31', '2000', '2017-07-13 09:19:31', '2017-07-13 09:19:31'),
(33, 10, 1, 6, 0, '2017-07-21 19:25:20', '2000', '2017-07-21 16:25:20', '2017-07-21 16:25:20'),
(34, 7, 1, 6, 0, '2017-07-25 18:09:22', '2000', '2017-07-25 15:09:22', '2017-07-25 15:09:22'),
(35, 13, 2, 6, 0, '2017-08-24 10:59:39', '4000', '2017-08-24 07:59:39', '2017-08-24 07:59:39'),
(36, 13, 1, 6, 0, '2017-08-24 10:59:40', '2000', '2017-08-24 07:59:40', '2017-08-24 07:59:40'),
(37, 13, 4, 6, 0, '2017-08-24 10:59:40', '5000', '2017-08-24 07:59:40', '2017-08-24 07:59:40');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `app_user_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `message` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contact_emails`
--

CREATE TABLE `contact_emails` (
  `id` int(11) NOT NULL,
  `emails` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `business_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `phone`, `email`, `business_id`, `created_at`, `updated_at`) VALUES
(5, 'mzee', '0704103356', 'mzee@gmail.com', 8, '2017-08-04 13:51:06', '2017-08-04 13:51:06'),
(6, 'Olua', '07118', 'olua@gmail.com', 8, '2017-08-04 13:51:17', '2017-08-04 13:51:17'),
(7, 'Naomi', '0704103356', 'murimi.kenn@gmail.comn', 6, '2017-08-25 14:57:02', '2017-08-25 14:57:02'),
(8, 'Kenn', '0704103356', 'murimi.kenn@gmail.comn', 6, '2017-08-25 18:39:39', '2017-08-25 18:39:39'),
(9, 'Kenn', '0704103356', 'murimi.kenn@gmail.comn', 6, '2017-08-26 08:00:57', '2017-08-26 08:00:57'),
(10, 'Olivier Martinez', '0780999666', 'Olivier@gmail.com', 6, '2017-08-26 08:03:32', '2017-08-26 08:03:32'),
(11, 'Maureen', '254', 'maureen@yahoo.com', 6, '2017-08-27 18:06:32', '2017-08-27 18:06:32'),
(12, 'Grace', '254', 'yahoo', 6, '2017-08-27 18:08:45', '2017-08-27 18:08:45'),
(13, 'George', '123', 'ymail', 6, '2017-08-27 18:10:55', '2017-08-27 18:10:55');

-- --------------------------------------------------------

--
-- Table structure for table `customer_type`
--

CREATE TABLE `customer_type` (
  `id` int(11) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_type`
--

INSERT INTO `customer_type` (`id`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Referral', NULL, NULL),
(2, 'Walk in', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `difficulty`
--

CREATE TABLE `difficulty` (
  `id` int(11) NOT NULL,
  `level_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `difficulty`
--

INSERT INTO `difficulty` (`id`, `level_name`, `created_at`, `updated_at`) VALUES
(1, 'Bigginer', '2017-05-10 03:12:43', '2017-05-10 03:12:43'),
(2, 'Professional', '2017-05-10 03:12:55', '2017-05-10 03:17:52');

-- --------------------------------------------------------

--
-- Table structure for table `managers`
--

CREATE TABLE `managers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `business_id` int(11) NOT NULL DEFAULT '0',
  `users_id` int(11) NOT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `managers`
--

INSERT INTO `managers` (`id`, `name`, `phone_number`, `business_id`, `users_id`, `image_url`, `email`, `created_at`, `updated_at`) VALUES
(5, 'Njeri', '0704103356', 6, 9, NULL, 'njeri.gichuhi@students.jkuat.ac.ke', '2017-08-25 10:28:58', '2017-08-25 10:28:58'),
(6, 'Shiko', '07', 6, 10, NULL, 'shikohn@yahoo.com', '2017-08-29 10:05:29', '2017-08-29 10:05:29'),
(8, 'Ivy Muigai', '07', 6, 13, NULL, 'ivywmuigai@gmail.com', '2017-08-31 07:48:59', '2017-08-31 07:48:59'),
(9, 'Mundu', '0723649624', 7, 16, NULL, 'mkhumundu@gmail.com', '2017-09-15 10:29:54', '2017-09-15 10:29:54');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `partner_name` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `user_id`, `partner_name`, `phone_number`, `status`, `slug`, `image_url`, `created_at`, `updated_at`) VALUES
(3, 6, 'Kennedy gym', '+254704103356', 0, 'name', NULL, '2017-05-04 06:27:59', '2017-05-04 06:29:19');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `site_name` varchar(255) NOT NULL,
  `site_email` varchar(255) NOT NULL,
  `site_phone` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `site_name`, `site_email`, `site_phone`, `address`, `created_at`, `updated_at`) VALUES
(1, 'uzyma', 'uzyma@gmail.com', '+254 704 103 356', 'Mama Ngina Street, Vedic House office 205', '2017-04-22 07:26:00', '2017-04-22 07:26:00');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `slider_name` varchar(255) NOT NULL,
  `image_url` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `slider_name`, `image_url`, `created_at`, `updated_at`) VALUES
(1, 'test', 'http://162.243.72.219/packages/uploads/slider/1499268633.png', '2017-07-05 12:18:40', '2017-07-05 15:30:33');

-- --------------------------------------------------------

--
-- Table structure for table `stylist`
--

CREATE TABLE `stylist` (
  `id` int(11) NOT NULL,
  `business_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `terms` int(11) NOT NULL DEFAULT '0',
  `image_url` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `firebase_id` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stylist`
--

INSERT INTO `stylist` (`id`, `business_id`, `name`, `phone_number`, `terms`, `image_url`, `email`, `firebase_id`, `created_at`, `updated_at`) VALUES
(2, 6, 'Hellen Wanjiru', '0704103356', 0, NULL, NULL, NULL, '2017-08-03 03:38:26', '2017-08-03 03:38:26');

-- --------------------------------------------------------

--
-- Table structure for table `subscription`
--

CREATE TABLE `subscription` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscription`
--

INSERT INTO `subscription` (`id`, `email`, `created_at`, `updated_at`) VALUES
(1, 'murimi.kenn@gmail.com', '2017-04-24 07:28:21', '2017-04-24 07:28:21'),
(2, 'victormochengo@gmail.com', '2017-05-12 12:25:48', '2017-05-12 12:25:48'),
(3, 'lenahmoraah@gmail.com', '2017-05-14 07:53:05', '2017-05-14 07:53:05'),
(4, 'ndutacnjeru@gmail.com', '2017-05-14 13:33:47', '2017-05-14 13:33:47'),
(5, 'nyaeraj@gmail.com', '2017-05-15 13:31:31', '2017-05-15 13:31:31');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `verified` tinyint(4) NOT NULL DEFAULT '0',
  `email_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_type` int(11) NOT NULL DEFAULT '101',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `verified`, `email_token`, `remember_token`, `account_type`, `created_at`, `updated_at`) VALUES
(2, 'kennedy', 'hellen.kenn@gmail.com', '$2y$10$qguyar.W6vkqFDPl9Nq2y.3okV2XHMBmMn.jaTq9QXB0k.Rw2n...', 1, NULL, 'hzjCqqQIYw8IaIiGvWLgiGBHxW2kR1qUoE8iIs7xeXZfEdFtAYOeCEIxc803', 221, '2017-04-21 08:41:11', '2017-04-21 08:41:59'),
(6, 'Kennedy gym', 'kennedy@designlabtechnologies.com', '$2y$10$ZhHtgqUKzqkvcBHU.Ss6qekdOB0jSe045oCKYf3Ypz.VtBhHnqrx.', 1, NULL, 'JZX2or1bnRjJC1ekvRmULQ4avq57LIbgTM0gV8qyxnon2rk4G180iidjU4UA', 101, '2017-05-04 06:27:59', '2017-05-05 05:39:49'),
(8, 'Kenned Murimi', 'murimi.kenn@gmail.com', '$2y$10$UIYiRAPS0nxIa9uudtaN8usB6M8qkWHZ1bTiNZFmV8gKr9Dy7F93G', 1, NULL, NULL, 333, '2017-08-03 08:47:14', '2017-08-03 08:47:34'),
(9, 'Njeri', 'njeri.gichuhi@students.jkuat.ac.ke', '$2y$10$Zmwcu2xhWtbyu2o8W0rhVeiOcA5gChcnrnAQGaIx9ZFQzTrXvz2VC', 0, 'jUudcOOfql', NULL, 444, '2017-08-25 10:28:58', '2017-08-25 10:28:58'),
(10, 'Shiko', 'shikohn@yahoo.com', '$2y$10$TioawGFj32scypbtCQJRWOg9sJ7/HE1YHRBrYhQRPit7evoaJ/JTG', 0, '994wv3Scv9', NULL, 444, '2017-08-29 10:05:29', '2017-08-29 10:05:29'),
(12, 'Victor Mochengo', 'victormochengo@gmail.com', '$2y$10$dKXVg4CvxcqqoC/6yLFbz.iwmVmaAWXejR8ajCXXzrNOmSj7BymC2', 1, NULL, '6j8yqYhva4Aqu8HfRyHuLswkgW3oP93OqvE7gsjsa4J0j7m0TFnBQjMTbhKr', 333, '2017-08-30 16:54:13', '2017-08-30 16:54:36'),
(13, 'Ivy Muigai', 'ivywmuigai@gmail.com', '$2y$10$Ms4Rtby0y8ZgVenqYnU7qOUJ3hFN4fRvD8.jcKCdToUWIBi004qsm', 0, 'RWFql0qW9f', NULL, 444, '2017-08-31 07:48:59', '2017-08-31 07:48:59'),
(14, 'Victor Mochengo', 'victormochengo@uzyma.com', '$2y$10$0tWOVqlVVlCCjuIPqkcpouY4Xpu1.19ZMXYYgxEmGOBXvnM.rxrqi', 1, NULL, NULL, 333, '2017-08-31 12:43:30', '2017-08-31 12:43:48'),
(15, 'Karen Rollier Kinya', 'kamwarikinya@gmail.com', '$2y$10$PGKmW74u0eXEb1YbTM0LVOZNlqjsP8PD5RlDdfdCmGSTxeEtP/zue', 1, NULL, 'Ybz5YTSq2czT5TIAsGf4j1BBcy3JzfWRkVsAk2ACo5IkTvKFeEmtEIWWzUyT', 333, '2017-09-13 08:01:31', '2017-09-13 08:02:37'),
(16, 'Mundu', 'mkhumundu@gmail.com', '$2y$10$DJ3Dv3ytI8gnM6aXiYajv.Xk37k6ETEmjnwVbPgDDyglhIzW/HfYq', 0, 'TBV8SKlUrx', NULL, 444, '2017-09-15 10:29:54', '2017-09-15 10:29:54');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activitys`
--
ALTER TABLE `activitys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activity_images`
--
ALTER TABLE `activity_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_users`
--
ALTER TABLE `app_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `businesses`
--
ALTER TABLE `businesses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business_activity`
--
ALTER TABLE `business_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business_type`
--
ALTER TABLE `business_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `checkout`
--
ALTER TABLE `checkout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_emails`
--
ALTER TABLE `contact_emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_type`
--
ALTER TABLE `customer_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `difficulty`
--
ALTER TABLE `difficulty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `managers`
--
ALTER TABLE `managers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stylist`
--
ALTER TABLE `stylist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscription`
--
ALTER TABLE `subscription`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activitys`
--
ALTER TABLE `activitys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `activity_images`
--
ALTER TABLE `activity_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `app_users`
--
ALTER TABLE `app_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `businesses`
--
ALTER TABLE `businesses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `business_activity`
--
ALTER TABLE `business_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `business_type`
--
ALTER TABLE `business_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `checkout`
--
ALTER TABLE `checkout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contact_emails`
--
ALTER TABLE `contact_emails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `customer_type`
--
ALTER TABLE `customer_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `difficulty`
--
ALTER TABLE `difficulty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `managers`
--
ALTER TABLE `managers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `stylist`
--
ALTER TABLE `stylist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `subscription`
--
ALTER TABLE `subscription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
